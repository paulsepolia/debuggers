#!/bin/bash

  g++-9.2.0    -g                 \
		       -O0                \
               -Wall              \
               -std=gnu++17       \
               driver_program.cpp \
               -o x_gnu
