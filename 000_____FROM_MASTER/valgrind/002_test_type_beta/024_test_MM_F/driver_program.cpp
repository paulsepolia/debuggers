
//================//
// shared pointer //
//================//

#include <iostream>
#include <memory>

using std::cin;
using std::cout;
using std::endl;
using std::shared_ptr;

//==================//
// the main program //
//==================//

int main ()
{
    const int IMAX = 10;

    for (int i = 0; i != IMAX; i++) {
        cout << "---------------------------------------------->> " << i << endl;

        shared_ptr<double> sp1(new double(11.1));
        shared_ptr<double> sp2(sp1);
        shared_ptr<double> sp3(sp1);
        shared_ptr<double> sp4(sp1);

        cout << " --> sp1.use_count() = " << sp1.use_count() << endl;
        cout << " --> sp2.use_count() = " << sp2.use_count() << endl;
        cout << " --> sp3.use_count() = " << sp3.use_count() << endl;
        cout << " --> sp4.use_count() = " << sp4.use_count() << endl;

        cout << " --> sp1.get() = " << sp1.get() << endl;
        cout << " --> sp2.get() = " << sp2.get() << endl;
        cout << " --> sp3.get() = " << sp3.get() << endl;
        cout << " --> sp4.get() = " << sp4.get() << endl;

        cout << " --> *sp1.get() = " << *sp1.get() << endl;
        cout << " --> *sp2.get() = " << *sp2.get() << endl;
        cout << " --> *sp3.get() = " << *sp3.get() << endl;
        cout << " --> *sp4.get() = " << *sp4.get() << endl;
    }

    return 0;
}

// END
