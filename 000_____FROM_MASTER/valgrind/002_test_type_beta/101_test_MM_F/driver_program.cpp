//================//
// shared pointer //
//================//

#include <iostream>
#include <memory>
#include <cmath>
#include <iomanip>

using std::cout;
using std::endl;
using std::allocator;
using std::pow;
using std::boolalpha;
using std::shared_ptr;
using std::fixed;
using std::setprecision;

// type definitions

typedef unsigned long int uli;
typedef const uli culi;

// the main program

int main()
{
    cout << boolalpha;
    cout << fixed;
    cout << setprecision(10);

    // local parameters

    culi DIMEN(static_cast<culi>(pow(10.0, 2.0)));
    culi TRIALS(static_cast<culi>(pow(10.0, 2.0)));

    // main game

    for(uli j = 0; j != TRIALS; j++) {

        cout << "----------------------------------------------->> " << j << endl;

        shared_ptr<double> spd(new double(1.23456789));

        cout << " -->  spd.use_count() = " << spd.use_count() << endl;
        cout << " -->  spd             = " << spd << endl;
        cout << " -->  spd.get()       = " << spd.get() << endl;

        shared_ptr<double> * arr(new shared_ptr<double> [DIMEN]);

        for(uli i = 0; i != DIMEN; i++) {
            arr[i] = spd;
            //arr[i].reset(spd.get());
            // THIS IS NOT EQUIVALENT TO THE ABOVE
            // SINCE DOES NOT INCREASE THE COUNTER
            // BECAUSE THE SYSTEM DOES NOT KNOW THAT THE ADRRESS IS COMING
            // FROM A SHARED PTR
        }

        cout << " -->  spd.use_count() = " << spd.use_count() << endl;
        cout << " -->  spd.get()       = " << spd.get() << endl;
        cout << " -->  arr[0].get()    = " << arr[0].get() << endl;
        cout << " -->  arr[1].get()    = " << arr[1].get() << endl;
        cout << " --> *arr[0].get()    = " << *arr[0].get() << endl;
        cout << " --> *arr[1].get()    = " << *arr[1].get() << endl;

        // delete heap

        delete [] arr;

    }

    return 0;
}

// end
