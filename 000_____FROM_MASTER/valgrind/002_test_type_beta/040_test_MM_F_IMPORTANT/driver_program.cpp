//================//
// shared pointer //
//================//

#include <iostream>
#include <memory>

using std::cin;
using std::cout;
using std::endl;
using std::shared_ptr;

//==================//
// the main program //
//==================//

int main ()
{
    const int SIZE = 1000;
    const int IMAX = 10;
    shared_ptr<double> p1;

    for (int i = 0; i != IMAX; i++) {
        cout << "---------------------------------------------->> " << i << endl;
        p1.reset(new double[SIZE]);
        for (int j = 0; j != SIZE; j++) {
            p1.get()[j] = static_cast<double>(j);
        }
        cout << " --> p1.get()[i] = " << p1.get()[i] << endl;
    }

    cout << " --> p1.get()[0] = " << p1.get()[0] << endl;
    cout << " --> p1.get()[1] = " << p1.get()[1] << endl;
    cout << " --> p1.get()[2] = " << p1.get()[2] << endl;
    cout << " --> p1.get()[3] = " << p1.get()[3] << endl;
    cout << " --> p1.get()[SIZE-1] = " << p1.get()[SIZE-1] << endl;

    return 0;
}

// END
