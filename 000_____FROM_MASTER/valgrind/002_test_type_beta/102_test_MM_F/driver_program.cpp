//================//
// unique pointer //
//================//

#include <iostream>
#include <memory>
#include <cmath>
#include <iomanip>

using std::cout;
using std::endl;
using std::allocator;
using std::pow;
using std::boolalpha;
using std::unique_ptr;
using std::fixed;
using std::setprecision;

// type definitions

typedef unsigned long int uli;
typedef const uli culi;

// the main program

int main()
{
    cout << boolalpha;
    cout << fixed;
    cout << setprecision(10);

    // local parameters

    culi DIMEN(static_cast<culi>(pow(10.0, 2.0)));
    culi TRIALS(static_cast<culi>(pow(10.0, 2.0)));

    // main game

    for(uli j = 0; j != TRIALS; j++) {

        cout << "----------------------------------------------->> " << j << endl;

        unique_ptr<double> * arr(new unique_ptr<double> [DIMEN]);

        for(uli i = 0; i != DIMEN; i++) {
            arr[i].reset(new double(i));
        }

        cout << " -->  arr[0].get()    = " <<  arr[0].get() << endl;
        cout << " -->  arr[1].get()    = " <<  arr[1].get() << endl;
        cout << " -->  arr[2].get()    = " <<  arr[2].get() << endl;
        cout << " --> *arr[0].get()    = " << *arr[0].get() << endl;
        cout << " --> *arr[1].get()    = " << *arr[1].get() << endl;
        cout << " --> *arr[2].get()    = " << *arr[2].get() << endl;

        // delete heap

        for(uli k = 0; k != DIMEN; k++) {
            arr[1].reset();
        }

        delete [] arr;

    }

    return 0;
}

// end
