//=============================================//
// delete heap via shared pointers 		  //
// and static shared pointer garbage collector //
// NO LEAK!                        		  //
//=============================================//

#include <iostream>
#include <memory>
#include <vector>
#include <cmath>

using std::cin;
using std::cout;
using std::endl;
using std::shared_ptr;
using std::vector;
using std::pow;

//==================//
// type definitions //
//==================//

typedef unsigned long int uli;

//=====================//
// class A declaration //
//=====================//

template<typename T>
class A {
public:

    // constructor

    A();

    // destructor

    virtual ~A();

    // member function

    void fun1() const;

protected:

    static vector<shared_ptr<T>> _garbage;
};

//====================//
// class A definition //
//====================//

// static member initialization

template <typename T>
vector<shared_ptr<T>> A<T>::_garbage;

// constructor

template <typename T>
A<T>::A() {}

// destructor

template <typename T>
A<T>::~A() {}

// member function

template <typename T>
void A<T>::fun1() const
{
    const uli IMAX = static_cast<uli>(pow(10.0, 4.0));
    for (uli i = 0; i != IMAX; i++) {
        T * pd = new T(i);
        shared_ptr<T> spd(pd);
        _garbage.push_back(spd);
    }
}

//==================//
// the main program //
//==================//

int main ()
{
    cout << " --> declare an object A" << endl;
    A<double> a1;
    cout << " --> call the 'fun1' member function" << endl;
    a1.fun1();

    return 0;
}

// END
