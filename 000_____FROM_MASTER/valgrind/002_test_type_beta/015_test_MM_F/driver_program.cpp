
#include <iostream>

using std::endl;
using std::cout;
using std::cin;

// base class --> Base

class Base {
public:

    // constructor

    Base()
    {
        cout << " --> constructor --> Base" << endl;
    }

    // destructor

    virtual ~Base()
    {
        cout << " --> destructor --> ~Base" << endl;
    }
};

// derived class --> D1

class D1 : public Base {
public:

    // constructor

    D1() : Base()
    {
        cout << " --> constructor --> D1" << endl;
    }

    // destructor

    virtual ~D1()
    {
        cout << " --> destructor --> ~D1" << endl;
    }
};

// the main program

int main()
{
    const int DIM = 100;

    // # 1

    cout << "------------------------------------------------------------>> # 1" << endl;
    cout << " --> Base * pb1 = new Base [DIM];" << endl;
    Base * pb1 = new Base [DIM];
    cout << " --> delete [] pb1;" << endl;
    delete [] pb1;

    // # 2

    cout << "------------------------------------------------------------>> # 2" << endl;

    cout << " --> Base * pb2 = new D1 [DIM];" << endl;
    Base * pb2 = new D1 [DIM] ;
    cout << " --> delete [] pb2;" << endl;
    delete [] pb2;

    // # 3

    cout << "------------------------------------------------------------>> # 3" << endl;
    cout << " --> D1 * pd1 = new D1 [DIM];" << endl;
    D1 * pd1 = new D1 [DIM];
    cout << " --> delete [] pd1;" << endl;
    delete [] pd1;

    cout << " ----------------------------> EXIT HERE(Nothing will happen)" << endl;

    return 0;
}

// end
