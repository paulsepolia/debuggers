//================//
// shared pointer //
//================//

// Convert many C-pointers to shared pointer
// using only one shared ptr

#include <iostream>
#include <memory>

using std::cin;
using std::cout;
using std::endl;
using std::shared_ptr;

//==================//
// the main program //
//==================//

int main ()
{
    const int IMAX = 10;
    double * p1;
    double * p2;
    shared_ptr<double> sp1;

    for (int i = 0; i != IMAX; i++) {
        cout << "---------------------------------------------->> " << i << endl;

        p1 = new double(1.1);

        cout << " --> 1" << endl;
        cout << " -->  p1 = " <<  p1 << endl;
        cout << " --> *p1 = " << *p1 << endl;

        sp1.reset(p1);

        cout << " --> 2" << endl;
        cout << " -->  p1 = " <<  p1 << endl;
        cout << " --> *p1 = " << *p1 << endl;

        p2 = new double(2.2);
        sp1.reset(p2);

        cout << " --> 3" << endl;
        cout << " -->  p1 = " <<  p1 << endl;
        cout << " --> *p1 = " << *p1 << endl;

        cout << " --> 4" << endl;
        cout << " -->  p2 = " <<  p2 << endl;
        cout << " --> *p2 = " << *p2 << endl;

        cout << " --> sp1.use_count() = " << sp1.use_count() << endl;
        cout << " --> sp1.get() = " << sp1.get() << endl;
        cout << " --> *sp1.get() = " << *sp1.get() << endl;
    }

    return 0;
}

// END
