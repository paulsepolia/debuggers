//================//
// shared pointer //
//================//

// Convert a C-pointer to shared pointer

#include <iostream>
#include <memory>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::shared_ptr;
using std::vector;

//==================//
// the main program //
//==================//

int main ()
{
    const int IMAX = 10;

    for (int i = 0; i != IMAX; i++) {
        cout << "---------------------------------------------->> " << i << endl;

        shared_ptr<vector<double>> sp1;

        vector<double> * p1 = new vector<double>( {1.1, 2.2, 3.3});

        sp1.reset(p1);

        cout << " --> sp1.use_count() = " << sp1.use_count() << endl;

        cout << " --> sp1.get() = " << sp1.get() << endl;
        cout << " --> p1        = " << p1 << endl;

        cout << " --> (*p1)[0] = " << (*p1)[0] << endl;
        cout << " --> (*p1)[1] = " << (*p1)[1] << endl;
        cout << " --> (*p1)[2] = " << (*p1)[2] << endl;

        cout << " --> (*(sp1.get()))[0] = " << (*(sp1.get()))[0] << endl;
        cout << " --> (*(sp1.get()))[1] = " << (*(sp1.get()))[1] << endl;
        cout << " --> (*(sp1.get()))[2] = " << (*(sp1.get()))[2] << endl;
    }

    return 0;
}

// END
