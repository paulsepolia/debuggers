//=============================================//
// delete heap via shared pointers 		  //
// and static shared pointer garbage collector //
// common to all derived classes               //
// NO LEAK!                        		  //
//=============================================//

#include <iostream>
#include <memory>
#include <vector>
#include <cmath>

using std::cin;
using std::cout;
using std::endl;
using std::shared_ptr;
using std::vector;
using std::pow;

//==================//
// type definitions //
//==================//

typedef unsigned long int uli;

//======================================//
// class 'GarbageCollector' declaration //
//======================================//

template<typename T>
class GarbageCollector {
public:
    virtual void freeRAM() const;
protected:
    static vector<shared_ptr<T>> _garbage;
};

//=====================================//
// class 'GarbageCollector' definition //
//=====================================//

// static member initialization

template <typename T>
vector<shared_ptr<T>> GarbageCollector<T>::_garbage;

// member function --> forceFreeRAM

template<typename T>
void GarbageCollector<T>::freeRAM() const
{
    _garbage.clear();
    _garbage.shrink_to_fit();
}

//======================//
// class A1 declaration //
//======================//

template<typename T>
class A1 : public GarbageCollector<T> {
public:

    // constructor

    A1();

    // destructor

    virtual ~A1();

    // member function

    virtual void fun() const;
};

//=====================//
// class A1 definition //
//=====================//

// constructor

template<typename T>
A1<T>::A1() {}

// destructor

template<typename T>
A1<T>::~A1() {}

// member function

template<typename T>
void A1<T>::fun() const
{
    const uli IMAX = static_cast<uli>(pow(10.0, 4.0));
    for (uli i = 0; i != IMAX; i++) {
        T * pd = new T(i);
        // collect garbages here
        shared_ptr<T> spd(pd);
        GarbageCollector<T>::_garbage.push_back(spd);
    }
}

//======================//
// class A2 declaration //
//======================//

template<typename T>
class A2 : public GarbageCollector<T> {
public:

    // constructor

    A2();

    // destructor

    virtual ~A2();

    // member function

    virtual void fun() const;
};

//=====================//
// class A2 definition //
//=====================//

// constructor

template<typename T>
A2<T>::A2() {}

// destructor

template<typename T>
A2<T>::~A2() {}

// member function

template<typename T>
void A2<T>::fun() const
{
    const uli IMAX = static_cast<uli>(pow(10.0, 4.0));
    for (uli i = 0; i != IMAX; i++) {
        T * pd = new T(i);
        // collect garbages here
        shared_ptr<T> spd(pd);
        GarbageCollector<T>::_garbage.push_back(spd);
    }
}

//==================//
// the main program //
//==================//

int main ()
{
    // local parameters and variables

    const uli IMAX = 5UL;
    const uli DO_MAX = 10;

    for (uli k = 0; k != DO_MAX; k++) {

        cout << " -------------------------------------------------------->> k = " << k << endl;

        // # 1

        cout << " --> declare an object of type A1<double>" << endl;

        A1<double> a1;

        cout << " --> call the 'fun' member function many times" << endl;

        for (uli i = 0; i != IMAX; i++) {
            a1.fun();
        }

        cout << " --> empty trash bin ..." << endl;

        a1.freeRAM();

        // # 2

        cout << " --> declare an object of type A2<int>" << endl;

        A2<int> a2;

        cout << " --> call the 'fun' member function many times" << endl;

        for (uli i = 0; i != IMAX; i++) {
            a2.fun();
        }

        cout << " --> empty trash bin ..." << endl;

        a2.freeRAM();

        // # 3

        cout << " --> declare an object of type A2<long double>" << endl;

        A2<long double> a3;

        cout << " --> call the 'fun' member function many times" << endl;

        for (uli i = 0; i != IMAX; i++) {
            a3.fun();
        }

        cout << " --> empty trash bin ..." << endl;

        a3.freeRAM();
    }

    return 0;
}

// END
