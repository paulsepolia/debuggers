//==========================//
// shared_ptr::owner_before //
//==========================//

#include <iostream>
#include <memory>
#include <iomanip>

using std::cout;
using std::endl;
using std::boolalpha;
using std::fixed;
using std::shared_ptr;

// the mian function

int main()
{
    cout << fixed;
    cout << boolalpha;

    int * p = new int (10);

    shared_ptr<int> a(new int (20));
    shared_ptr<int> b(a, p);  // alias constructor

    cout << " -->  a             = " << a << endl;
    cout << " -->  b             = " << b << endl;

    cout << " -->  a.get()       = " << a.get() << endl;
    cout << " -->  b.get()       = " << b.get() << endl;

    cout << " --> *a.get()       = " << *a.get() << endl;
    cout << " --> *b.get()       = " << *b.get() << endl;

    cout << " -->  a.use_count() = " << a.use_count() << endl;
    cout << " -->  b.use_count() = " << b.use_count() << endl;

    cout << " -->  comparing a and b..." << endl;
    cout << " -->  value-based = " << ((!(a < b)) && (!(b < a))) << endl;;
    cout << " -->  owner-based = " << ((!a.owner_before(b)) && (!b.owner_before(a))) << endl;

    // The a and b pointers share ownership
    // so the expressions bellow are false
    // Also, p must be explicitly deleted because it is not owned by none
    // of the shared pointers

    cout << " -->  a.owner_before(b) = " << a.owner_before(b) << endl;
    cout << " -->  b.owner_before(a) = " << b.owner_before(a) << endl;

    delete p; // p must be deleted here. It is a memory leak if not

    return 0;
}

// end
