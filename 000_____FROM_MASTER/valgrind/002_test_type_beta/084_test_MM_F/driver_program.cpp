//===================================================//
// use of copy constructor while building the vector //
//===================================================//

#include <iostream>
#include <cmath>
#include <vector>
#include <new>

using std::cout;
using std::endl;
using std::pow;
using std::vector;
using std::move;

typedef unsigned long int uli;
typedef const uli culi;

// class A

culi DIMEN_LOC(static_cast<uli>(pow(10.0, 4.0)));

class A {
public:

    // constructor

    A()
    {
        _vlocal = new double[DIMEN_LOC];
        for(uli i = 0; i != DIMEN_LOC; i++) {
            _vlocal[i] = static_cast<uli>(i);
        }
    }

    // destructor

    ~A()
    {
        delete [] _vlocal;
        _vlocal = 0;
    }

    // copy constructor

    A(const A & other)
    {
        this->_vlocal = new double[DIMEN_LOC];
        for(uli i = 0; i != DIMEN_LOC; i++) {
            this->_vlocal[i] = other._vlocal[i];
        }
    }

    // copy assignment operator

    const A & operator=(const A & other)
    {
        if(this->_vlocal != 0) {
            delete [] this->_vlocal;
            this->_vlocal = 0;
        }

        this->_vlocal = new double[DIMEN_LOC];

        for(uli i = 0; i != DIMEN_LOC; i++) {
            this->_vlocal[i] = other._vlocal[i];
        }

        return *this;
    }

private:

    double * _vlocal;
};

// the main program

int main()
{
    // local parameters and variables

    culi DIMEN(static_cast<uli>(pow(10.0, 4.0)));
    culi TRIALS(2*static_cast<uli>(pow(10.0, 2.0)));

    // main test loop

    for(uli i = 0; i != TRIALS; i++) {
        cout << "----------------------------------------------->> " << i << endl;

        vector<A> v1;

        for(uli j = 0; j != DIMEN; j++) {
            v1.push_back(A());
        }
    }

    cout << " p--> exit" << endl;
    return 0;
}

// end
