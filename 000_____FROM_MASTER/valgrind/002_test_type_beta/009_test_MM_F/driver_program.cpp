
#include <iostream>

using std::endl;
using std::cout;
using std::cin;

// base class --> Base

class Base {
public:

    // constructor

    Base() : x1(0), x2(0), x3(0)
    {
        cout << " --> constructor --> Base" << endl;
        x1 = new int (11); // this is from Base
        x2 = new int (22); // this is from Base
        x3 = new int (33); // this is from Base
    }

    // destructor

    virtual ~Base()
    {
        cout << " --> destructor --> ~Base" << endl;

        if (x1) {
            cout << " --> ~Base --> delete --> x1" << endl;
            delete x1;
            x1 = 0;
        }

        if (x2) {
            cout << " --> ~Base --> delete --> x2" << endl;
            delete x2;
            x2 = 0;
        }

        if (x3) {
            cout << " --> ~Base --> delete --> x3" << endl;
            delete x3;
            x3 = 0;
        }
    }

private:

    int * x1;
    int * x2;
    int * x3;
};

// derived class --> D1

class D1 : public Base {
public:

    // constructor

    D1() : Base(), x1(0), x2(0), x3(0)
    {
        cout << " --> constructor --> D1" << endl;
        x1 = new int (1); // this is from D1
        x2 = new int (2); // this is from D1
        x3 = new int (3); // this is from D1
    }

    // destructor

    virtual ~D1()
    {
        cout << " --> destructor --> ~D1" << endl;

        if (x1) {
            cout << " --> ~D1 --> delete --> x1" << endl;
            delete x1;
            x1 = 0;
        }

        if (x2) {
            cout << " --> ~D1 --> delete --> x2" << endl;
            delete x2;
            x2 = 0;
        }

        if (x3) {
            cout << " --> ~D1 --> delete --> x3" << endl;
            delete x3;
            x3 = 0;
        }
    }

private:

    int * x1;
    int * x2;
    int * x3;
};

// the main program

int main()
{
    // # 1

    cout << " --> Base * pb1 = new Base;" << endl;
    Base * pb1 = new Base;
    cout << " --> delete pb1;" << endl;
    delete pb1;

    // # 2

    cout << " --> Base * pb2 = new D1;" << endl;
    Base * pb2 = new D1;
    cout << " --> delete pb2;" << endl;
    delete pb2;

    // # 3

    cout << " --> D1 * pd1 = new D1;" << endl;
    D1 * pd1 = new D1;
    cout << " --> delete pd1;" << endl;
    delete pd1;

    cout << " ----------------------------> EXIT HERE(Nothing will happen)" << endl;

    return 0;
}

// end
