//============//
// shared_ptr //
//============//

// NO LEAKS
// different behaviour under different version of the same compiler

#include <iostream>
#include <memory>
#include <cmath>
#include <list>

using std::cout;
using std::endl;
using std::shared_ptr;
using std::pow;
using std::list;
using std::puts;
using std::free;

// type definitions

typedef unsigned long int uli;
typedef const uli culi;

// overlod the new operator

void* operator new(size_t sz) throw(std::bad_alloc)
{
    static uli index(0);
    index++;
    cout << " --> inside new overloaded operator, index = " << index << endl;
    return malloc(sz);
}

// overload the delete operator

void operator delete(void* ptr) noexcept
{

    static uli index(0);
    index++;
    cout << " --> inside delete overloaded operator, index = " << index << endl;
    if(ptr != 0) {
        cout << " --> overloaded delete, index = " << index << endl;
        free(ptr);
        ptr = 0;
    }
}

// deleter

template<typename T>
struct my_deleter {

    // a verbose array deleter

    void operator()(T * p)
    {

        static uli index(0);
        index++;

        cout << " --> inside my_deleter --> index = " << index << endl;

        if(p != 0) {
            cout << " --> my_deleter called --> index = " << index << endl;
            delete [] p;
            p = 0;
        }
    }
};

// the main function

int main()
{
    culi	DIMEN(2*static_cast<uli>(pow(10.0, 0.0)));
    culi	TRIALS(2*static_cast<uli>(pow(10.0, 0.0)));

    // the pointer

    cout << " --> here -----------------------------> 1" << endl;

    double * p = new double(10.0);

    // the vector

    cout << " --> here -----------------------------> 2" << endl;

    list<shared_ptr<double>> lsp;
    lsp.resize(0);

    // the shared pointer

    cout << " --> here -----------------------------> 3" << endl;

    shared_ptr<double> sp(p, my_deleter<double>());

    cout << " --> here -----------------------------> 4" << endl;

    // build the list

    for(uli k = 0; k != TRIALS; k++) {
        for(uli i = 0; i != DIMEN; i++) {
            cout << " --> loop --> A --> " << k << ", " << i << endl;
            cout << " --> push_back --> now" << endl;
            lsp.push_back(sp);
            cout << " --> push_back --> now" << endl;
            lsp.push_back(sp);
            cout << " --> lsp.size() --> " << lsp.size() << endl;
        }
    }

    cout << " --> here -----------------------------> 6" << endl;

    lsp.clear();

    cout << " --> here -----------------------------> 7 --> end" << endl;

    return 0;
}

// end
