//=======================//
// functions declaration //
//=======================//

#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include "parameters.hpp"
#include "vec.hpp"

// function --> fun1

template<typename T>
Vec<T> fun1();

#endif // FUNCTIONS_H
