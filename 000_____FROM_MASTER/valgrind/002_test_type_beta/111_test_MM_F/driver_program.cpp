//===================//
// garbage collector //
//===================//

#include <iostream>
#include <memory>
#include <map>
#include <cmath>

using std::cout;
using std::endl;
using std::map;
using std::shared_ptr;
using std::pow;

//==================//
// type definitions //
//==================//

typedef unsigned long int uli;
typedef const uli culi;

//=========================//
// class declaration --> A //
//=========================//

class A {
public:

    A();
    virtual ~A();
    static map<uli, shared_ptr<A>> _garbage;
    uli get_index() const;

private:

    static uli _index;
    uli _elem;
};

//========================//
// class definition --> A //
//========================//

// static member initialization

uli A::_index = 0;
map<uli, shared_ptr<A>> A::_garbage;

// constructor

A::A() : _elem(++_index) {}

// destructor

A::~A()
{
    _garbage[this->get_index()].reset();
}

// member functions

uli A::get_index() const
{
    return this->_elem;
}

//=========================//
// class declaration --> B //
//=========================//

class B : public A {
public:

    B();
    ~B();
};

//========================//
// class definition --> B //
//========================//

// constructor

B::B() {}

// destructor

B::~B() {}

//==================//
// the main program //
//==================//

int main()
{
    culi SIZE(static_cast<uli>(pow(10.0, 5.0)));
    culi TRIALS(1*static_cast<uli>(pow(10.0, 2.0)));

    for(uli j = 0; j != TRIALS; j++) {
        cout << "-------------------------------------------------------->> " << j << endl;

        for(uli i = 0; i != SIZE; i++) {
            A * pb = new B;

            // garbage collection

            shared_ptr<A> spa;
            spa.reset(pb);
            A::_garbage[pb->get_index()] = spa;
        }
    }

    return 0;
}

// end
