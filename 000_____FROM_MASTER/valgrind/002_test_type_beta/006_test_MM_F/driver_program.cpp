
#include <iostream>

using std::endl;
using std::cout;
using std::cin;

// base class --> Base

class Base {
public:

    // constructor

    Base() : x1(0), x2(0), x3(0)
    {
        cout << " --> constructor --> Base" << endl;
    }

    // destructor

    virtual ~Base()
    {
        cout << " --> destructor --> ~Base" << endl;

        if (x1) {
            cout << " --> ~Base --> delete --> x1" << endl;
            delete x1;
            x1 = 0;
        }

        if (x2) {
            cout << " --> ~Base --> delete --> x2" << endl;
            delete x2;
            x2 = 0;
        }

        if (x3) {
            cout << " --> ~Base --> delete --> x3" << endl;
            delete x3;
            x3 = 0;
        }
    }

protected:

    int * x1;
    int * x2;
    int * x3;
};

// derived class --> D1

class D1 : public Base {
public:

    // constructor

    D1() : Base(), x2(0)
    {
        cout << " --> constructor --> D1" << endl;
        x1 = new int (1); // this is from Base
    }

    // destructor

    virtual ~D1()
    {
        cout << " --> destructor --> ~D1" << endl;
        if (x2) {
            cout << " --> ~D1 --> delete --> x2" << endl;
            delete x2;
            x2 = 0;
        }
    }

protected:

    int * x2;
};

// derived class --> D2

class D2 : public D1 {
public:

    // constructor

    D2() : D1(), x3(0)
    {
        cout << " --> constructor --> D2" << endl;
        x2 = new int (2); // this is from D1
    }

    // destructor

    virtual ~D2()
    {
        cout << " --> destructor --> ~D2" << endl;
        if (x3) {
            cout << " --> ~D2 --> delete --> x3" << endl;
            delete x3;
            x3 = 0;
        }
    }

protected:

    int * x3;
};

// derived class --> D3

class D3 : public D2 {
public:

    // constructor

    D3() : D2()
    {
        cout << " --> constructor --> D3" << endl;
        x3 = new int (3); // this is from D2
    }

    // destructor

    virtual ~D3()
    {
        cout << " --> destructor --> ~D3" << endl;
    }
};

// the main program

int main()
{
    // # 1

    cout << " --> Base * pb1 = new D3;" << endl;
    Base * pb1 = new D3;
    cout << " --> delete pb1;" << endl;
    delete pb1;

    // # 2

    cout << " --> D1 * pd1 = new D3;" << endl;
    D1 * pd1 = new D3;
    cout << " --> delete pd1;" << endl;
    delete pd1;

    // # 3

    cout << " --> D2 * pd2 = new D3;" << endl;
    D2 * pd2 = new D3;
    cout << " --> delete pd2;" << endl;
    delete pd2;

    // # 4

    cout << " --> D3 * pd3 = new D3;" << endl;
    D3 * pd3 = new D3;
    cout << " --> delete pd3;" << endl;
    delete pd3;

    cout << " ----------------------------> EXIT HERE(Nothing will happen)" << endl;

    return 0;
}

// end
