//=========================================//
// 'new' and 'delete' overloaded operators //
//=========================================//

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

//=====================================//
// class-specific allocation functions //
//=====================================//

// class X declaration

class X {
public:

    // new
    static void* operator new(size_t);

    // new []
    static void* operator new[](size_t);

    // delete
    static void operator delete(void* ptr);

    // delete []
    static void operator delete[](void* ptr);
};

// class X efinition

// X::new
void * X::operator new(size_t sz)
{
    cout << "custom new for size " << sz << endl;
    return ::operator new(sz);
}

// X::new[]
void* X::operator new[](size_t sz)
{
    cout << "custom new for size " << sz << endl;
    return ::operator new(sz);
}

// X::delete
void X::operator delete(void* ptr)
{
    cout << "custom placement delete called" << endl;
    ::operator delete(ptr);
}

// X::delete []

void X::operator delete[](void* ptr)
{
    cout << "custom placement delete [] called" << endl;
    ::operator delete(ptr);
}

// the main function

int main()
{
    X* p1 = new X;
    delete p1;
    X* p2 = new X[10];
    delete[] p2;

    double * p3 = new double;
    delete p3;

    double * p4 = new double [10];
    delete p4;

    return 0;
}

// END
