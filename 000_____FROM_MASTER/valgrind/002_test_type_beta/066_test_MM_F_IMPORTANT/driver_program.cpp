//=============================================//
// delete heap via shared pointers 		  //
// and static shared pointer garbage collector //
// the classes are friends to garbage collector//
// NO LEAK!                        		  //
//=============================================//

#include <iostream>
#include <memory>
#include <vector>
#include <cmath>

using std::cin;
using std::cout;
using std::endl;
using std::shared_ptr;
using std::vector;
using std::pow;

//==================//
// type definitions //
//==================//

typedef unsigned long int uli;

// forward declaration

template<typename T>
class GarbageCollector;

//================//
// 'new' overload //
//================//

// new

template<typename T>
void* operator new(uli sz, GarbageCollector<T> & trash) throw (std::bad_alloc)
{
    //cout << "global op new called, size = " << sz << endl;
    void * local1 = malloc(sz);
    T * local2 = (T*)local1;
    shared_ptr<T> spd(local2);
    trash._garbage.push_back(spd);
    return local1;
}

// delete

template<typename T>
void operator delete(void* ptr, GarbageCollector<T> &trash) noexcept
{
    cout << " --> global op delete called" << endl;
    free(ptr);
}

//======================================//
// class 'GarbageCollector' declaration //
//======================================//

template<typename T>
class GarbageCollector {
public:
    virtual void freeRAM() const;
protected:
    static vector<shared_ptr<T>> _garbage;
private:
    template<typename K>
    friend void* operator new(uli, GarbageCollector<K>&) throw (std::bad_alloc);
};

//=====================================//
// class 'GarbageCollector' definition //
//=====================================//

// static member initialization

template<typename T>
vector<shared_ptr<T>> GarbageCollector<T>::_garbage;

// member function --> freeRAM

template<typename T>
void GarbageCollector<T>::freeRAM() const
{
    _garbage.clear();
    _garbage.shrink_to_fit();
}

//======================//
// class A1 declaration //
//======================//

template<typename T>
class A1 : public GarbageCollector<T> {
public:

    // constructor

    A1();

    // destructor

    virtual ~A1();

    // member function

    template<typename K>
    K fun();
};

//=====================//
// class A1 definition //
//=====================//

// constructor

template<typename T>
A1<T>::A1() {}

// destructor

template<typename T>
A1<T>::~A1() {}

// member function

template<typename T>
template<typename K>
K A1<T>::fun()
{
    const uli IMAX = static_cast<uli>(pow(10.0, 4.0));
    K val;
    for (uli i = 0; i != IMAX; i++) {
        K * pd = new (*this) K(i); // use the overloaded operator
        val = *pd; // for testing purposes
    }

    return val;
}

//======================//
// class A2 declaration //
//======================//

template<typename T>
class A2 : public GarbageCollector<T> {
public:

    // constructor

    A2();

    // destructor

    virtual ~A2();

    // member function

    template<typename K>
    K fun();
};

//=====================//
// class A2 definition //
//=====================//

// constructor

template<typename T>
A2<T>::A2() {}

// destructor

template<typename T>
A2<T>::~A2() {}

// member function

template<typename T>
template<typename K>
K A2<T>::fun()
{
    const uli IMAX = static_cast<uli>(pow(10.0, 4.0));
    K val;
    for (uli i = 0; i != IMAX; i++) {
        K * pd = new (*this) K(i); // use the overloaded operator
        val = *pd;
    }

    return val;
}

//==================//
// the main program //
//==================//

int main ()
{
    // local parameters and variables

    const uli IMAX = 5UL;
    const uli DO_MAX = 10;
    double vD;
    long double vLD;

    for (uli k = 0; k != DO_MAX; k++) {

        cout << " -------------------------------------------------------->> k = " << k << endl;

        // # 1

        cout << " --> declare an object of type A1<shared_ptr<double>>" << endl;

        A1<double> a1;

        cout << " --> call the 'fun' member function many times" << endl;

        for (uli i = 0; i != IMAX; i++) {
            vD = a1.fun<double>();
        }

        cout << " --> vD = " << vD << endl;

        cout << " --> empty trash bin ..." << endl;

        a1.freeRAM();

        // # 2

        cout << " --> declare an object of type A2<shared_ptr<long double>>" << endl;

        A2<long double> a2;

        cout << " --> call the 'fun' member function many times" << endl;

        for (uli i = 0; i != IMAX; i++) {
            vLD = a2.fun<long double>();
        }

        cout << " --> vLD = " << vLD << endl;
        cout << " --> empty trash bin ..." << endl;

        a2.freeRAM();

        // # 3

        cout << " --> declare an object of type A2<shared_ptr<long double>>" << endl;

        A2<long double> a3;

        cout << " --> call the 'fun' member function many times" << endl;

        for (uli i = 0; i != IMAX; i++) {
            vLD = a3.fun<long double>();
        }

        cout << " --> vLD = " << vLD << endl;

        cout << " --> empty trash bin ..." << endl;

        a3.freeRAM();

    }

    return 0;
}

// END
