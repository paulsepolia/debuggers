//=============================================//
// delete heap via shared pointers 		  //
// and static shared pointer garbage collector //
//=============================================//

#include "type_definitions.hpp"
#include "garbage_collector.hpp"
#include "a1.hpp"
#include "a2.hpp"
#include <iostream>
#include <cmath>
using std::cin;
using std::cout;
using std::endl;
using std::pow;

//==================//
// the main program //
//==================//

int main ()
{
    // local parameters and variables

    const uli IMAX1 = 2UL;
    const uli IMAX2 = static_cast<uli>(pow(10.0, 4.0));
    const uli DO_MAX = 3;
    double vD;
    long double vLD;

    for (uli k = 0; k != DO_MAX; k++) {

        cout << " -------------------------------------------------------->> k = " << k << endl;

        // # 1

        cout << " --> declare an object of type A1<double>" << endl;

        A1<double> a1;

        cout << " --> call the 'fun' member function many times" << endl;

        for (uli i = 0; i != IMAX1; i++) {
            vD = a1.fun<double>(IMAX2);
        }

        cout << " --> vD = " << vD << endl;

        cout << " --> empty trash bin ..." << endl;

        a1.freeRAM();

        // # 2

        cout << " --> declare an object of type A2<long double>" << endl;

        A2<long double> a2;

        cout << " --> call the 'fun' member function many times" << endl;

        for (uli i = 0; i != IMAX1; i++) {
            vLD = a2.fun<long double>(IMAX2);
        }

        cout << " --> vLD = " << vLD << endl;
        cout << " --> empty trash bin ..." << endl;

        a2.freeRAM();

        // # 3

        cout << " --> declare an object of type A2<long double>" << endl;

        A2<long double> a3;

        cout << " --> call the 'fun' member function many times" << endl;

        for (uli i = 0; i != IMAX1; i++) {
            vLD = a3.fun<long double>(IMAX2);
        }

        cout << " --> vLD = " << vLD << endl;

        cout << " --> empty trash bin ..." << endl;

        a3.freeRAM();
    }

    return 0;
}

// END
