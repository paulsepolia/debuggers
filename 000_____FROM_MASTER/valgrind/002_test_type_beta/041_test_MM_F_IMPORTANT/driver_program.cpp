//=================================//
// delete heap via shared pointers //
// NO LEAK!                        //
//=================================//

#include <iostream>
#include <memory>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::shared_ptr;
using std::vector;

static vector<shared_ptr<double>> garbage;

//==================//
// the main program //
//==================//

int main ()
{
    const int IMAX = 10;
    shared_ptr<double> sp1;

    for (int i = 0; i != IMAX; i++) {
        cout << " ----------------------------------------------->> i = " << i << endl << endl;
        double * p1 = new double(i);
        sp1.reset(p1);
        garbage.push_back(sp1);
        cout << " -->  p1 = " <<  p1 << endl;
        cout << " --> &p1 = " << &p1 << endl;
        cout << " --> *p1 = " << *p1 << endl;
        cout << endl;
    }

    // test if the values are still there
    // YES THERE ARE!

    for (int i = 0; i != IMAX; i++) {
        cout << " ----------------------------------------------->> i = " << i << endl << endl;
        cout << " -->  garbage[i]       = " <<  garbage[i] << endl;
        cout << " -->  garbage[i].get() = " <<  garbage[i].get() << endl;
        cout << " --> *garbage[i].get() = " << *garbage[i].get() << endl;

        cout << endl;
    }

    return 0;
}

// END
