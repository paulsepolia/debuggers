//========//
// void * //
//========//

#include <iostream>
#include <iomanip>
using std::cin;
using std::cout;
using std::endl;
using std::setprecision;
using std::fixed;

//==================//
// the main program //
//==================//

int main ()
{
    cout << fixed;
    cout << setprecision(10);

    void * p1 = new double(11.11);
    void * p2 = new int(22.22);

    cout << "--->> void*" << endl;

    cout << "   p1 = " <<  p1 << endl;
    //cout << " *p1 = " << *p1 << endl; // ERROR: it is not a pointer OR a complete type object
    cout << "  &p1 = " << &p1 << endl;
    cout << "   p2 = " <<  p2 << endl;
    //cout << " *p2 = " << *p2 << endl; // ERROR: it is not a pointer OR a complete type object
    cout << "  &p2 = " << &p2 << endl;

    cout << "--->> type*" << endl;

    double * pd1 = new double(10.0);
    int * pi1 = new int(20);

    cout << "  pd1 = " <<  pd1 << endl;
    cout << " *pd1 = " << *pd1 << endl;
    cout << " &pd1 = " << &pd1 << endl;
    cout << "  pi1 = " <<  pi1 << endl;
    cout << " *pi1 = " << *pi1 << endl;
    cout << " &pi1 = " << &pi1 << endl;

    cout << "--->> cast --> c-style cast" << endl;

    pd1 = (double*)(p1);
    pi1 = (int*)(p2);

    cout << "  pd1 = " <<  pd1 << endl;
    cout << " *pd1 = " << *pd1 << endl;
    cout << " &pd1 = " << &pd1 << endl;
    cout << "  pi1 = " <<  pi1 << endl;
    cout << " *pi1 = " << *pi1 << endl;
    cout << " &pi1 = " << &pi1 << endl;

    cout << "--->> cast --> static_cast" << endl;

    pd1 = static_cast<double*>(p1);
    pi1 = static_cast<int*>(p2);

    cout << "  pd1 = " <<  pd1 << endl;
    cout << " *pd1 = " << *pd1 << endl;
    cout << " &pd1 = " << &pd1 << endl;
    cout << "  pi1 = " <<  pi1 << endl;
    cout << " *pi1 = " << *pi1 << endl;
    cout << " &pi1 = " << &pi1 << endl;

    cout << "--->> cast --> dynamic_cast --> can not be done" << endl;

    //pd1 = dynamic_cast<double*>(p1); // ERROR: target is not pointer or reference to a class
    //pi1 = dynamic_cast<int*>(p2); // ERROR: target is not pointer or reference to a class

    cout << "--->> cast --> reinterpret_cast" << endl;

    pd1 = reinterpret_cast<double*>(p1);
    pi1 = reinterpret_cast<int*>(p2);

    cout << "  pd1 = " <<  pd1 << endl;
    cout << " *pd1 = " << *pd1 << endl;
    cout << " &pd1 = " << &pd1 << endl;
    cout << "  pi1 = " <<  pi1 << endl;
    cout << " *pi1 = " << *pi1 << endl;
    cout << " &pi1 = " << &pi1 << endl;

    return 0;
}

// END
