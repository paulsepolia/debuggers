//=============================================//
// delete heap via shared pointers 		  //
// and static shared pointer garbage collector //
// common to all derived and base  classes     //
// NO LEAK!                        		  //
//=============================================//

#include <iostream>
#include <memory>
#include <vector>
#include <cmath>

using std::cin;
using std::cout;
using std::endl;
using std::shared_ptr;
using std::vector;
using std::pow;

//==================//
// type definitions //
//==================//

typedef unsigned long int uli;

//=====================//
// class A declaration //
//=====================//

template<typename T>
class A {
public:

    // constructor

    A();

    // destructor

    virtual ~A();

    // member function

    virtual void fun1() const;

protected:

    static vector<shared_ptr<T>> _garbage;
};

//====================//
// class A definition //
//====================//

// static member initialization

template <typename T>
vector<shared_ptr<T>> A<T>::_garbage;

// constructor

template <typename T>
A<T>::A() {}

// destructor

template <typename T>
A<T>::~A() {}

// member function

template <typename T>
void A<T>::fun1() const
{
    const uli IMAX = static_cast<uli>(pow(10.0, 4.0));
    for (uli i = 0; i != IMAX; i++) {
        T * pd = new T(i);
        shared_ptr<T> spd(pd);
        _garbage.push_back(spd);
    }
}

//=====================//
// class B declaration //
//=====================//

template<typename T>
class B : public A<T> {
public:

    // constructor

    B();

    // destructor

    virtual ~B();

    // member fuction

    virtual void fun1() const;
};

//====================//
// class B definition //
//====================//

// constructor

template<typename T>
B<T>::B(): A<T>() {}

// destructor

template<typename T>
B<T>::~B() {}

// member function

template<typename T>
void B<T>::fun1() const
{
    const uli IMAX = static_cast<uli>(pow(10.0, 4.0));
    for (uli i = 0; i != IMAX; i++) {
        T * pd = new T(i);
        shared_ptr<T> spd(pd);
        A<T>::_garbage.push_back(spd);
    }
}

//=====================//
// class C declaration //
//=====================//

template<typename T>
class C : public B<T> {
public:

    // constructor

    C();

    // destructor

    virtual ~C();

    // member fuction

    virtual void fun1() const;
};

//====================//
// class C definition //
//====================//

// constructor

template<typename T>
C<T>::C(): B<T>() {}

// destructor

template<typename T>
C<T>::~C() {}

// member function

template<typename T>
void C<T>::fun1() const
{
    const uli IMAX = static_cast<uli>(pow(10.0, 4.0));
    for (uli i = 0; i != IMAX; i++) {
        T * pd = new T(i);
        shared_ptr<T> spd(pd);
        A<T>::_garbage.push_back(spd);
    }
}

//==================//
// the main program //
//==================//

int main ()
{
    // local parameters and variables

    const uli IMAX = 1000UL;

    // main game

    cout << " --> declare an object of type A" << endl;

    A<double> a1;

    cout << " --> call the 'fun1' member function many times" << endl;

    for (uli i = 0; i != IMAX; i++) {
        a1.fun1();
    }

    cout << " --> declare an object of type B" << endl;

    B<double> b1;

    cout << " --> call the 'fun1' member function many times" << endl;

    for (uli i = 0; i != IMAX; i++) {
        b1.fun1();
    }

    cout << " --> declare an object of type C" << endl;

    C<double> c1;

    cout << " --> call the 'fun1' member function many times" << endl;

    for (uli i = 0; i != IMAX; i++) {
        c1.fun1();
    }

    return 0;
}

// END
