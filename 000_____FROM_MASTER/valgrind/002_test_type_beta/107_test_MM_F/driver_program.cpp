//===================//
// garbage collector //
//===================//

#include <iostream>
#include <memory>
#include <vector>
#include <cmath>

using std::cout;
using std::endl;
using std::vector;
using std::shared_ptr;
using std::pow;

//==================//
// type definitions //
//==================//

typedef unsigned long int uli;
typedef const uli culi;

//=========================//
// class declaration --> A //
//=========================//

class A {
public:

    A();
    virtual ~A();
    static vector<shared_ptr<A>> _garbage;
};

//========================//
// class definition --> A //
//========================//

// static member initialization

vector<shared_ptr<A>> A::_garbage {};

// constructor

A::A() {}

// destructor

A::~A() {}

//==================//
// the main program //
//==================//

int main()
{
    culi SIZE(static_cast<uli>(pow(10.0, 7.0)));
    culi TRIALS(static_cast<uli>(pow(10.0, 1.0)));

    for(uli j = 0; j != TRIALS; j++) {
        cout << "-------------------------------------------------------->> " << j << endl;

        for(uli i = 0; i != SIZE; i++) {
            A * pa = new A;

            // garbage collection

            shared_ptr<A> spa;
            spa.reset(pa);
            A::_garbage.push_back(spa);
        }

        // empty the garbage vector

        A::_garbage.clear();
        A::_garbage.shrink_to_fit();
    }

    return 0;
}

// end
