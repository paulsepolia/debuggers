//================//
// shared pointer //
//================//

#include <iostream>
#include <memory>

using std::cin;
using std::cout;
using std::endl;
using std::shared_ptr;

// function

shared_ptr<double>  fun1()
{
    shared_ptr<double> p1;
    p1.reset(new double(12.34));

    return p1;
}

//==================//
// the main program //
//==================//

int main ()
{
    const int IMAX = 10;

    for (int i = 0; i != IMAX; i++) {
        cout << "---------------------------------------------->> " << i << endl;

        cout << " -->  fun1().get() = " << fun1().get() << endl;
        cout << " --> *fun1().get() = " << *fun1().get() << endl;

    }

    return 0;
}

// END
