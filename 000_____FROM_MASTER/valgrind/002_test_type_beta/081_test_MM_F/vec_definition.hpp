//======================//
// class Vec definition //
//======================//

#include "vec_declaration.hpp"
#include <utility>
#include <iostream>

using std::move;
using std::cout;
using std::endl;

// constructor

template<typename T>
Vec<T>::Vec() : _size(0), _p() {}

// destructor

template<typename T>
Vec<T>::~Vec()
{
    cout << " --> destructor --> 1" << endl;
    cout << " --> this->_p.get() = " << this->_p.get() << endl;

    if(_p) {
        _p.reset();
    }

    cout << " --> destructor --> 2" << endl;
    cout << " --> this->_p.get() = " << this->_p.get() << endl;
}

// copy constructor

template<typename T>
Vec<T>::Vec(const Vec<T> & other)
{
    cout << " --> copy constructor --> 1" << endl;
    cout << " --> this->_p.get() = " << this->_p.get() << endl;
    cout << " --> other._p.get() = " << other._p.get() << endl;

    if(this != &other) {
        if(other._p) {
            this->deallocate();
            this->allocate(other._size);
            for(uli i = 0; i != other._size; i++) {
                this->set(i, other.get(i));
            }
        } else if(other._p.get() == 0) {
            this->deallocate();
        }
    }

    cout << " --> copy constructor --> 2" << endl;
    cout << " --> this->_p.get() = " << this->_p.get() << endl;
    cout << " --> other._p.get() = " << other._p.get() << endl;
}

// move constructor

template<typename T>
Vec<T>::Vec(Vec<T> && other)
{
    cout << " --> move constructor --> 1" << endl;
    cout << " --> this->_p.get() = " << this->_p.get() << endl;
    cout << " --> other._p.get() = " << other._p.get() << endl;

    this->_p = move(other._p);

    cout << " --> move constructor --> 2" << endl;
    cout << " --> this->_p.get() = " << this->_p.get() << endl;
    cout << " --> other._p.get() = " << other._p.get() << endl;
}

// copy assignment operator

template<typename T>
Vec<T> & Vec<T>::operator=(const Vec<T> & other)
{
    cout << " --> copy operator= --> 1" << endl;
    cout << " --> this->_p.get() = " << this->_p.get() << endl;
    cout << " --> other._p.get() = " << other._p.get() << endl;

    if(this != &other) {
        if(other._p) {
            this->deallocate();
            this->allocate(other._size);
            for(uli i = 0; i != other._size; i++) {
                this->set(i, other.get(i));
            }
        } else if(other._p.get() == 0) {
            this->deallocate();
        }
    }

    cout << " --> copy operator= --> 2" << endl;
    cout << " --> this->_p.get() = " << this->_p.get() << endl;
    cout << " --> other._p.get() = " << other._p.get() << endl;

    return *this;
}

// move assignment operator

template<typename T>
Vec<T> & Vec<T>::operator=(Vec<T> && other)
{

    cout << " --> move operator= --> 1" << endl;
    cout << " --> this->_p.get() = " << this->_p.get() << endl;
    cout << " --> other._p.get() = " << other._p.get() << endl;

    this->deallocate();
    this->allocate(other._size);
    this->_p = move(other._p);

    cout << " --> move operator= --> 2" << endl;
    cout << " --> this->_p.get() = " << this->_p.get() << endl;
    cout << " --> other._p.get() = " << other._p.get() << endl;

    return *this;
}

//==================//
// member functions //
//==================//

// allocate

template<typename T>
void Vec<T>::allocate(const uli & val_size)
{
    this->deallocate();
    this->_p.reset(new T [val_size]);
    this->_size = val_size;
}

// deallocate

template<typename T>
void Vec<T>::deallocate()
{
    cout << " --> deallocate --> 1" << endl;
    cout << " --> this->_p.get() = " << this->_p.get() << endl;

    if(this->_p) {
        this->_p.reset();
    }

    cout << " --> deallocate --> 2" << endl;
    cout << " --> this->_p.get() = " << this->_p.get() << endl;
}

// get

template<typename T>
T Vec<T>::get(const uli & index) const
{
    return this->_p.get()[index];
}

// set

template<typename T>
void Vec<T>::set(const uli & index, const T & val)
{
    this->_p.get()[index] = val;
}

// operator[]

template<typename T>
T Vec<T>::operator[](const uli & index)
{
    return this->get(index);
}

// end
