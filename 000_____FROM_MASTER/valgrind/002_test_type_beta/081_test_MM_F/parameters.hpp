//===================//
// parameters header //
//===================//

#ifndef PARAMETERS_H
#define PARAMETERS_H

#include <cmath>
using std::pow;

typedef unsigned long int uli;
const uli DIMEN = static_cast<uli>(pow(10.0, 8.0));
const uli TRIALS = 10*static_cast<uli>(pow(10.0, 0.0));

#endif // PARAMETERS_H
