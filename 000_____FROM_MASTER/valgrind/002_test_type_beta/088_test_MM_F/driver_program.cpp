//=======================//
// use of move symantics //
//=======================//

#include <iostream>
#include <vector>
#include <new>
#include <cmath>
#include <iomanip>
#include <chrono>

using std::cout;
using std::endl;
using std::vector;
using std::move;
using std::pow;
using std::boolalpha;
using namespace std::chrono;

// type definitions

typedef unsigned long int uli;
typedef const uli culi;

// the main program

int main()
{
    // local parameters

    culi TRIALS(static_cast<culi>(pow(10.0, 5.0)));
    culi DIMEN(static_cast<culi>(pow(10.0, 4.0)));
    cout << boolalpha;
    auto t1 = system_clock::now();
    auto t2 = system_clock::now();
    duration<double> time_span;

    // main game

    vector<double> v1;

    v1.clear();
    v1.resize(DIMEN);
    v1.shrink_to_fit();

    cout << " v1.size() = " << v1.size() << endl;

    vector<double> v2;
    v2.clear();
    v2.shrink_to_fit();

    cout << " v2.size() = " << v2.size() << endl;

    cout << " v2 = v1;" << endl;

    t1 = system_clock::now();

    uli i = 0;
    while(i != TRIALS) {
        v2 = v1;
        i++;
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    cout << " i = " << i << endl;
    cout << " v1.size() = " << v1.size() << endl;
    cout << " v2.size() = " << v2.size() << endl;
    cout << " (v1 == v2) = " << (v1 == v2) << endl;

    t1 = system_clock::now();

    i = 0;
    while(i != TRIALS) {
        v2 = move(v1);
        v1 = move(v2);
        i++;
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    cout << " i = " << i << endl;
    cout << " v1.size() = " << v1.size() << endl;
    cout << " v2.size() = " << v2.size() << endl;
    cout << " (v1 == v2) = " << (v1 == v2) << endl;


    return 0;
}

// end
