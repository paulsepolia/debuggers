//==================//
// use of allocator //
//==================//

#include <iostream>
#include <memory>
#include <cmath>
#include <iomanip>

using std::cout;
using std::endl;
using std::allocator;
using std::pow;
using std::boolalpha;
using std::unique_ptr;

// type definitions

typedef unsigned long int uli;
typedef const uli culi;

// the main program

int main()
{
    cout << boolalpha;

    // local parameters

    culi DIMEN(static_cast<culi>(pow(10.0, 2.0)));

    // main game

    // allocate an array of pointers to double

    allocator<unique_ptr<double>> updal;
    allocator<double> dal;

    // allocate --> step --> 1/2

    unique_ptr<double>* arr1 = updal.allocate(DIMEN);
    unique_ptr<double>* arr2 = updal.allocate(DIMEN);

    // allocate --> step --> 2/2

    for(uli i = 0; i != DIMEN; i++) {
        arr1[i].reset(dal.allocate(DIMEN));
        arr2[i].reset(dal.allocate(DIMEN));
    }

    // build arrays --> arr1 and arr2

    for(uli i = 0; i != DIMEN; i++) {
        for(uli j = 0; j != DIMEN; j++) {
            arr1[i].get()[j] = static_cast<double>(i+j);
            arr2[i].get()[j] = static_cast<double>(i+j+1);
        }
    }

    // some output

    cout << " --> arr1[0].get()[0] = " << arr1[0].get()[0] << endl;
    cout << " --> arr1[1].get()[1] = " << arr1[1].get()[1] << endl;
    cout << " --> arr1[2].get()[2] = " << arr1[2].get()[2] << endl;

    cout << " --> arr2[0].get()[0] = " << arr2[0].get()[0] << endl;
    cout << " --> arr2[1].get()[1] = " << arr2[1].get()[1] << endl;
    cout << " --> arr2[2].get()[2] = " << arr2[2].get()[2] << endl;

    cout << " (arr1 == arr2) = " << (arr1 == arr2) << endl;

    cout << " --> updal.address(arr1[0]) = " << updal.address(arr1[0]) << endl;
    cout << " --> updal.address(arr1[1]) = " << updal.address(arr1[1]) << endl;
    cout << " --> updal.address(arr1[2]) = " << updal.address(arr1[2]) << endl;

    cout << " --> updal.address(arr2[0]) = " << updal.address(arr2[0]) << endl;
    cout << " --> updal.address(arr2[1]) = " << updal.address(arr2[1]) << endl;
    cout << " --> updal.address(arr2[2]) = " << updal.address(arr2[2]) << endl;

    // deallocate --> 1

    for(uli i = 0; i != DIMEN; i++) {
        dal.deallocate(arr1[i].get(), DIMEN);
        dal.deallocate(arr2[i].get(), DIMEN);
    }

    // deallocate --> 2
    // WORKS OK with unique pointers
    // AND IT IS NEEDED EVEN IF THE POINTERS ARE UNIQUE

    updal.deallocate(arr1, DIMEN);
    updal.deallocate(arr2, DIMEN);

    return 0;
}

// end
