//======================//
// functions definition //
//======================//

#include "functions_declaration.hpp"
#include "vec.hpp"
#include <iostream>
#include <utility>
#include <cmath>
using std::cout;
using std::endl;
using std::move;
using std::pow;

// function --> fun1

template<typename T>
Vec<T> fun1()
{
    Vec<T> v_local;
    v_local.allocate(DIMEN);

    for(uli i = 0; i != DIMEN; i++) {
        v_local.set(i, static_cast<T>(i));
    }

    return v_local;
}

// end
