//============//
// shared_ptr //
//============//

#include <iostream>
#include <memory>
#include <cmath>

using std::cout;
using std::endl;
using std::unique_ptr;
using std::pow;

// type definitions

typedef unsigned long int uli;
typedef const uli culi;

// the main function

int main()
{
    culi	DIMEN(static_cast<uli>(pow(10.0, 2.0)));
    culi	TRIALS(static_cast<uli>(pow(10.0, 1.0)));

    for(uli k = 0; k != TRIALS; k++) {
        for(uli i = 0; i != DIMEN; i++) {
            double * p = new double(i);
            unique_ptr<double> sp1(p);
            p = new double(i);
            unique_ptr<double> sp2(p);
        }
    }

    return 0;
}

// end
