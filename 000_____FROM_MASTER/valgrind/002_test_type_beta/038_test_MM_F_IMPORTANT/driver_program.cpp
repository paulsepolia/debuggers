//================//
// shared pointer //
//================//

#include <iostream>
#include <memory>

using std::cin;
using std::cout;
using std::endl;
using std::shared_ptr;

// function

template <typename T>
class arrayDeleter {
public:
    void operator()(const T * p)
    {
        delete [] p;
    }
};

//==================//
// the main program //
//==================//

int main ()
{
    const int SIZE = 1000;
    const int IMAX = 10;
    shared_ptr<double> p1;

    for (int i = 0; i != IMAX; i++) {
        cout << "---------------------------------------------->> " << i << endl;
        p1.reset(new double[SIZE], arrayDeleter<double>());
        for (int j = 0; j != SIZE; j++) {
            p1.get()[j] = static_cast<double>(j);
        }
        cout << " --> p1.get()[i] = " << p1.get()[i] << endl;
        p1.reset(); // it is not nessecary
    }

    return 0;
}

// END
