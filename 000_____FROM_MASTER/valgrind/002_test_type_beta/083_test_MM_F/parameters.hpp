//===================//
// parameters header //
//===================//

#ifndef PARAMETERS_H
#define PARAMETERS_H

#include <cmath>
#include <string>
using std::pow;
using std::string;

typedef unsigned long int uli;
const uli DIMEN = static_cast<uli>(pow(10.0, 2.0));
const uli TRIALS = 10*static_cast<uli>(pow(10.0, 0.0));
const string ERROR_001 = "ERROR:001:THE VECTOR IS NOT ALLOCATED";
const string ERROR_002 = "ERROR:002:INVALID INDEX";

#endif // PARAMETERS_H
