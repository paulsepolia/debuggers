//========//
// void * //
//========//

#include <iostream>
#include <iomanip>
using std::cin;
using std::cout;
using std::endl;
using std::setprecision;
using std::fixed;

//==================//
// the main program //
//==================//

int main ()
{
    cout << fixed;
    cout << setprecision(10);

    void * p1 = new char('a');
    void * p2 = new char('b');

    cout << "--->> void*" << endl;

    cout << "   p1 = " <<  p1 << endl;
    //cout << " *p1 = " << *p1 << endl; // ERROR: it is not a pointer OR a complete type object
    cout << "  &p1 = " << &p1 << endl;
    cout << "   p2 = " <<  p2 << endl;
    //cout << " *p2 = " << *p2 << endl; // ERROR: it is not a pointer OR a complete type object
    cout << "  &p2 = " << &p2 << endl;

    cout << "--->> type*" << endl;

    double * pd1 = new double(12.3456);
    double * pd2 = new double(98.7654);

    cout << "  pd1 = " <<  pd1 << endl;
    cout << " *pd1 = " << *pd1 << endl;
    cout << " &pd1 = " << &pd1 << endl;
    cout << "  pd2 = " <<  pd2 << endl;
    cout << " *pd2 = " << *pd2 << endl;
    cout << " &pd2 = " << &pd2 << endl;

    cout << "--->> cast --> c-style cast" << endl;

    pd1 = (double*)(p1);
    pd2 = (double*)(p2);

    cout << "  pd1 = " <<  pd1 << endl;
    cout << " *pd1 = " << *pd1 << endl;
    cout << " &pd1 = " << &pd1 << endl;
    cout << "  pd2 = " <<  pd2 << endl;
    cout << " *pd2 = " << *pd2 << endl;
    cout << " &pd2 = " << &pd2 << endl;

    cout << "--->> cast --> static_cast" << endl;

    pd1 = static_cast<double*>(p1);
    pd2 = static_cast<double*>(p2);

    cout << "  pd1 = " <<  pd1 << endl;
    cout << " *pd1 = " << *pd1 << endl;
    cout << " &pd1 = " << &pd1 << endl;
    cout << "  pd2 = " <<  pd2 << endl;
    cout << " *pd2 = " << *pd2 << endl;
    cout << " &pd2 = " << &pd2 << endl;

    cout << "--->> cast --> dynamic_cast --> can not be done" << endl;

    //pd1 = dynamic_cast<double*>(p1); // ERROR: target is not pointer or reference to a class
    //pd2 = dynamic_cast<double*>(p2); // ERROR: target is not pointer or reference to a class

    cout << "--->> cast --> reinterpret_cast" << endl;

    pd1 = reinterpret_cast<double*>(p1);
    pd2 = reinterpret_cast<double*>(p2);

    cout << "  pd1 = " <<  pd1 << endl;
    cout << " *pd1 = " << *pd1 << endl;
    cout << " &pd1 = " << &pd1 << endl;
    cout << "  pd2 = " <<  pd2 << endl;
    cout << " *pd2 = " << *pd2 << endl;
    cout << " &pd2 = " << &pd2 << endl;

    return 0;
}

// END
