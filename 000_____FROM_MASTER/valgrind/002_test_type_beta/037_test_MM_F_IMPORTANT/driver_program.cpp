//================//
// shared pointer //
//================//

#include <iostream>
#include <memory>
#include <cstring>

using std::cin;
using std::cout;
using std::endl;
using std::shared_ptr;

// function

template <typename T>
class arrayDeleter {
public:
    void operator()(const T * p)
    {
        delete [] p;
    }
};

//==================//
// the main program //
//==================//

int main ()
{
    const int SIZE = 1000;
    shared_ptr<char> p1;
    p1.reset(new char[SIZE], arrayDeleter<char>());

    const int IMAX = 10;

    for (int i = 0; i != IMAX; i++) {
        cout << "---------------------------------------------->> " << i << endl;
        p1.reset(new char[SIZE], arrayDeleter<char>());
        for (int j = 0; j != SIZE-1; j++) {
            p1.get()[j] = 'a';
        }
        p1.get()[SIZE-2] = '\0';
        p1.get()[SIZE-1] = '\0';
        cout << strlen(p1.get()) << endl;
        p1.reset();
    }

    p1.reset();
    return 0;
}

// END
