//=========================================//
// 'new' and 'delete' overloaded operators //
//=========================================//

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

// class-specific allocation functions

class X {
public:

    // new
    static void* operator new(size_t sz)
    {
        cout << "custom new for size " << sz << endl;
        return ::operator new(sz);
    }

    // new []
    static void* operator new[](size_t sz)
    {
        cout << "custom new for size " << sz << endl;
        return ::operator new(sz);
    }
};

// the main function

int main()
{
    X* p1 = new X;
    delete p1;
    X* p2 = new X[10];
    delete[] p2;
}

// END
