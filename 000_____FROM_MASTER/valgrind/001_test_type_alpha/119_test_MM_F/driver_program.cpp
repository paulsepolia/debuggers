//=============//
// memory leak //
//=============//

#include <iostream>

// leaking function

void f()
{
    int * x = new int [10];
    double * y = new double [100];
    x[10] = 0; // problem 1: heap overrun
    // problem 2: memory leak, since x not freed
}

// the main function

int main()
{
    f();
    return 0;
}

// END
