
// memset example

#include <cstring>
#include <iostream>

using std::endl;
using std::cout;
using std::cin;

int main ()
{
    char str[] = "almost every programmer should know memset!";

    memset(str,'-',6);

    puts(str);

    return 0;
}

// end
