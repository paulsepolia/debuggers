
//===========//
// EXCELLENT //
// EXCELLENT //
//===========//

//============//
// shared_ptr //
//============//

#include <iostream>
#include <memory>
#include <vector>
#include <cmath>

using std::endl;
using std::cout;
using std::cin;
using std::shared_ptr;
using std::pow;

// the main function

int main()
{
    const int TRIALS = 5;
    const int DIM = 10000000;

    for (int ik = 0; ik != TRIALS; ik++) {
        cout << "-------------------------------------------->> " << ik << endl;

        shared_ptr<shared_ptr<double>> da;

        da.reset(new shared_ptr<double>[DIM], [](shared_ptr<double> * p) {
            delete [] p;
        } );

        for (int i = 0; i != DIM; i++) {
            da.get()[i].reset(new double (i+12.34), [](double * p) {
                delete [] p;
            } );
        }

        cout << *(da.get()[0]) << endl;
        cout << *(da.get()[1]) << endl;
        cout << *(da.get()[2]) << endl;

        for (int i = 0; i != DIM; i++) {
            da.get()[i].reset(new double (i+22.34), [](double * p) {
                delete [] p;
            } );
        }

        cout << *(da.get()[0]) << endl;
        cout << *(da.get()[1]) << endl;
        cout << *(da.get()[2]) << endl;
    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

// end
