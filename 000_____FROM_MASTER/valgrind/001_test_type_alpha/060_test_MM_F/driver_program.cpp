
#include <iostream>
#include <vector>
#include <memory>
#include <cmath>

using std::endl;
using std::cout;
using std::cin;
using std::vector;
using std::make_shared;
using std::shared_ptr;
using std::pow;

// the main function

int main()
{
    const unsigned int DIM_A = static_cast<unsigned int>(pow(10.0, 8.0));
    const unsigned int DIM_B = 1000;

    for (unsigned int ik = 0; ik != DIM_B; ik++) {

        cout << "------------------------------------------>> " << ik << endl;

        shared_ptr<vector<double>> spv = make_shared<vector<double>>();

        for (unsigned int i = 0; i != DIM_A; i++) {
            spv->push_back(i);
        }
    }

    return 0;
}

// end
