//=============//
// memory leak //
//=============//

#include <iostream>

using std::cout;
using std::endl;

// base class ML1

class ML1 {
public:

    // constructor

    ML1() : x1(new int)
    {
        x2 = new int;
    }

    // destructor

    virtual ~ML1()
    {

        if (x1 != 0) {
            delete x1;
        }

        if (x2 != 0) {
            delete x2;
        }
    }

private:

    int * x1;
    int * x2;
};

// derived class ML2

class ML2 : public ML1 {
public:
    // constructor

    ML2() : x1(new int)
    {
        x2 = new int;
    }

    // destructor

    virtual ~ML2()
    {

        if (x1 != 0) {
            delete x1;
        }

        if (x2 != 0) {
            delete x2;
        }
    }

private:

    int * x1;
    int * x2;
};

// the main function

int main()
{
    ML1 a1;
    ML1 a2;

    ML2 a3;
    ML2 a4;

    return 0;
}

// END
