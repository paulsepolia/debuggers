
#include <iostream>
#include <string>

using std::endl;
using std::cout;
using std::cin;
using std::string;

// class

class A {
public:

    // constructor

    A() : age(-1), name("NO-NAME-YET")
    {
        id = new int;
        *id = -1;
    }

    // copy constructor

    A(const A & other) : age(other.age), name(other.name)
    {
        *id = *(other.id);
    }

    // assignment operator

    A & operator=(const A& other )
    {
        age = other.age;
        name = other.name;
        *id = *(other.id);
        return *this;
    }

    // set age

    void setAge(const int & my_age)
    {
        age = my_age;
    }

    // set name

    void setName(const string & my_name)
    {
        name = my_name;
    }

    // set id

    void setID(const int & my_ID)
    {
        *id = my_ID;
    }

    // get age

    int getAge()
    {
        return this->age;
    }

    // get name

    string getName()
    {
        return this->name;
    }

    // get id

    int getID()
    {
        if (this->id != 0) {
            return *this->id;
        } else {
            return -1;
        }
    }

    // reset person

    void reset_it()
    {
        this->setName("NO-NAME-YET");
        this->setID(-1);
        this->setAge(-1);
    }

    // delete person

    void delete_it()
    {
        this->reset_it();
        delete this->id;
        id = 0;
    }

    // destructor

    virtual ~A()
    {
        if (id != 0) {
            delete id;
        }
    }

private:

    int age;
    string name;
    int * id;
};

// the main function

int main ()
{
    const int DIM = 40;

    // allocate sapce for many persons

    A * person = new A [DIM];

    // set person[0]

    person[0].setName("Pavlos G. Galiatsatos");
    person[0].setAge(42);
    person[0].setID(102);

    // set rest persons

    for (int i = 0; i != DIM; i++) {
        person[i] = person[0];
    }

    // reset person[3]

    person[3].reset_it();

    // output

    cout << "--> person[0].getName() = " << person[0].getName() << endl;
    cout << "--> person[0].getAge()  = " << person[0].getAge() << endl;
    cout << "--> person[0].getID()   = " << person[0].getID() << endl;

    cout << "--> person[1].getName() = " << person[1].getName() << endl;
    cout << "--> person[1].getAge()  = " << person[1].getAge() << endl;
    cout << "--> person[1].getID()   = " << person[1].getID() << endl;

    cout << "--> person[2].getName() = " << person[2].getName() << endl;
    cout << "--> person[2].getAge()  = " << person[2].getAge() << endl;
    cout << "--> person[2].getID()   = " << person[2].getID() << endl;

    cout << "--> person[3].getName() = " << person[3].getName() << endl;
    cout << "--> person[3].getAge()  = " << person[3].getAge() << endl;
    cout << "--> person[3].getID()   = " << person[3].getID() << endl;

    // delete a person

    person[2].delete_it();

    cout << "--> person[2].getName() = " << person[2].getName() << endl;
    cout << "--> person[2].getAge()  = " << person[2].getAge() << endl;
    cout << "--> person[2].getID()   = " << person[2].getID() << endl;

    // free up heap

    delete [] person;

    return 0;
}

// end
