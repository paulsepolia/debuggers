
//=============//
// MEMORY LEAK //
//=============//

//=========//
// pointer //
//=========//

#include <iostream>
#include <memory>
#include <vector>
#include <cmath>

using std::cout;
using std::endl;
using std::cin;
using std::unique_ptr;
using std::vector;
using std::pow;
using std::move;

//===================//
// the main function //
//===================//

typedef unsigned long int uli;

int main ()
{
    // local parameters

    const uli DIM = static_cast<uli>(pow(10.0, 4.0));
    const uli TRIALS = static_cast<uli>(pow(10.0, 1.0));

    // local variables

    for(uli ik = 0; ik != TRIALS; ik++) {

        vector<double*> vp1;

        cout << "--------------------------------------------------->> " << ik << endl;

        // build vector of pointers
        // way 1

        for (uli i = 0; i != DIM; i++) {
            vp1.push_back(new double(i)); // MEMORY LEAK
        }

        cout << " *vp1[0] = " << *vp1[0] << endl;
        cout << " *vp1[1] = " << *vp1[1] << endl;
        cout << " *vp1[2] = " << *vp1[2] << endl;
        cout << " *vp1[3] = " << *vp1[3] << endl;

        vp1.clear();
        vp1.shrink_to_fit();

        // build vector of pointers
        // way 2

        for (uli i = 0; i != DIM; i++) {
            vp1.emplace_back(new double(i)); // MEMORY LEAK
        }

        cout << " *vp1[0] = " << *vp1[0] << endl;
        cout << " *vp1[1] = " << *vp1[1] << endl;
        cout << " *vp1[2] = " << *vp1[2] << endl;
        cout << " *vp1[3] = " << *vp1[3] << endl;

        vp1.clear();
        vp1.shrink_to_fit();
    }

    return 0;
}

//=====//
// END //
//=====//
