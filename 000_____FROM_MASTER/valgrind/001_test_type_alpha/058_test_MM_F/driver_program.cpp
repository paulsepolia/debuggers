
#include <iostream>
#include <memory>

using std::endl;
using std::cout;
using std::cin;
using std::weak_ptr;
using std::make_shared;
using std::shared_ptr;

weak_ptr<int> gw;

// function

void f()
{
    if (shared_ptr<int> spt = gw.lock()) { // Has to be copied into a shared_ptr before usage
        cout << *spt << endl;
    } else {
        cout << "gw is expired" << endl;
    }
}

// the main function

int main()
{
    {
        shared_ptr<int> sp = make_shared<int>(42);
        gw = sp;

        cout << " --> 1" << endl;
        f();
        cout << " --> 2" << endl;
    }

    cout << " --> 3" << endl;
    f();
    cout << " --> 4" << endl;

    return 0;
}

// end
