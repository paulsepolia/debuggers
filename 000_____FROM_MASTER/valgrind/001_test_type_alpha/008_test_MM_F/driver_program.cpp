
#include <iostream>
#include <string>

using std::endl;
using std::cout;
using std::cin;
using std::string;

// class A

class A {
public:

    // constructor

    A() : age(-1), name("NO-NAME-YET")
    {
        id = new int;
        *id = -1;
    }

    // copy constructor

    A(const A & other) : age(other.age), name(other.name)
    {
        *id = *(other.id);
    }

    // assignment operator

    A & operator=(const A& other )
    {
        age = other.age;
        name = other.name;
        *id = *(other.id);
        return *this;
    }

    // set age

    virtual void setAge(const int & my_age)
    {
        age = my_age;
    }

    // set name

    virtual void setName(const string & my_name)
    {
        name = my_name;
    }

    // set id

    virtual void setID(const int & my_ID)
    {
        *id = my_ID;
    }

    // get age

    virtual int getAge() const
    {
        return this->age;
    }

    // get name

    virtual string getName() const
    {
        return this->name;
    }

    // get id

    virtual int getID() const
    {
        if (this->id != 0) {
            return *this->id;
        } else {
            return -1;
        }
    }

    // reset person

    virtual void reset_it()
    {
        this->setName("NO-NAME-YET");
        this->setID(-1);
        this->setAge(-1);
    }

    // delete person

    virtual void delete_it()
    {
        this->reset_it();
        delete this->id;
        id = 0;
    }

    // destructor

    virtual ~A()
    {
        if (id != 0) {
            delete id;
        }
    }

private:

    int age;
    string name;
    int * id;
};

// class B

class B : public A {

public:

    B(): A()
    {
        id2 = new int;
        *id2 = -2;
    }

    // assignment operator

    B & operator=(const B& other )
    {
        this->setAge(other.getAge());
        this->setName(other.getName());
        this->setID(other.getID());
        *id2 = *(other.id2);
        return *this;
    }

private:

    int * id2;

public:

    virtual ~B()
    {
        if (id2 != 0) {
            delete id2;
        }
    }
};

// the main function

int main ()
{
    const int DIM = 40;

    // allocate space for many persons

    B * person = new B [DIM];

    // set person[0]

    person[0].setName("Pavlos G. Galiatsatos");
    person[0].setAge(42);
    person[0].setID(102);

    // set rest persons

    for (int i = 0; i != DIM; i++) {
        person[i] = person[0];
    }

    // reset person[3]

    person[3].reset_it();

    // output

    cout << "--> person[0].getName() = " << person[0].getName() << endl;
    cout << "--> person[0].getAge()  = " << person[0].getAge() << endl;
    cout << "--> person[0].getID()   = " << person[0].getID() << endl;

    cout << "--> person[1].getName() = " << person[1].getName() << endl;
    cout << "--> person[1].getAge()  = " << person[1].getAge() << endl;
    cout << "--> person[1].getID()   = " << person[1].getID() << endl;

    cout << "--> person[2].getName() = " << person[2].getName() << endl;
    cout << "--> person[2].getAge()  = " << person[2].getAge() << endl;
    cout << "--> person[2].getID()   = " << person[2].getID() << endl;

    cout << "--> person[3].getName() = " << person[3].getName() << endl;
    cout << "--> person[3].getAge()  = " << person[3].getAge() << endl;
    cout << "--> person[3].getID()   = " << person[3].getID() << endl;

    // delete a person

    person[2].delete_it();

    cout << "--> person[2].getName() = " << person[2].getName() << endl;
    cout << "--> person[2].getAge()  = " << person[2].getAge() << endl;
    cout << "--> person[2].getID()   = " << person[2].getID() << endl;

    // free up heap

    delete [] person;

    return 0;
}

// end
