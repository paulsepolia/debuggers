
#include <iostream>
#include <cstring>
#include <ctime>

using std::endl;
using std::cout;
using std::cin;

// the main function

int main ()
{
    const int DIM = 100000000;
    const int COEF = 1;
    const int TRIALS_A = 100;
    const int TRIALS_B = 100;

    int * pA = new int [DIM];
    int * pB = new int [DIM];

    for (int i = 0; i != DIM; i++) {
        pA[i] = i;
    }

    cout << " --> pA[0] = " << pA[0] << endl;
    cout << " --> pA[1] = " << pA[1] << endl;
    cout << " --> pA[2] = " << pA[2] << endl;

    cout << " --> memmove" << endl;

    clock_t t1;
    clock_t t2;

    t1 = clock();

    for (int i = 0; i != TRIALS_A; i++) {
        memmove(pB, pA, COEF*DIM*sizeof(int));
    }

    t2 = clock();

    cout << " --> pB[0] = " << pB[0] << endl;
    cout << " --> pB[1] = " << pB[1] << endl;
    cout << " --> pB[2] = " << pB[2] << endl;

    cout << " --> time used for memmove = "
         << static_cast<double>((t2-t1))/CLOCKS_PER_SEC << endl;

    int isE;

    cout << " --> memcmp" << endl;

    t1 = clock();

    for (int i = 0; i != TRIALS_B; i++) {
        isE = memcmp(pB, pA, DIM*sizeof(int));
    }

    t2 = clock();

    cout << " --> time used for memcmp = "
         << static_cast<double>((t2-t1))/CLOCKS_PER_SEC << endl;

    cout << " --> isE = " << isE << endl;

    delete [] pA;
    delete [] pB;

    return 0;
}

// end
