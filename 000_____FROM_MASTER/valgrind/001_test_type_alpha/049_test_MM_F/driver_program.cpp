
#include <memory>
#include <iostream>
#include <cmath>
#include <vector>

using std::endl;
using std::cout;
using std::cin;
using std::shared_ptr;
using std::make_shared;
using std::pow;
using std::vector;

#define OUT
#undef OUT

// the main function

int main()
{
    const unsigned int TRIALS = 100;

    for (unsigned int iK = 0; iK != TRIALS; iK++) {
        const unsigned int DIM = static_cast<unsigned int>(pow(10.0, 3.0));

#ifdef OUT
        cout << " --> step --> 1 --> declare vector of shared pointers" << endl;
#endif
        vector<shared_ptr<double>> vsp;

#ifdef OUT
        cout << " --> step 2 --> make a shared pointer many times" << endl;
#endif
        for (unsigned int i = 0; i != DIM; i++) {
            shared_ptr<double> sp;
            sp = make_shared<double>(i);
            vsp.push_back(sp);
        }
#ifdef OUT
        cout << " --> step 3 --> some output" << endl;

        cout << " -->  vsp[0] = " <<  vsp[0] << endl;
        cout << " -->  vsp[1] = " <<  vsp[1] << endl;
        cout << " -->  vsp[2] = " <<  vsp[2] << endl;
        cout << " --> *vsp[0] = " << *vsp[0] << endl;
        cout << " --> *vsp[1] = " << *vsp[1] << endl;
        cout << " --> *vsp[2] = " << *vsp[2] << endl;
#endif

#ifdef OUT
        cout << " --> step 4 --> clear the vector" << endl;
#endif
        vsp.clear();
    }

    return 0;
}

// end
