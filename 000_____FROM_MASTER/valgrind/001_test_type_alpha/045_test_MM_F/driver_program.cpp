
#include <memory>
#include <iostream>

using std::endl;
using std::cout;
using std::cin;
using std::make_shared;
using std::shared_ptr;

// struct Foo

struct Foo {
    Foo(int n = -1) noexcept :
        bar(n)
    {
        cout << "Foo: constructor, bar = " << bar << endl;
    }
    ~Foo()
    {
        cout << "Foo: destructor, bar = " << bar << endl;
    }
    int getBar() const noexcept
    {
        return bar;
    }
private:
    int bar;
};

// the main function

int main()
{
    cout << " --> step 1 --> make shared pointer" << endl;

    shared_ptr<Foo> sptr = make_shared<Foo>(3);

    cout << " --> step 2 --> output" << endl;

    cout << "The first Foo's bar is " << sptr->getBar() << endl;

    cout << " --> step 3 --> output" << endl;

    // reset the shared_ptr, hand it a fresh instance of Foo
    // (the old instance will be destroyed after this call)

    cout << " --> step 4 --> reset the shared_pointer" << endl;

    sptr.reset(new Foo);

    cout << " --> step 5 --> after the reset" << endl;

    cout << "The second Foo's bar is " << sptr->getBar() << endl;

    cout << " --> step 6 --> output the resetted pointer" << endl;

    return 0;
}

// end
