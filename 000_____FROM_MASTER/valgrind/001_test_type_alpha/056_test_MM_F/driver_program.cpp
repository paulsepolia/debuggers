
//=============//
// MEMORY LEAK //
//=============//

#include <memory>
#include <iostream>

using std::endl;
using std::cout;
using std::cin;
using std::unique_ptr;

// the main function

int main()
{
    unique_ptr<int> auto_pointer (new int);
    int * manual_pointer;

    *auto_pointer = 11;

    manual_pointer = auto_pointer.release();
    // auto_pointer is now empty

    std::cout << " --> manual_pointer points to " << *manual_pointer << endl;

    delete manual_pointer;

    manual_pointer = 0;

    return 0;
}

// end
