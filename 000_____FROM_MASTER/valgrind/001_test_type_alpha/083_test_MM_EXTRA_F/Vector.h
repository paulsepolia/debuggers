
#ifndef VECTOR_H
#define VECTOR_H

//==================//
// class: Vector<T> //
//==================//

template<class T>
class Vector {
public:

    // default constructor

    Vector() : m_dim(0), pt(0) {}

    // constructor

    explicit Vector(const unsigned long int & dim) : m_dim(dim)
    {
        this->allocate(m_dim);
    }

    // copy constructor

    Vector(const Vector & other) : m_dim(other.m_dim)
    {

        this->allocate(m_dim);

        for (unsigned int i = 0; i != m_dim; i++) {
            this->pt[i] = other.pt[i];
        }
    }

    // allocate

    void allocate(const unsigned long int & DIM)
    {
        this->m_dim = DIM;
        this->pt = new T [DIM];
    }

    // deallocate

    void deallocate()
    {
        if (this->pt != 0) {
            delete [] this->pt;
            this->pt = 0;
        }
    }

    // set element

    void set_element(const unsigned long int & index, const T & elem)
    {
        this->pt[index] = elem;
    }

    // get element

    T get_element(const unsigned long int & index)
    {
        T elem = this->pt[index];
        return elem;
    }

    // assignment operator

    const Vector & operator=(const Vector & other)
    {
        for (unsigned long int i = 0; i != other.m_dim; i++) {
            T elem = other.pt[i];
            this->pt[i] = elem;
        }

        return *this;
    }

    // assignment operator

    const Vector & operator=(const T & elem)
    {
        for (unsigned long int i = 0; i != this->m_dim; i++) {
            this->pt[i] = elem;
        }

        return *this;
    }

    // + operator --> version --> 1

    const Vector operator+(const Vector & other)
    {
        Vector tmp(other.m_dim);

        for (unsigned long int i = 0; i != other.m_dim; i++) {
            T elem1 = other.pt[i];
            T elem2 = this->pt[i];
            T elem_tmp = elem2 + elem1;
            tmp.pt[i] = elem_tmp;
        }

        return tmp;
    }

    // + operator --> version --> 2

    const Vector operator+(const T & elem)
    {
        Vector tmp(this->m_dim);

        for (unsigned long int i = 0; i != this->m_dim; i++) {
            T elem1 = this->pt[i];
            T elem_tmp = elem1 + elem;
            tmp.pt[i] = elem_tmp;
        }

        return tmp;
    }

    // - operator --> version --> 1

    const Vector operator-(const Vector & other)
    {
        Vector tmp(other.m_dim);

        for (unsigned long int i = 0; i != other.m_dim; i++) {
            T elem1 = other.pt[i];
            T elem2 = this->pt[i];
            T elem_tmp = elem2 - elem1;
            tmp.pt[i] = elem_tmp;
        }

        return tmp;
    }

    // - operator --> version --> 2

    const Vector operator-(const T & elem)
    {
        Vector tmp(this->m_dim);

        for (unsigned long int i = 0; i != this->m_dim; i++) {
            T elem1 = this->pt[i];
            T elem_tmp = elem1 - elem;
            tmp.pt[i] = elem_tmp;
        }

        return tmp;
    }

    // * operator --> version --> 1

    const Vector operator*(const Vector & other)
    {
        Vector tmp(other.m_dim);

        for (unsigned long int i = 0; i != other.m_dim; i++) {
            T elem1 = other.pt[i];
            T elem2 = this->pt[i];
            T elem_tmp = elem2 * elem1;
            tmp.pt[i] = elem_tmp;
        }

        return tmp;
    }

    // * operator --> version --> 2

    const Vector operator*(const T & elem)
    {
        Vector tmp(this->m_dim);

        for (unsigned long int i = 0; i != this->m_dim; i++) {
            T elem1 = this->pt[i];
            T elem_tmp = elem1 * elem;
            tmp.pt[i] = elem_tmp;
        }

        return tmp;
    }

    // / operator --> version --> 1

    const Vector operator/(const Vector & other)
    {
        Vector tmp(other.m_dim);

        for (unsigned long int i = 0; i != other.m_dim; i++) {
            T elem1 = other.pt[i];
            T elem2 = this->pt[i];
            T elem_tmp = elem2 / elem1;
            tmp.pt[i] = elem_tmp;
        }

        return tmp;
    }

    // / operator --> version --> 2

    const Vector operator/(const T & elem)
    {
        Vector tmp(this->m_dim);

        for (unsigned long int i = 0; i != this->m_dim; i++) {
            T elem1 = this->pt[i];
            T elem_tmp = elem1 / elem;
            tmp.pt[i] = elem_tmp;
        }

        return tmp;
    }

    // ++ operator --> version --> 1

    const Vector operator++()
    {
        for (unsigned long int i = 0; i != this->m_dim; i++) {
            T elem = this->pt[i];
            T elem_tmp = elem + static_cast<T>(1.0);
            this->pt[i] = elem_tmp;
        }

        return *this;
    }

    // ++ operator --> version --> 2

    const Vector operator++(int)
    {
        ++(*this);
        return *this;
    }

    // -- operator --> version --> 1

    const Vector operator--()
    {
        for (unsigned long int i = 0; i != this->m_dim; i++) {
            T elem = this->pt[i];
            T elem_tmp = elem - static_cast<T>(1.0);
            this->pt[i] = elem_tmp;
        }

        return *this;
    }

    // -- operator --> version --> 2

    const Vector operator--(int)
    {
        --(*this);
        return *this;
    }

    // += operator --> version --> 1

    const Vector operator+=(const Vector & other)
    {
        *this = *this + other;
        return *this;
    }

    // += operator --> version --> 2

    const Vector operator+=(const T & elem)
    {
        *this = *this + elem;
        return *this;
    }

    // -= operator --> version --> 1

    const Vector operator-=(const Vector & other)
    {
        *this = *this - other;
        return *this;
    }

    // -= operator --> version --> 2

    const Vector operator-=(const T & elem)
    {
        *this = *this - elem;
        return *this;
    }

    // *= operator --> version --> 1

    const Vector operator*=(const Vector & other)
    {
        *this = *this * other;
        return *this;
    }

    // *= operator --> version --> 2

    const Vector operator*=(const T & elem)
    {
        *this = *this * elem;
        return *this;
    }

    // /= operator --> version --> 1

    const Vector operator/=(const Vector & other)
    {
        *this = *this / other;
        return *this;
    }

    // /= operator --> version --> 2

    const Vector operator/=(const T & elem)
    {
        *this = *this / elem;
        return *this;
    }

    // destructor

    virtual ~Vector()
    {
        if (pt != 0) {
            delete [] pt;
            pt = 0;
        }
    }

private:

    unsigned long int m_dim;
    T * pt;
};

//=====//
// end //
//=====//

#endif
