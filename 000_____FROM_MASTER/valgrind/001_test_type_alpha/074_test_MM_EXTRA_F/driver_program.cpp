
//================//
// NO MEMORY LEAK //
//================//

#include <iostream>
#include <memory>
#include <cmath>

using std::endl;
using std::cout;
using std::cin;
using std::unique_ptr;
using std::shared_ptr;
using std::make_shared;
using std::pow;

// class A

template<class T>
class A {
public:

    // default constructor

    A() : sp(make_shared<T>(T(0.0))), up(new T(0.0)), p(new T(0.0)) {}

    // copy constructor

    A(const A & other) : sp(make_shared<T>(*other.sp)), up(new T(*other.up)), p(new T(*other.p)) {}

    // assignment operator

    A & operator=(const A & other)
    {
        sp = make_shared<T>(*other.sp);
        ssp = make_shared<shared_ptr<T>>(*other.ssp);
        up.reset(new T(*other.up));
        *p = *other.p;

        return *this;
    }

    // + operator

    A operator+(const A & other)
    {
        A tmp;

        *tmp.sp = other.get_shared() + this->get_shared();
        *tmp.p = other.get_pointer() + this->get_pointer();
        tmp.up.reset(new T(other.get_unique() + this->get_unique()));

        return tmp;
    }

    // - operator

    A operator-(const A & other)
    {
        A tmp;

        *tmp.sp = other.get_shared() - this->get_shared();
        *tmp.p = other.get_pointer() - this->get_pointer();
        tmp.up.reset(new T(other.get_unique() - this->get_unique()));

        return tmp;
    }

    // * operator

    A operator*(const A & other)
    {
        A tmp;

        *tmp.sp = other.get_shared() * this->get_shared();
        *tmp.p = other.get_pointer() * this->get_pointer();
        tmp.up.reset(new T(other.get_unique() * this->get_unique()));

        return tmp;
    }

    // / operator

    A operator/(const A & other)
    {
        A tmp;

        *tmp.sp = other.get_shared() / this->get_shared();
        *tmp.p = other.get_pointer() / this->get_pointer();
        tmp.up.reset(new T(other.get_unique() / this->get_unique()));

        return tmp;
    }

    // set shared pointer

    void set_shared(const T & val)
    {
        sp = make_shared<T>(val);
    }

    // get shared pointer

    T get_shared() const
    {
        return *this->sp;
    }

    // set pointer

    void set_pointer(const T & val)
    {
        *p = val;
    }

    // get pointer

    T get_pointer() const
    {
        return *this->p;
    }

    // set unique pointer

    void set_unique(const T & val)
    {
        up.reset(new T(val));
    }

    // get unique pointer

    T get_unique() const
    {
        return *up.get();
    }

    // delete pointer

    void delete_pointer()
    {
        if (this->p!=0) {
            delete p;
            p = 0;
        }
    }

    // vector allocate

    void vector_allocate(const unsigned long int & DIM)
    {
        if (this->p != 0) {
            delete [] this->p;
            this->p = new T [DIM];
        } else if (this->p == 0) {
            this->p = new T [DIM];
        }
    }

    // vector shared allocate

    void vector_shared_allocate(const unsigned long int & DIM)
    {
        this->ssp.reset(new shared_ptr<T> [DIM], [](shared_ptr<T> * p) {
            delete [] p;
        });
    }

    // vector shared set element

    void vector_shared_set_element(const unsigned long int & index, const T & elem)
    {
        this->ssp.get()[index].reset(new T (elem), [](T * p) {
            delete p;
        });
    }

    // vector shared get element

    T vector_shared_get_element(const unsigned long int & index)
    {
        T elem;
        elem = *this->ssp.get()[index];

        return elem;
    }

    // set element

    void set_element(const unsigned long int & index, const T & elem)
    {
        this->p[index] = elem;
    }

    // get element

    T get_element(const unsigned long int & index) const
    {
        return this->p[index];
    }

    // vector deallocate

    void vector_deallocate()
    {
        if (this->p != 0) {
            delete [] p;
            p = 0;
        }
    }

    // destructor

    virtual ~A()
    {
        if (p != 0) {
            delete [] p;
            p = 0;
        }
    }

private:

    shared_ptr<shared_ptr<T>> ssp;
    shared_ptr<T> sp;
    unique_ptr<T> up;
    T * p;

};

// the main function

int main()
{
    A<double> a1;
    A<double> a2;

    const unsigned long int DIM1 = static_cast<unsigned long int>(pow(10.0, 5.0));
    const unsigned long int DIM2 = static_cast<unsigned long int>(pow(10.0, 5.0));

    a1.vector_shared_allocate(DIM2);
    a2.vector_shared_allocate(DIM2);

    a1 = a2;

    const int TRIALS = 10;

    for (int ik = 0; ik != TRIALS; ik++) {

        cout << "------------------------------------------------>> " << ik << endl;

        cout << "--> allocate shared vector" << endl;

        a1.vector_shared_allocate(DIM2);
        for (unsigned long int i = 0; i != DIM2; i++) {
            a1.vector_shared_set_element(i, i+ik);
        }

        double d1 = a1.vector_shared_get_element(0);
        double d2 = a1.vector_shared_get_element(1);
        double d3 = a1.vector_shared_get_element(2);

        cout << "--> a1.vector_shared_get_element(0) = " << d1 << endl;
        cout << "--> a1.vector_shared_get_element(1) = " << d2 << endl;
        cout << "--> a1.vector_shared_get_element(2) = " << d3 << endl;

        cout << "--> allocate vector" << endl;

        a1.vector_allocate(DIM1);

        cout << "--> build vector" << endl;

        for (unsigned long int i = 0; i != DIM1; i++) {
            a1.set_element(i, static_cast<double>(i));
        }

        cout << "--> a1.get_element(ik) = " << a1.get_element(ik) << endl;

        cout << "--> deallocate vector" << endl;

        a1.vector_deallocate();
    }

    return 0;
}

// end
