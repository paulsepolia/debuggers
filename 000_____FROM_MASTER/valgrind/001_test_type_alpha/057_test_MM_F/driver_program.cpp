
//=============//
// MEMORY LEAK //
//=============//

#include <memory>
#include <iostream>

using std::endl;
using std::cout;
using std::cin;

// the main function

int main()
{
    int * p1;
    int * p2;

    p1 = new int;

    *p1 = 11;

    p2 = p1;

    cout << " -->  p1 = " <<  p1 << endl;
    cout << " --> *p1 = " << *p1 << endl;
    cout << " -->  p2 = " <<  p2 << endl;
    cout << " --> *p2 = " << *p2 << endl;

    delete p2;

    p2 = 0;
    p1 = 0;

    return 0;
}

// end
