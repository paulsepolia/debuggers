//========================================//
// unique_ptr::get vs unique_ptr::release //
//========================================//

#include <iostream>
#include <memory>

using std::endl;
using std::cout;
using std::cin;
using std::unique_ptr;
using std::move;

// the main function

int main ()
{
    // foo   bar    p
    // ---   ---   ---

    unique_ptr<int> foo;                // null
    unique_ptr<int> bar;                // null  null
    int* p = nullptr;                   // null  null  null

    foo = unique_ptr<int>(new int(10)); // (10)  null  null

    cout << " 1 --> *foo = " << *foo << endl;

    bar = move(foo);                    // null  (10)  null

    //cout << " 2 --> *foo = " << *foo << endl; // SEG-FAULT
    cout << " 3 --> *bar = " << *bar << endl;

    p = bar.get();                      // null  (10)  (10)

    cout << " 4 --> *p = " << *p << endl;

    *p = 20;                            // null  (20)  (20)

    cout << " 5 --> *p = " << *p << endl;

    p = nullptr;                        // null  (20)  null

    foo = unique_ptr<int>(new int(30)); // (30)  (20)  null

    cout << " 6 --> *foo = " << *foo << endl;
    cout << " 7 --> *bar = " << *bar << endl;

    p = foo.release();                  // null  (20)  (30)

    cout << " 8 --> *p = " << *p << endl;

    *p = 40;                            // null  (20)  (40)

    cout << " foo: ";

    if (foo) {
        cout << *foo << endl;
    } else {
        cout << "(null)" << endl;
    }

    cout << "bar: ";

    if (bar) {
        cout << *bar << endl;
    } else {
        cout << "(null)" << endl;
    }

    cout << "p: ";
    if (p) {
        cout << *p << endl;
    } else {
        cout << "(null)" << endl;
    }
    cout << endl;

    delete p;   // the program is now responsible of deleting the object pointed to by p
    // bar deletes its managed object automatically

    return 0;
}

// end
