
#include <iostream>
#include <string>

using std::endl;
using std::cout;
using std::cin;
using std::string;

// function A

int funA(int * pa)
{
    pa = new int;
    *pa = 10;

    return *pa;
}

// the main function

int main ()
{
    int * pA;
    pA = 0;
    int x ;

    x = funA(pA);
    pA = &x;

    cout << " -->  pA = " << pA << endl;
    cout << " --> *pA = " << *pA << endl;

//     delete pA; // ERROR
// MEMORY LEAK

    return 0;
}

// end
