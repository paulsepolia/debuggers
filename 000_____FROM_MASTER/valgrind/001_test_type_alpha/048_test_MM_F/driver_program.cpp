
#include <memory>
#include <iostream>
#include <cmath>

using std::endl;
using std::cout;
using std::cin;
using std::shared_ptr;
using std::make_shared;
using std::pow;

// the main function

int main()
{
    const unsigned int DIM = static_cast<unsigned int>(pow(10.0, 6.0));

    cout << " --> step 1 --> declare shared pointer" << endl;

    shared_ptr<int> sp;

    cout << " --> step 2 --> make a shared pointer many times" << endl;

    for (unsigned int i = 0; i != DIM; i++) {
        sp = make_shared<int>(10);
    }

    cout << " --> step 3 --> output" << endl;

    cout << " --> *sp = " << *sp << endl;

    return 0;
}

// end
