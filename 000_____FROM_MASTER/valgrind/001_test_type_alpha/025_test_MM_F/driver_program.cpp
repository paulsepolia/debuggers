
#include <iostream>

using std::endl;
using std::cout;
using std::cin;

// the function

const int DIM = 1000;

int * funA()
{
    return (new int [DIM]);
}

// the main function

int main()
{
    int * p;

    p = funA();

    for (int i = 0; i != DIM; i++) {
        p[i] = i;
    }

    cout << " p[0] = " << p[0] << endl;
    cout << " p[1] = " << p[1] << endl;
    cout << " p[2] = " << p[2] << endl;

    delete [] p;

    return 0;
}

// end
