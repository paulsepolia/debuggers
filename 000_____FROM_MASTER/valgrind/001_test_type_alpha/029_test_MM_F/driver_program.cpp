//================================//
// shared_ptr constructor example //
//================================//

#include <iostream>
#include <memory>
#include <vector>

using std::endl;
using std::cout;
using std::cin;
using std::shared_ptr;
using std::vector;

// the main function

int main ()
{
    shared_ptr<int> p3 (new int);

    cout << "use_count: " << endl;

    cout << "p3: " << p3.use_count() << endl;

    {
        // vector

        vector<shared_ptr<int>> vP1;

        const int DIM = 100000000;

        for (int i = 0; i != DIM; i++) {
            vP1.push_back(p3);
        }

    }

    // sentineling

    int sentinel;
    cout << "Enter an integer to exit: ";
    cin >> sentinel;

    return 0;
}

// end
