
//=============//
// MEMORY LEAK //
//=============//

#include <memory>
#include <iostream>
#include <cmath>
#include <vector>

using std::endl;
using std::cout;
using std::cin;
using std::pow;
using std::vector;

#define OUT
#undef OUT

// the main function

int main()
{
    const unsigned int TRIALS = 100;

    for (unsigned int iK = 0; iK != TRIALS; iK++) {
        const unsigned int DIM = static_cast<unsigned int>(pow(10.0, 3.0));

#ifdef OUT
        cout << " --> step --> 1 --> declare vector of double pointers" << endl;
#endif
        vector<double*> * vp = new vector<double*>;

#ifdef OUT
        cout << " --> step 2 --> build the vector" << endl;
#endif
        for (unsigned int i = 0; i != DIM; i++) {
            double * sp;
            sp = new double (i);
            vp->push_back(sp);
        }
#ifdef OUT
        cout << " --> step 3 --> some output" << endl;

        cout << " -->  (*vp)[0] = " <<  (*vp)[0] << endl;
        cout << " -->  (*vp)[1] = " <<  (*vp)[1] << endl;
        cout << " -->  (*vp)[2] = " <<  (*vp)[2] << endl;
#endif

#ifdef OUT
        cout << " --> step 4 --> delete the pointers" << endl
#endif
        for (unsigned int i = 0; i != DIM; i++) {
            delete (*vp)[i];
            (*vp)[i] = 0;
        }

#ifdef OUT
        cout << " --> step 5 --> clear the vector" << endl;
#endif
        (*vp).clear();

        delete vp;
    }

    return 0;
}

// end
