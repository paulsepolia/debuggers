
//============//
// unique_ptr //
//============//

#include <iostream>
#include <memory>
#include <ostream>
#include <vector>

using std::cout;
using std::cin;
using std::endl;
using std::unique_ptr;
using std::make_unique; // works only for g++-4.9.2, g++-5.2.0
using std::ostream;
using std::vector;

//==========//
// a struct //
//==========//

struct Vec3 {
    int x, y, z;

    // default constructor

    Vec3() : x(0), y(0), z(0) { }

    // another constructor

    Vec3(int x, int y, int z) :x(x), y(y), z(z) { }

    // operator <<(ostream& os, Vec3 &)

    friend ostream& operator<<(ostream& os, Vec3& v)
    {
        return os << '{' << "x:" << v.x << " y:" << v.y << " z:" << v.z  << '}';
    }

    // operator =(int &)

    const Vec3 & operator=(const int & x_loc)
    {
        this->x = x_loc;
        this->y = x_loc;
        this->z = x_loc;

        return *this;
    }

    // operator =(const vector &)

    const Vec3 & operator=(const vector<double> & v_loc)
    {
        this->x = v_loc[0];
        this->y = v_loc[1];
        this->z = v_loc[2];

        return *this;
    }
};

//===================//
// the main function //
//===================//

int main()
{
    // Use the default constructor

    unique_ptr<Vec3> v1 = make_unique<Vec3>();

    // Use the constructor that matches these arguments

    unique_ptr<Vec3> v2 = make_unique<Vec3>(0, 1, 2);

    // Create a unique_ptr to an array of 5 elements

    unique_ptr<Vec3[]> v3 = make_unique<Vec3[]>(5);

    cout << "make_unique<Vec3>():      " << *v1 << endl
         << "make_unique<Vec3>(0,1,2): " << *v2 << endl
         << "make_unique<Vec3[]>(5):   " << endl;

    for (int i = 0; i < 5; i++) {
        cout << "     " << v3[i] << endl;
    }

    // assign new values

    cout << " --> assign new values" << endl;

    v3[0] = 10;
    v3[1] = 11;
    v3[2] = 12;
    v3[3] = 13;
    v3[4] = 14;

    for (int i = 0; i < 5; i++) {
        cout << "     " << v3[i] << endl;
    }

    // and new values

    vector<double> vv1( {-1,-2,-3});
    vector<double> vv2( {-11,-22,-33});
    vector<double> vv3( {-22,-33,-44});
    vector<double> vv4( {-55,-66,-77});
    vector<double> vv5( {-88,-99,-100});

    v3[0] = vv1;
    v3[1] = vv2;
    v3[2] = vv3;
    v3[3] = vv4;
    v3[4] = vv5;

    for (int i = 0; i < 5; i++) {
        cout << "     " << v3[i] << endl;
    }

    return 0;
}

//=====//
// END //
//=====//
