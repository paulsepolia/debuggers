
//===========================//
// unique_ptr::reset example //
//===========================//

#include <iostream>
#include <memory>

using std::cout;
using std::endl;
using std::cin;
using std::unique_ptr;
using std::addressof;

// deleter

template<class T>
struct D {
    void operator() (T * p)
    {
        cout << "NOW DELETE" << endl;
        delete [] p;
    }
};

//===================//
// the main function //
//===================//

int main ()
{
    const int DIM = 500000000;
    const int TRIALS = 10;

    for (int ik = 0; ik != TRIALS; ik++) {
        cout << "-------------------------------------------------->> " << ik << endl;

        unique_ptr<int[]> upA(new int [DIM]);

        cout << "-------------------------->> A " << endl;

        for (int i = 0; i != DIM; i++) {
            upA[i] = i;
        }

        cout << "--> enter an integer to reset:";
        int sentinel;
        cin >> sentinel;

        upA.reset(nullptr);

        cout << "--> enter an integer to continue:";
        cin >> sentinel;

        cout << "-------------------------->> B " << endl;

        unique_ptr<int[], D<int>> upB(new int [DIM], D<int>());

        for (int i = 0; i != DIM; i++) {
            upB[i] = i+10;
        }

        cout << "--> enter an integer to reset:";
        cin >> sentinel;

        upB.reset(nullptr); // using the D hand-made deleter

        cout << "--> enter an integer to continue:";
        cin >> sentinel;
    }

    return 0;
}

//=====//
// END //
//=====//
