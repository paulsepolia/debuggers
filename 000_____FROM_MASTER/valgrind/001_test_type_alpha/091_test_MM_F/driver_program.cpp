
//=========================================//
// Unique pointer with non-default deleter //
// and with default deleter                //
//=========================================//

//====================//
// SILLY MEMORY LEAKS //
//====================//

#include <iostream>
#include <memory>

using std::endl;
using std::cout;
using std::cin;
using std::unique_ptr;

// struct Foo

struct Foo {

    // constructor

    Foo()
    {
        cout << "Foo..." << endl;
        int * p = new int;
    }

    // destructor

    virtual ~Foo()
    {
        cout << "~Foo..." << endl;
        int * p = new int;
    }
};

// struct D

struct D {
    void operator() (Foo * p)
    {
        cout << "Calling delete for Foo object..." << endl;
        delete p;
    }
};

// the main function

int main()
{
    const int I_MAX = 10;

    for (int i = 0; i != I_MAX; i++) {
        cout << "------------------------------------------------>> " << i << endl;

        cout << " --> Creating new Foo --> 1" << endl;
        unique_ptr<Foo, D> up1(new Foo(), D());  // up owns the Foo pointer (deleter D)

        cout << " --> Creating new Foo --> 2" << endl;
        unique_ptr<Foo> up2(new Foo());  // up owns the Foo pointer (default deleter)

        cout << " --> Replace owned Foo with a new Foo --> 1" << endl;
        up1.reset(new Foo());  // calls deleter for the old one

        cout << " --> Replace owned Foo with a new Foo --> 2" << endl;
        up2.reset(new Foo());  // calls deleter for the old one

        cout << " --> Release and delete the owned Foo --> 1" << endl;
        up1.reset(nullptr);

        cout << " --> Release and delete the owned Foo --> 2" << endl;
        up2.reset(nullptr);
    }

    return 0;
}

// END
