
//=============//
// MEMORY LEAK //
//=============//

#include <memory>
#include <iostream>
#include <cmath>
#include <vector>

using std::endl;
using std::cout;
using std::cin;
using std::pow;
using std::vector;
using std::unique_ptr;

#define OUT
//#undef OUT

// the main function

int main()
{
    const unsigned int TRIALS = 2;

    for (unsigned int iK = 0; iK != TRIALS; iK++) {
        const unsigned int DIM = 1*static_cast<unsigned int>(pow(10.0, 7.0));

#ifdef OUT
        cout << " --> step --> 1 --> declare vector of double pointers" << endl;
#endif
        vector<unique_ptr<double>> * vsp = new vector<unique_ptr<double>>;

#ifdef OUT
        cout << " --> step --> 2 --> build the vector" << endl;
#endif
        for (unsigned int i = 0; i != DIM; i++) {
            vsp->push_back(unique_ptr<double>(new double(i)));
        }
#ifdef OUT
        cout << " --> step --> 3 --> some output" << endl;

        cout << " -->  *(*vsp)[0] = " <<  *(*vsp)[0] << endl;
        cout << " -->  *(*vsp)[1] = " <<  *(*vsp)[1] << endl;
        cout << " -->  *(*vsp)[2] = " <<  *(*vsp)[2] << endl;
        cout << " -->  *(*vsp)[3] = " <<  *(*vsp)[3] << endl;
        cout << " -->  *(*vsp)[4] = " <<  *(*vsp)[4] << endl;
        cout << " -->  *(*vsp)[5] = " <<  *(*vsp)[5] << endl;

#endif

#ifdef OUT
        cout << " --> step --> 4 --> no need to delete the shared pointers" << endl;
#endif

#ifdef OUT
        cout << " --> step --> 5 --> clear the vector" << endl;
#endif
        (*vsp).clear();

        delete vsp;
    }

    return 0;
}

// end
