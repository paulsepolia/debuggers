
#include <iostream>
#include <string>

using std::endl;
using std::cout;
using std::cin;
using std::string;

// function B

int * funB(int * pa)
{
    const int DIM = 100;
    pa = new int [DIM];

    for (int i = 0; i != DIM; i++) {
        pa[i] = i;
    }

    return pa;
}

// the main function

int main ()
{
    const int DIM = 10000000;

    for (int i = 0; i != DIM; i++) {

        int * pA;
        pA = 0;

        pA = funB(pA);

//          cout << " -->  pA = " << pA << endl;

//          cout << " --> *pA   = " << *pA   << endl;
//          cout << " --> *pA+1 = " << *pA+1 << endl;
//          cout << " --> *pA+2 = " << *pA+2 << endl;
//          cout << " --> *pA+3 = " << *pA+3 << endl;

        delete [] pA;
    }

    return 0;
}

// end
