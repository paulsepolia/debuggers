
#include <iostream>
#include <cstring>

using std::endl;
using std::cout;
using std::cin;

// --> main function

int main()
{
    char * str1 = new char [40];

    char * str2 = new char [40];

    strcpy(str1, "Memory leak");

    str2 = str1; // Bad! Now the 40 bytes are impossible to free

    delete [] str2; // This deletes the 30 bytes

    delete [] str1; // Possible access violation. What a disaster!

    return 0;
}

// end
