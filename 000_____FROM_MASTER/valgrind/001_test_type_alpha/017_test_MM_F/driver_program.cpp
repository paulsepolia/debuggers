
#include <iostream>
#include <cstring>

using std::endl;
using std::cout;
using std::cin;

// the main function

int main ()
{
    const int DIM = 1000000;
    const int COEF = 1;

    int * pA = new int [DIM];
    int * pB = new int [DIM];

    for (int i = 0; i != DIM; i++) {
        pA[i] = i;
    }

    cout << " --> pA[0] = " << pA[0] << endl;
    cout << " --> pA[1] = " << pA[1] << endl;
    cout << " --> pA[2] = " << pA[2] << endl;

    memmove(pB, pA, COEF*DIM*sizeof(int));

    cout << " --> pB[0] = " << pB[0] << endl;
    cout << " --> pB[1] = " << pB[1] << endl;
    cout << " --> pB[2] = " << pB[2] << endl;

    delete [] pA;
    delete [] pB;

    return 0;
}

// end
