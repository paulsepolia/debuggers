
#include <iostream>
#include <vector>
#include <memory>
#include <cmath>

using std::endl;
using std::cout;
using std::cin;
using std::vector;
using std::unique_ptr;
using std::shared_ptr;
using std::make_shared;
using std::pow;

// class A

template<class T>
class A {
public:

    // default constructor

    A() : sp(make_shared<T>(T (11.1))), up(new T(22.2)), p(new T(33.3))
    {

        cout << " --> default constructor" << endl;

        cout << " --> sp       = " << sp << endl;
        cout << " --> up.get() = " << up.get() << endl;
        cout << " -->  p       = " << p  << endl;

        cout << " --> *sp = " << *sp << endl;
        cout << " --> *up = " << *up << endl;
        cout << " -->  *p = " << *p  << endl;

    }

    // destructor

    virtual ~A()
    {
        cout << " --> destructor" << endl;
        delete p;
    }

private:

    shared_ptr<T> sp;
    unique_ptr<T> up;
    T * p;

};

// the main function

int main()
{
    A<double> a1;
    A<double> a2;
    A<double> a3;

    return 0;
}

// end
