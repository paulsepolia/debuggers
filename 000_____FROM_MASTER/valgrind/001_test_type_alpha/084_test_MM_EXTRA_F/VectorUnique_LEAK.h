
//==================//
// WITH MEMORY LEAK //
//==================//

#ifndef VECTORUNIQUE_H
#define VECTORUNIQUE_H

#include <memory>

using std::unique_ptr;

//========================//
// class: VectorUnique<T> //
//========================//

template<class T>
class VectorUnique {
public:

    // default constructor

    VectorUnique() : m_dim(0), uup(nullptr) {}

    // constructor

    explicit VectorUnique(const unsigned long int & dim) : m_dim(dim)
    {
        this->allocate(m_dim);
    }

    // copy constructor

    VectorUnique(const VectorUnique & other) : m_dim(other.m_dim)
    {
        this->allocate(m_dim);
        for (unsigned long int i = 0; i != other.m_dim; i++) {
            T elem = *other.uup.get()[i];
            this->uup.get()[i].reset(new T (elem));
        }
    }

    // allocate

    void allocate(const unsigned long int & DIM)
    {
        this->m_dim = DIM;
        this->uup.reset(new unique_ptr<T> [DIM]);
    }

    // set element

    void set_element(const unsigned long int & index, const T & elem)
    {
        this->uup.get()[index].reset(new T (elem));
    }

    // get element

    T get_element(const unsigned long int & index)
    {
        T elem = *this->uup.get()[index];
        return elem;
    }

    // assignment operator

    const VectorUnique & operator=(const VectorUnique & other)
    {
        for (unsigned long int i = 0; i != other.m_dim; i++) {
            T elem = *other.uup.get()[i];
            this->uup.get()[i].reset(new T (elem));
        }

        return *this;
    }

    // assignment operator

    const VectorUnique & operator=(const T & elem)
    {
        for (unsigned long int i = 0; i != this->m_dim; i++) {
            this->uup.get()[i].reset(new T (elem));
        }

        return *this;
    }

    // + operator --> version --> 1

    const VectorUnique operator+(const VectorUnique & other)
    {
        VectorUnique tmp(other.m_dim);

        for (unsigned long int i = 0; i != other.m_dim; i++) {
            T elem1 = *other.uup.get()[i];
            T elem2 = *this->uup.get()[i];
            T elem_tmp = elem2 + elem1;
            tmp.uup.get()[i].reset(new T (elem_tmp));
        }

        return tmp;
    }

    // + operator --> version --> 2

    VectorUnique operator+(const T & elem)
    {
        VectorUnique tmp(this->m_dim);

        for (unsigned long int i = 0; i != this->m_dim; i++) {
            T elem1 = *this->uup.get()[i];
            T elem_tmp = elem1 + elem;
            tmp.uup.get()[i].reset(new T (elem_tmp));
        }

        return tmp;
    }

    // - operator --> version --> 1

    const VectorUnique operator-(const VectorUnique & other)
    {
        VectorUnique tmp(other.m_dim);

        for (unsigned long int i = 0; i != other.m_dim; i++) {
            T elem1 = *other.uup.get()[i];
            T elem2 = *this->uup.get()[i];
            T elem_tmp = elem2 - elem1;
            tmp.uup.get()[i].reset(new T (elem_tmp));
        }

        return tmp;
    }

    // - operator --> version --> 2

    const VectorUnique operator-(const T & elem)
    {
        VectorUnique tmp(this->m_dim);

        for (unsigned long int i = 0; i != this->m_dim; i++) {
            T elem1 = *this->uup.get()[i];
            T elem_tmp = elem1 - elem;
            tmp.uup.get()[i].reset(new T (elem_tmp));
        }

        return tmp;
    }

    // * operator --> version --> 1

    const VectorUnique operator*(const VectorUnique & other)
    {
        VectorUnique tmp(other.m_dim);

        for (unsigned long int i = 0; i != other.m_dim; i++) {
            T elem1 = *other.uup.get()[i];
            T elem2 = *this->uup.get()[i];
            T elem_tmp = elem2 * elem1;
            tmp.uup.get()[i].reset(new T (elem_tmp));
        }

        return tmp;
    }

    // * operator --> version --> 2

    const VectorUnique operator*(const T & elem)
    {
        VectorUnique tmp(this->m_dim);

        for (unsigned long int i = 0; i != this->m_dim; i++) {
            T elem1 = *this->uup.get()[i];
            T elem_tmp = elem1 * elem;
            tmp.uup.get()[i].reset(new T (elem_tmp));
        }

        return tmp;
    }

    // / operator --> version --> 1

    const VectorUnique operator/(const VectorUnique & other)
    {
        VectorUnique tmp(other.m_dim);

        for (unsigned long int i = 0; i != other.m_dim; i++) {
            T elem1 = *other.uup.get()[i];
            T elem2 = *this->uup.get()[i];
            T elem_tmp = elem2 / elem1;
            tmp.uup.get()[i].reset(new T (elem_tmp));
        }

        return tmp;
    }

    // / operator --> version --> 2

    const VectorUnique operator/(const T & elem)
    {
        VectorUnique tmp(this->m_dim);

        for (unsigned long int i = 0; i != this->m_dim; i++) {
            T elem1 = *this->uup.get()[i];
            T elem_tmp = elem1 / elem;
            tmp.uup.get()[i].reset(new T (elem_tmp));
        }

        return tmp;
    }

    // ++ operator --> version --> 1

    const VectorUnique operator++()
    {
        for (unsigned long int i = 0; i != this->m_dim; i++) {
            T elem = *this->uup.get()[i];
            T elem_tmp = elem + static_cast<T>(1.0);
            this->uup.get()[i].reset(new T (elem_tmp));
        }

        return *this;
    }

    // ++ operator --> version --> 2

    const VectorUnique operator++(int)
    {
        ++(*this);
        return *this;
    }

    // -- operator --> version --> 1

    const VectorUnique operator--()
    {
        for (unsigned long int i = 0; i != this->m_dim; i++) {
            T elem = *this->uup.get()[i];
            T elem_tmp = elem - static_cast<T>(1.0);
            this->uup.get()[i].reset(new T (elem_tmp));
        }

        return *this;
    }

    // -- operator --> version --> 2

    const VectorUnique operator--(int)
    {
        --(*this);
        return *this;
    }

    // += operator --> version --> 1

    const VectorUnique operator+=(const VectorUnique & other)
    {
        *this = *this + other;
        return *this;
    }

    // += operator --> version --> 2

    const VectorUnique operator+=(const T & elem)
    {
        *this = *this + elem;
        return *this;
    }

    // -= operator --> version --> 1

    const VectorUnique operator-=(const VectorUnique & other)
    {
        *this = *this - other;
        return *this;
    }

    // -= operator --> version --> 2

    const VectorUnique operator-=(const T & elem)
    {
        *this = *this - elem;
        return *this;
    }

    // *= operator --> version --> 1

    const VectorUnique operator*=(const VectorUnique & other)
    {
        *this = *this * other;
        return *this;
    }

    // *= operator --> version --> 2

    const VectorUnique operator*=(const T & elem)
    {
        *this = *this * elem;
        return *this;
    }

    // /= operator --> version --> 1

    const VectorUnique operator/=(const VectorUnique & other)
    {
        *this = *this / other;
        return *this;
    }

    // /= operator --> version --> 2

    const VectorUnique operator/=(const T & elem)
    {
        *this = *this / elem;
        return *this;
    }

    // destructor

    virtual ~VectorUnique()
    {
        uup.release();
    }

private:

    unsigned long int m_dim;
    unique_ptr<unique_ptr<T>> uup;
};

//=====//
// end //
//=====//

#endif
