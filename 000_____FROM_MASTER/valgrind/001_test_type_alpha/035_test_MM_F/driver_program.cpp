//============//
// unique_ptr //
//============//

#include <iostream>
#include <memory>

using std::endl;
using std::cout;
using std::cin;
using std::unique_ptr;

// the main function

int main ()
{
    const int DIM = 1000000;

    for (int iK = 0; iK != 100; iK++) {
        unique_ptr<int[]> foo (new int [DIM]);

        for (int i = 0; i != DIM; ++i) {
            foo[i] = i;
        }
    }

    return 0;
}

// end
