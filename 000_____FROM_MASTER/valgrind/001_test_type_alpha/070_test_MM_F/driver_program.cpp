
//================//
// NO MEMORY LEAK //
//================//

#include <iostream>
#include <memory>
#include <cmath>

using std::endl;
using std::cout;
using std::cin;
using std::unique_ptr;
using std::shared_ptr;
using std::make_shared;
using std::pow;

// class A

template<class T>
class A {
public:

    // default constructor

    A() : sp(make_shared<T>(T(11.1))), up(), p(new T(33.3)) {}

    // copy constructor

    A(const A & other) : sp(make_shared<T>(*other.sp)), up(), p(new T(*other.p)) {}

    // assignment operator

    A & operator=(const A & other)
    {
        sp = make_shared<T>(*other.sp);
        //up();
        *p = *other.p;

        return *this;
    }

    // destructor

    virtual ~A()
    {
        delete p;
        p = 0;
    }

private:

    shared_ptr<T> sp;
    unique_ptr<T> up;
    T * p;

};

// the main function

int main()
{
    const unsigned long int TRIALS = static_cast<unsigned long int>(pow(10.0, 1.0));
    const int TR2 = 10;

    A<double> a1;
    cout << " sizeof(a1) = " << sizeof(a1) << endl;

    A<double> a2(a1);
    cout << " sizeof(a2) = " << sizeof(a2) << endl;

    a2 = a1;

    for (int ik = 0; ik != TR2; ik++) {
        cout << "------------------------------------------------>> ik = " << ik << endl;

        for (unsigned long int i = 0; i != TRIALS; i++) {
            A<double> a1;
            A<double> a2(a1);
            A<double> a3;

            a2 = a1;
            a3 = a2;
        }
    }

    return 0;
}

// end
