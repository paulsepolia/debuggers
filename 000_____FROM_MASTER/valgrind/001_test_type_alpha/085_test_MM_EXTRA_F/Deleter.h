
#ifndef DELETER_H
#define DELETER_H

#include <memory>

using std::shared_ptr;

//==================//
// class Deleter<T> //
//==================//

template<class T>
class Deleter {
public:
    // (a)

    void operator()(T* p) const
    {
        delete p;
    }

    // (b)

    void operator()(shared_ptr<T> * p) const
    {
        delete [] p;
    }
};

//=====//
// end //
//=====//

#endif
