
//===========================//
// unique_ptr::reset example //
//===========================//

#include <iostream>
#include <memory>

using std::cout;
using std::endl;
using std::cin;
using std::unique_ptr;
using std::addressof;

// deleter

template<class T>
struct D {
    void operator() (T * p)
    {
        delete [] p;
    }
};


//===================//
// the main function //
//===================//

int main ()
{
    const int DIM = 5000000;
    const int TRIALS = 10;

    for (int ik = 0; ik != TRIALS; ik++) {
        cout << "-------------------------------------------------->> " << ik << endl;

        unique_ptr<int[]> upA(new int [DIM]);

        cout << "-------------------------->> A " << endl;

        for (int i = 0; i != DIM; i++) {
            upA[i] = i;
        }

        cout << " upA[0] = " << upA[0] << endl;
        cout << " upA[1] = " << upA[1] << endl;
        cout << " upA[2] = " << upA[2] << endl;
        cout << " upA[3] = " << upA[3] << endl;

        cout << " addressof(upA[0]) = " << addressof(upA[0]) << endl;
        cout << " addressof(upA[1]) = " << addressof(upA[1]) << endl;
        cout << " addressof(upA[2]) = " << addressof(upA[2]) << endl;
        cout << " addressof(upA[3]) = " << addressof(upA[3]) << endl;

        cout << " &upA[0] = " << &upA[0] << endl;
        cout << " &upA[1] = " << &upA[1] << endl;
        cout << " &upA[2] = " << &upA[2] << endl;
        cout << " &upA[3] = " << &upA[3] << endl;

        cout << " *&upA[0] = " << *&upA[0] << endl;
        cout << " *&upA[1] = " << *&upA[1] << endl;
        cout << " *&upA[2] = " << *&upA[2] << endl;
        cout << " *&upA[3] = " << *&upA[3] << endl;

        upA.reset(nullptr);

        cout << "-------------------------->> B " << endl;

        unique_ptr<int[], D<int>> upB(new int [DIM], D<int>());

        for (int i = 0; i != DIM; i++) {
            upB[i] = i+10;
        }

        cout << " upB[0] = " << upB[0] << endl;
        cout << " upB[1] = " << upB[1] << endl;
        cout << " upB[2] = " << upB[2] << endl;
        cout << " upB[3] = " << upB[3] << endl;

        cout << " addressof(upB[0]) = " << addressof(upB[0]) << endl;
        cout << " addressof(upB[1]) = " << addressof(upB[1]) << endl;
        cout << " addressof(upB[2]) = " << addressof(upB[2]) << endl;
        cout << " addressof(upB[3]) = " << addressof(upB[3]) << endl;

        cout << " &upB[0] = " << &upB[0] << endl;
        cout << " &upB[1] = " << &upB[1] << endl;
        cout << " &upB[2] = " << &upB[2] << endl;
        cout << " &upB[3] = " << &upB[3] << endl;

        cout << " *&upB[0] = " << *&upB[0] << endl;
        cout << " *&upB[1] = " << *&upB[1] << endl;
        cout << " *&upB[2] = " << *&upB[2] << endl;
        cout << " *&upB[3] = " << *&upB[3] << endl;

        upB.reset(nullptr);
    }

    return 0;
}

//=====//
// END //
//=====//
