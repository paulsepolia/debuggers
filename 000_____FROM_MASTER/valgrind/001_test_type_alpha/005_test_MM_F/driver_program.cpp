
#include <iostream>
#include <cstring>

using std::endl;
using std::cout;
using std::cin;

// struct

struct A {
    char name[40];
    int age;
};

// the main function

int main ()
{
    const int DIM = 10000;

    A * person = new A [DIM];

    char myname[] = "Pierre de Fermat";
    int myage = 123;

    // initialize person[0]

    memcpy(person[0].name, myname, strlen(myname)+1);

    memcpy(&person[0].age, &myage, sizeof(myage));

    // output

    cout << " --> person[0].name = " << person[0].name << endl;
    cout << " --> person[0].age = " << person[0].age << endl;

    // copy all

    for (int i = 1; i != DIM; i++) {
        memcpy(&person[i], &person[0], sizeof(person[0]));
    }

    // output

    cout << " --> person[1].name = " << person[1].name << endl;
    cout << " --> person[1].age = " << person[1].age << endl;

    // free up heap

    delete [] person;

    return 0;
}

// end
