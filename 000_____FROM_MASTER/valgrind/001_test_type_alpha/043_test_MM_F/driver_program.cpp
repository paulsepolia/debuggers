
//===========//
// EXCELLENT //
// EXCELLENT //
//===========//

//============//
// shared_ptr //
//============//

#include <iostream>
#include <memory>
#include <vector>
#include <cmath>

using std::endl;
using std::cout;
using std::cin;
using std::shared_ptr;
using std::pow;

// the main function

int main()
{
    const int TRIALS = 3;
    const int DIM = 100000000;
    const double ELEM = 12.34;

    for (int ik = 0; ik != TRIALS; ik++) {
        cout << "-------------------------------------------->> " << ik << endl;

        shared_ptr<shared_ptr<double>> da;

        da.reset(new shared_ptr<double>[DIM], [](shared_ptr<double> * p) {
            delete [] p;
        } );

        da.get()[0].reset(new double (ELEM+ik), [](double * p) {
            delete [] p;
        } );

        da.get()[1].reset(new double (ELEM+ik+1), [](double * p) {
            delete [] p;
        } );

        da.get()[2].reset(new double (ELEM+ik+2), [](double * p) {
            delete [] p;
        } );

        cout << *(da.get()[0]) << endl;
        cout << *(da.get()[1]) << endl;
        cout << *(da.get()[2]) << endl;

    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

// end
