
#ifndef VECTORSHARED_H
#define VECTORSHARED_H

#include <memory>

using std::shared_ptr;
using std::make_shared;

#include "Deleter.h"

//========================//
// class: VectorShared<T> //
//========================//

template<class T>
class VectorShared {
public:

    // default constructor

    VectorShared() : m_dim(0), ssp(0) {}

    // constructor

    explicit VectorShared(const unsigned long int & dim) : m_dim(dim)
    {
        this->allocate(m_dim);
    }

    // copy constructor

    VectorShared(const VectorShared & other) : m_dim(other.m_dim)
    {
        this->allocate(m_dim);
        for (unsigned long int i = 0; i != other.m_dim; i++) {
            T elem = *other.ssp.get()[i];
            this->ssp.get()[i].reset(new T (elem), Deleter<T>());
        }
    }

    // allocate

    void allocate(const unsigned long int & DIM)
    {
        this->m_dim = DIM;
        this->ssp.reset(new shared_ptr<T> [DIM], Deleter<T>());
    }

    // set element

    void set_element(const unsigned long int & index, const T & elem)
    {
        this->ssp.get()[index].reset(new T (elem), Deleter<T>());
    }

    // get element

    T get_element(const unsigned long int & index)
    {
        T elem = *this->ssp.get()[index];
        return elem;
    }

    // assignment operator

    const VectorShared & operator=(const VectorShared & other)
    {
        for (unsigned long int i = 0; i != other.m_dim; i++) {
            T elem = *other.ssp.get()[i];
            this->ssp.get()[i].reset(new T (elem), Deleter<T>());
        }

        return *this;
    }

    // assignment operator

    const VectorShared & operator=(const T & elem)
    {
        for (unsigned long int i = 0; i != this->m_dim; i++) {
            this->ssp.get()[i].reset(new T (elem), Deleter<T>());
        }

        return *this;
    }

    // + operator --> version --> 1

    const VectorShared operator+(const VectorShared & other)
    {
        VectorShared tmp(other.m_dim);

        for (unsigned long int i = 0; i != other.m_dim; i++) {
            T elem1 = *other.ssp.get()[i];
            T elem2 = *this->ssp.get()[i];
            T elem_tmp = elem2 + elem1;
            tmp.ssp.get()[i].reset(new T (elem_tmp), Deleter<T>());
        }

        return tmp;
    }

    // + operator --> version --> 2

    const VectorShared operator+(const T & elem)
    {
        VectorShared tmp(this->m_dim);

        for (unsigned long int i = 0; i != this->m_dim; i++) {
            T elem1 = *this->ssp.get()[i];
            T elem_tmp = elem1 + elem;
            tmp.ssp.get()[i].reset(new T (elem_tmp), Deleter<T>());
        }

        return tmp;
    }

    // - operator --> version --> 1

    const VectorShared operator-(const VectorShared & other)
    {
        VectorShared tmp(other.m_dim);

        for (unsigned long int i = 0; i != other.m_dim; i++) {
            T elem1 = *other.ssp.get()[i];
            T elem2 = *this->ssp.get()[i];
            T elem_tmp = elem2 - elem1;
            tmp.ssp.get()[i].reset(new T (elem_tmp), Deleter<T>());
        }

        return tmp;
    }

    // - operator --> version --> 2

    const VectorShared operator-(const T & elem)
    {
        VectorShared tmp(this->m_dim);

        for (unsigned long int i = 0; i != this->m_dim; i++) {
            T elem1 = *this->ssp.get()[i];
            T elem_tmp = elem1 - elem;
            tmp.ssp.get()[i].reset(new T (elem_tmp), Deleter<T>());
        }

        return tmp;
    }

    // * operator --> version --> 1

    const VectorShared operator*(const VectorShared & other)
    {
        VectorShared tmp(other.m_dim);

        for (unsigned long int i = 0; i != other.m_dim; i++) {
            T elem1 = *other.ssp.get()[i];
            T elem2 = *this->ssp.get()[i];
            T elem_tmp = elem2 * elem1;
            tmp.ssp.get()[i].reset(new T (elem_tmp), Deleter<T>());
        }

        return tmp;
    }

    // * operator --> version --> 2

    const VectorShared operator*(const T & elem)
    {
        VectorShared tmp(this->m_dim);

        for (unsigned long int i = 0; i != this->m_dim; i++) {
            T elem1 = *this->ssp.get()[i];
            T elem_tmp = elem1 * elem;
            tmp.ssp.get()[i].reset(new T (elem_tmp), Deleter<T>());
        }

        return tmp;
    }

    // / operator --> version --> 1

    const VectorShared operator/(const VectorShared & other)
    {
        VectorShared tmp(other.m_dim);

        for (unsigned long int i = 0; i != other.m_dim; i++) {
            T elem1 = *other.ssp.get()[i];
            T elem2 = *this->ssp.get()[i];
            T elem_tmp = elem2 / elem1;
            tmp.ssp.get()[i].reset(new T (elem_tmp), Deleter<T>());
        }

        return tmp;
    }

    // / operator --> version --> 2

    const VectorShared operator/(const T & elem)
    {
        VectorShared tmp(this->m_dim);

        for (unsigned long int i = 0; i != this->m_dim; i++) {
            T elem1 = *this->ssp.get()[i];
            T elem_tmp = elem1 / elem;
            tmp.ssp.get()[i].reset(new T (elem_tmp), Deleter<T>());
        }

        return tmp;
    }

    // ++ operator --> version --> 1

    const VectorShared operator++()
    {
        for (unsigned long int i = 0; i != this->m_dim; i++) {
            T elem = *this->ssp.get()[i];
            T elem_tmp = elem + static_cast<T>(1.0);
            this->ssp.get()[i].reset(new T (elem_tmp), Deleter<T>());
        }

        return *this;
    }

    // ++ operator --> version --> 2

    const VectorShared operator++(int)
    {
        ++(*this);
        return *this;
    }

    // -- operator --> version --> 1

    const VectorShared operator--()
    {
        for (unsigned long int i = 0; i != this->m_dim; i++) {
            T elem = *this->ssp.get()[i];
            T elem_tmp = elem - static_cast<T>(1.0);
            this->ssp.get()[i].reset(new T (elem_tmp), Deleter<T>());
        }

        return *this;
    }

    // -- operator --> version --> 2

    const VectorShared operator--(int)
    {
        --(*this);
        return *this;
    }

    // += operator --> version --> 1

    const VectorShared operator+=(const VectorShared & other)
    {
        *this = *this + other;
        return *this;
    }

    // += operator --> version --> 2

    const VectorShared operator+=(const T & elem)
    {
        *this = *this + elem;
        return *this;
    }

    // -= operator --> version --> 1

    const VectorShared operator-=(const VectorShared & other)
    {
        *this = *this - other;
        return *this;
    }

    // -= operator --> version --> 2

    const VectorShared operator-=(const T & elem)
    {
        *this = *this - elem;
        return *this;
    }

    // *= operator --> version --> 1

    const VectorShared operator*=(const VectorShared & other)
    {
        *this = *this * other;
        return *this;
    }

    // *= operator --> version --> 2

    const VectorShared operator*=(const T & elem)
    {
        *this = *this * elem;
        return *this;
    }

    // /= operator --> version --> 1

    const VectorShared operator/=(const VectorShared & other)
    {
        *this = *this / other;
        return *this;
    }

    // /= operator --> version --> 2

    const VectorShared operator/=(const T & elem)
    {
        *this = *this / elem;
        return *this;
    }

    // destructor

    virtual ~VectorShared() {}

private:

    unsigned long int m_dim;
    shared_ptr<shared_ptr<T>> ssp;
};

//=====//
// end //
//=====//

#endif
