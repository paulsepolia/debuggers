
//===============================//
// SHARED POINTER BASED VECTOR   //
// vs                            //
// ORDINARY POINTER BASED VECTOR //
//===============================//

#include <iostream>
#include <cmath>
#include <iomanip>
#include <ctime>

using std::endl;
using std::cout;
using std::cin;
using std::pow;
using std::fixed;
using std::setprecision;

#include "Vector.h"
#include "VectorShared.h"

//===================//
// the main function //
//===================//

int main()
{
    // local parameters

    const unsigned long int DIM = static_cast<unsigned long int>(pow(10.0, 7.2));
    const unsigned long int TRIALS = 2*static_cast<unsigned long int>(pow(10.0, 0.0));

    // local variables

    time_t t1;
    time_t t2;

    // adjust output

    cout << fixed;
    cout << setprecision(6);

    for (unsigned long int i = 0; i != TRIALS; i++) {

        cout << "-------------------------------------------------------->> " << i << endl;

        cout << "--> 1  --> declare shared vectors" << endl;

        t1 = clock();

        VectorShared<double> vsh1;
        VectorShared<double> vsh2;

        t2 = clock();

        cout << "--> time taken is = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << "--> 2  --> declare vectors" << endl;

        t1 = clock();

        Vector<double> v1;
        Vector<double> v2;

        t2 = clock();

        cout << "--> time taken is = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << "--> 3  --> allocate shared vectors" << endl;

        t1 = clock();

        vsh1.allocate(DIM);
        vsh2.allocate(DIM);

        t2 = clock();

        cout << "--> time taken is = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << "--> 4  --> allocate vectors" << endl;

        t1 = clock();

        v1.allocate(DIM);
        v2.allocate(DIM);

        t2 = clock();

        cout << "--> time taken is = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << "--> 5  --> build shared vectors" << endl;

        t1 = clock();

        for (unsigned long int j = 0; j != DIM; j++) {
            vsh1.set_element(j, static_cast<double>(j));
            vsh2.set_element(j, static_cast<double>(0.1+j));
        }

        t2 = clock();

        cout << "--> time taken is = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << "--> 6  --> build vectors" << endl;

        t1 = clock();

        for (unsigned long int j = 0; j != DIM; j++) {
            v1.set_element(j, static_cast<double>(j));
            v2.set_element(j, static_cast<double>(0.1+j));
        }

        t2 = clock();

        cout << "--> time taken is = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << "--> 7  --> set equal two shared vectors" << endl;

        t1 = clock();

        vsh1 = vsh2;

        t2 = clock();

        cout << "--> time taken is = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << "--> 8  --> set equal two vectors" << endl;

        t1 = clock();

        v1 = v2;

        t2 = clock();

        cout << "--> time taken is = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << "--> 9  --> declare via copy-constructor a shared vector" << endl;

        t1 = clock();

        VectorShared<double> vsh3(DIM);

        t2 = clock();

        cout << "--> time taken is = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << "--> 10 --> declare via copy-constructor a vector" << endl;

        t1 = clock();

        Vector<double> v3(DIM);

        t2 = clock();

        cout << "--> time taken is = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << "--> 11 --> add two shared vectors" << endl;

        t1 = clock();

        vsh3 = vsh1 + vsh2;

        t2 = clock();

        cout << "--> time taken is = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << "--> 12 --> add two vectors" << endl;

        t1 = clock();

        v3 = v1 + v2;

        t2 = clock();

        cout << "--> time taken is = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << "--> 13 --> subtract two shared vectors" << endl;

        t1 = clock();

        vsh3 = vsh1 - vsh2;

        t2 = clock();

        cout << "--> time taken is = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << "--> 14 --> subtract two vectors" << endl;

        t1 = clock();

        v3 = v1 - v2;

        t2 = clock();

        cout << "--> time taken is = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << "--> 15 --> multiply two shared vectors" << endl;

        t1 = clock();

        vsh3 = vsh1 * vsh2;

        t2 = clock();

        cout << "--> time taken is = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << "--> 16 --> multiply two vectors" << endl;

        t1 = clock();

        v3 = v1 * v2;

        t2 = clock();

        cout << "--> time taken is = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << "--> 17 --> divide two shared vectors" << endl;

        t1 = clock();

        vsh3 = vsh1 / vsh2;

        t2 = clock();

        cout << "--> time taken is = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << "--> 18 --> divide two vectors" << endl;

        t1 = clock();

        v3 = v1 / v2;

        t2 = clock();

        cout << "--> time taken is = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    }

    return 0;
}

//=====//
// end //
//=====//
