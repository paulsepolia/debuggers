
#include <iostream>
#include <cstring>

using std::endl;
using std::cout;
using std::cin;

struct {
    char name[40];
    int age;
} person, person_copy;

int main ()
{
    char myname[] = "Pierre de Fermat";

    // using memcpy to copy string

    memcpy(person.name, myname, strlen(myname)+1000);

    person.age = 46;

    // using memcpy to copy structure

    memcpy(&person_copy, &person, sizeof(person));

    cout << " --> person_copy.name = " << person_copy.name << endl;
    cout << " --> person_copy.age = " << person_copy.age << endl;

    return 0;
}

// end
