
#include <iostream>
#include <cmath>

using std::endl;
using std::cout;
using std::cin;
using std::pow;

// --> functions declaration

// (a)

double larger(double x, double y);

// (b)

double compareThree(double x, double y, double z);

// --> main function

int main()
{
    cout << "The larger of 5 and 10 is " << larger(5, 10) << endl;

    cout << "The largest of 43.48, 34.001 and 12.65 is "
         << compareThree(43.48, 34.001, 12.65) << endl;

    return 0;
}

// --> functions definition

// (a)

double larger(double x, double y)
{
    double max;

    if (x >= y) {
        max = x;
    } else {
        max = y;
    }

    return max;
}

// (b)

double compareThree(double x, double y, double z)
{
    const unsigned long int DIM_MAX = static_cast<unsigned long int>(pow(10.0, 9.0));
    int * aLarge = new int [DIM_MAX];
    for (unsigned long int i = 0; i != DIM_MAX; i++) {
        aLarge[i] = -1;
    }


    return larger(x, larger(y,z));
}

// end
