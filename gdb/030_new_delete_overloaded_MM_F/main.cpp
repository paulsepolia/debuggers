#include <iostream>
#include <memory>

void *operator new(size_t size)
{
    const auto memory = malloc(size);
    std::cout << "pgg --> Allocating " << size << " bytes at address " << memory << std::endl;
    return memory;
}

void operator delete(void *memory, size_t size)
{
    std::cout << "pgg --> Deleting " << size << " bytes at address " << memory << std::endl;
    free(memory);
}

void operator delete(void *memory)
{
    std::cout << "pgg --> Deleting at address " << memory << std::endl;
    free(memory);
}

int main()
{
    {
        std::cout << "------------------>> 1" << std::endl;
        int *x1 = new int(100);
        delete x1;
        x1 = nullptr;
    }

    {
        std::cout << "------------------>> 2" << std::endl;
        int *x1 = new int[100];
        delete[] x1;
        x1 = nullptr;
    }

    {
        std::cout << "------------------>> 3" << std::endl;
        std::unique_ptr<double> p(new double(123.456));
    }

    {
        std::cout << "------------------>> 4" << std::endl;
        std::shared_ptr<double> p(new double(123.456));
    }

    {
        // that does not reporting properly
        std::cout << "------------------>> 5" << std::endl;
        std::shared_ptr<double> p(new double[100]);
    }
}