#include <iostream>
#include <vector>
#include <map>

int main()
{
    double *p1 = nullptr;
    std::vector<double> p2(1, 123.456);
    double p3 = 100;
    std::map<double, double> p4;
    auto *p5 = new double(123);

    p4.insert({1, 1});
    p4.insert({2, 2});
    p4.insert({3, 3});

    //======================//
    // STACK type addresses //
    //======================//

    std::cout << "STACK STYLE ADDRESSES" << std::endl;

    std::cout << "------>> 1" << std::endl;
    std::cout << &p1 << std::endl;
    std::cout << "------>> 2" << std::endl;
    std::cout << &p2 << std::endl;
    std::cout << "------>> 3" << std::endl;
    std::cout << &p3 << std::endl;
    std::cout << "------>> 4" << std::endl;
    std::cout << &p4 << std::endl;

    std::cout << "HEAP STYLE ADDRESSES" << std::endl;

    std::cout << "------>> 5" << std::endl;
    std::cout << &p2[0] << std::endl;
    std::cout << "------>> 6" << std::endl;
    std::cout << &p4[1] << std::endl;
    std::cout << "------>> 7" << std::endl;
    std::cout << p5 << std::endl;

    p2.clear();
    p2.shrink_to_fit();
    p4.clear();

    std::cout << "------>> 8" << std::endl;
    std::cout << &p2[0] << std::endl;
    std::cout << "------>> 9" << std::endl;
    std::cout << &p4[1] << std::endl;
    std::cout << "------>>10" << std::endl;
    std::cout << &p4[2] << std::endl;
}
