#include <iostream>

int main()
{
    int static i = 0;
    ++i;
    if (i == 1000)
    {
        std::cout << i << std::endl;
        return 0;
    }
    main();
}