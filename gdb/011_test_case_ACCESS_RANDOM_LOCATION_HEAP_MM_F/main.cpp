#include <iostream>

int main()
{
    const size_t DO_MAX = 1'000'000'000UL;
    const size_t HEAP_SIZE = 10'000;
    auto *p1 = new double[HEAP_SIZE];

    for (size_t i = 0; i < HEAP_SIZE; i++)
    {
        p1[i] = (double) i;
    }

    p1 = &p1[HEAP_SIZE - 1];

    for (size_t i = 0; i < DO_MAX; i++)
    {
        std::cout << "------------------>> i = " << i << std::endl;
        std::cout << *p1 << std::endl;
        p1++;
    }

    delete[] p1;
}
