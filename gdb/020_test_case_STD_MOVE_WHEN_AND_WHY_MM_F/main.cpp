#include <iostream>
#include <vector>
#include <chrono>
#include <memory>

std::vector<double> f1(double &tf)
{
    const auto t1 = std::chrono::steady_clock::now();
    const size_t DIM = 100'000'000;
    std::vector<double> v;
    v.reserve(DIM);
    for (size_t i = 0; i < DIM; i++)
    {
        v[i] = (double) i;
    }

    const auto t2 = std::chrono::steady_clock::now();
    std::chrono::duration<double> dt = t2 - t1;
    tf = dt.count();

    return v;
}

const std::vector<double> *f2(double &tf)
{
    const auto t1 = std::chrono::steady_clock::now();
    const size_t DIM = 100'000'000;
    auto *v = new std::vector<double>();
    v->reserve(DIM);
    for (size_t i = 0; i < DIM; i++)
    {
        (*v)[i] = (double) i;
    }

    const auto t2 = std::chrono::steady_clock::now();
    std::chrono::duration<double> dt = t2 - t1;
    tf = dt.count();

    return v;
}

std::shared_ptr<std::vector<double>> f3(double &tf)
{
    const auto t1 = std::chrono::steady_clock::now();
    const size_t DIM = 100'000'000;
    auto v = std::make_shared<std::vector<double>>();
    v->reserve(DIM);
    for (size_t i = 0; i < DIM; i++)
    {
        (*v)[i] = (double) i;
    }

    const auto t2 = std::chrono::steady_clock::now();
    std::chrono::duration<double> dt = t2 - t1;
    tf = dt.count();

    return v;
}

std::unique_ptr<std::vector<double>> f4(double &tf)
{
    const auto t1 = std::chrono::steady_clock::now();
    const size_t DIM = 100'000'000;
    auto v = std::make_unique<std::vector<double>>();
    v->reserve(DIM);
    for (size_t i = 0; i < DIM; i++)
    {
        (*v)[i] = (double) i;
    }

    const auto t2 = std::chrono::steady_clock::now();
    std::chrono::duration<double> dt = t2 - t1;
    tf = dt.count();

    return v;
}

int main()
{
    {
        std::vector<double> v;
        double tf = 0;
        const auto t1 = std::chrono::steady_clock::now();
        v = f1(tf);
        const auto t2 = std::chrono::steady_clock::now();
        std::chrono::duration<double> dt = t2 - t1;
        std::cout << "time to move data = " << (dt.count() - tf) << std::endl;

        std::cout << v[0] << std::endl;
        std::cout << v[1] << std::endl;
        std::cout << v[2] << std::endl;
    }

    {
        const std::vector<double> *v;
        double tf = 0;
        const auto t1 = std::chrono::steady_clock::now();
        v = f2(tf);
        const auto t2 = std::chrono::steady_clock::now();
        std::chrono::duration<double> dt = t2 - t1;
        std::cout << "time to move data = " << (dt.count() - tf) << std::endl;

        std::cout << (*v)[0] << std::endl;
        std::cout << (*v)[1] << std::endl;
        std::cout << (*v)[2] << std::endl;

        delete v;
        v = nullptr;
    }

    {
        std::shared_ptr<std::vector<double>> v;
        double tf = 0;
        const auto t1 = std::chrono::steady_clock::now();
        v = f3(tf);
        const auto t2 = std::chrono::steady_clock::now();
        std::chrono::duration<double> dt = t2 - t1;
        std::cout << "time to move data = " << (dt.count() - tf) << std::endl;

        std::cout << (*v)[0] << std::endl;
        std::cout << (*v)[1] << std::endl;
        std::cout << (*v)[2] << std::endl;
    }

    {
        std::unique_ptr<std::vector<double>> v;
        double tf = 0;
        const auto t1 = std::chrono::steady_clock::now();
        v = f4(tf);
        const auto t2 = std::chrono::steady_clock::now();
        std::chrono::duration<double> dt = t2 - t1;
        std::cout << "time to move data = " << (dt.count() - tf) << std::endl;

        std::cout << (*v)[0] << std::endl;
        std::cout << (*v)[1] << std::endl;
        std::cout << (*v)[2] << std::endl;
    }
}