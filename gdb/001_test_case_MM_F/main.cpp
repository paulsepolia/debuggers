#include <iostream>
#include <vector>

void f0(double x)
{
    std::cout << "f0 HERE" << std::endl;
    std::cout << "x = " << x << std::endl;
    std::cout << "&x = " << &x << std::endl;
}

void f1(double x)
{
    std::cout << "x = " << x << std::endl;
    std::cout << "&x = " << &x << std::endl;
}

void f2(const double &x)
{
    std::cout << "x = " << x << std::endl;
    std::cout << "&x = " << &x << std::endl;
}

void f3(const double *x)
{
    std::cout << "*x = " << *x << std::endl;
    std::cout << "x = " << x << std::endl;
    std::cout << "&x = " << &x << std::endl;
}

void f4(const double *&x)
{
    std::cout << "*x = " << *x << std::endl;
    std::cout << "x = " << x << std::endl;
    std::cout << "&x = " << &x << std::endl;
}

const double &f5(const double &x)
{
    std::cout << "x = " << x << std::endl;
    std::cout << "&x = " << &x << std::endl;
    return x;
}

class A
{
public:

    A() = default;

    virtual ~A() = default;

    double x1 = 0;
};

class A1 : public A
{
public:

    A1()
    {
        x1 = 1.5;
        x2 = 1.5;
    }

    ~A1() override = default;

    double x1 = 1;
    double x2 = 1;
};

class A2 final : public A1
{
public:

    A2()
    {
        x1 = 2.5;
        x2 = 2.5;
        x3 = 2.5;
    }

    ~A2() final = default;

    double x1 = 2;
    double x2 = 2;
    double x3 = 2;
};

int main()
{
    std::cout << "-------------------->> 1" << std::endl;
    double x1 = 1.0;
    std::cout << &x1 << std::endl;
    f1(x1);

    std::cout << "-------------------->> 2" << std::endl;
    double x2 = 2.0;
    std::cout << &x2 << std::endl;
    f2(x2);

    std::cout << "-------------------->> 3" << std::endl;
    const auto *x3 = new double(3.0);
    std::cout << *x3 << std::endl;
    std::cout << x3 << std::endl;
    std::cout << &x3 << std::endl;
    f3(x3);

    std::cout << "-------------------->> 4" << std::endl;
    const auto *x4 = new double(4.0);
    std::cout << *x4 << std::endl;
    std::cout << x4 << std::endl;
    std::cout << &x4 << std::endl;
    f4(x4);

    std::cout << "-------------------->> 5" << std::endl;
    A *x5 = new A();
    std::cout << x5->x1 << std::endl;

    std::cout << "-------------------->> 6" << std::endl;
    A *x6 = new A1();
    std::cout << dynamic_cast<A1 *>(x6)->x1 << std::endl;
    std::cout << dynamic_cast<A1 *>(x6)->x2 << std::endl;

    std::cout << "-------------------->> 7" << std::endl;
    A *x7 = new A2();
    std::cout << dynamic_cast<A2 *>(x7)->x1 << std::endl;
    std::cout << dynamic_cast<A2 *>(x7)->x2 << std::endl;
    std::cout << dynamic_cast<A2 *>(x7)->x3 << std::endl;

    std::cout << "-------------------->> 8" << std::endl;
    A *x8 = new A2();
    std::cout << ((A2 *) x8)->x1 << std::endl;
    std::cout << ((A2 *) x8)->x2 << std::endl;
    std::cout << ((A2 *) x8)->x3 << std::endl;

    std::cout << "-------------------->> 9" << std::endl;
    A *x9 = new A2();
    std::cout << ((A1 *) x9)->x1 << std::endl;
    std::cout << ((A1 *) x9)->x2 << std::endl;

    std::cout << "-------------------->>10" << std::endl;
    double x10Local = 10;
    const double &x10 = f5(x10Local);
    std::cout << x10 << std::endl;
    std::cout << &x10 << std::endl;

    std::cout << "-------------------->>11" << std::endl;
    double x11Local = 11;
    auto &x11 = const_cast<double &>(f5(x11Local));
    std::cout << x11 << std::endl;
    std::cout << &x11 << std::endl;
    x11 = 11.5;
    std::cout << x11Local << std::endl;

    std::cout << "-------------------->>12" << std::endl;

    void (*fp1)(double) = f1;
    void (*fp2)(double) = f1;

    fp1(12.0);
    fp2(12.5);

    std::vector<void (*)(double)> vfp;

    vfp.push_back(f1);
    vfp.push_back(f1);
    vfp.push_back(f1);

    std::cout << vfp.size() << std::endl;
    vfp[0](101);
    vfp[1](102);
    vfp[2](103);

    vfp[0] = f0;
    vfp[1] = f0;
    vfp[2] = f0;
    vfp[0](101);
    vfp[1](102);
    vfp[2](103);


    std::cout << "-------------------->>13" << std::endl;
    // delete all heap
    delete x3;
    delete x4;
    delete x5;
    delete x6;
    delete x7;
    delete x8;
    delete x9;

    std::cout << "-->> 14" << std::endl;
    std::cout << *x3 << std::endl;
    std::cout << *x4 << std::endl;

    x3 = nullptr;
    x4 = nullptr;
    x5 = nullptr;
    x6 = nullptr;
    x7 = nullptr;
    x8 = nullptr;
    x9 = nullptr;
}