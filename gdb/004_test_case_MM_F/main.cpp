#include <iostream>
#include <string>
#include <memory>

std::shared_ptr<double> f1()
{
    std::shared_ptr<double> x(new double(10));
    std::cout << x << std::endl;
    std::cout << *x << std::endl;
    return x;
}

std::unique_ptr<double> f2()
{
    std::unique_ptr<double> x(new double(10));
    std::cout << x.get() << std::endl;
    std::cout << *x << std::endl;
    return x;
}

const double *f3()
{
    const auto *x = new double(10);
    std::cout << x << std::endl;
    std::cout << *x << std::endl;
    return x;
}

int main()
{
    {
        std::cout << "------------------------------>> 1" << std::endl;
        auto x1 = f1();
        std::cout << x1 << std::endl;
        std::cout << *x1 << std::endl;
    }

    {
        std::cout << "------------------------------>> 2" << std::endl;
        auto x2 = f2();
        std::cout << x2.get() << std::endl;
        std::cout << *x2 << std::endl;
    }

    {
        std::cout << "------------------------------>> 3" << std::endl;
        const auto *x3 = f3();
        std::cout << x3 << std::endl;
        std::cout << *x3 << std::endl;
        delete x3;
    }
}