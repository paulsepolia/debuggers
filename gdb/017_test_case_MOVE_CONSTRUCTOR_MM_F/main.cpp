#include <iostream>
#include <algorithm>

class MemoryBlock
{
public:

    explicit MemoryBlock(size_t length)
            : _length(length), _data(new int[length])
    {}

    virtual ~MemoryBlock()
    {
        delete[] _data;
    }

    MemoryBlock(const MemoryBlock &other)
            : _length(other._length),
              _data(new int[other._length])
    {
        std::copy(other._data, other._data + _length, _data);
    }

    MemoryBlock &operator=(const MemoryBlock &other)
    {
        if (this != &other)
        {
            delete[] _data;

            _length = other._length;
            _data = new int[_length];
            std::copy(other._data, other._data + _length, _data);
        }
        return *this;
    }

    MemoryBlock(MemoryBlock &&other) noexcept
            : _length{0},
              _data{nullptr}
    {
        _length = other._length;
        _data = other._data;

        other._length = 0;
        other._data = nullptr;
    }

    MemoryBlock &operator=(MemoryBlock &&other) noexcept
    {
        if (this != &other)
        {
            _length = 0;
            delete[] _data;

            _length = other._length;
            _data = other._data;

            other._length = 0;
            other._data = nullptr;
        }
        return *this;
    }

    [[nodiscard]] size_t Length() const
    {
        return _length;
    }

private:
    size_t _length = 0;
    int *_data = nullptr;
};

int main()
{
    MemoryBlock mb1(10'000);

    std::cout << mb1.Length() << std::endl;

    MemoryBlock mb2(mb1);

    std::cout << mb2.Length() << std::endl;
    std::cout << mb1.Length() << std::endl;

    MemoryBlock mb3(std::move(mb1));
    
    std::cout << mb3.Length() << std::endl;
    std::cout << mb2.Length() << std::endl;
    std::cout << mb1.Length() << std::endl;
}