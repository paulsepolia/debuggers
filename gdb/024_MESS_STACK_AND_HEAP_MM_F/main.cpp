#include <vector>
#include <iostream>

void f1()
{
    auto *x1 = new double(1);
    std::cout << "*x1 = " << *x1 << std::endl;
    delete x1;
}

void f2()
{
    auto *x2 = new double(2);
    std::cout << "*x2 = " << *x2 << std::endl;
    delete x2;
    *x2 = 100; // IF I DO THIS I DESTROY ANY FURTHER HEAP ALLOCATIONS
    x2 = nullptr;
}

void f3()
{
    auto *x3 = new double(3);
    std::cout << "*x3 = " << *x3 << std::endl;
    delete x3;
    x3 = nullptr;
}

int main()
{
    std::cout << "----------------------->> 1" << std::endl;

    f1();
    f2();
    f3();

    const size_t DIM = 10000;
    auto *x3 = new double[DIM];
    x3[0] = 0;
    x3[1] = 1;
    x3[2] = 2;
}
