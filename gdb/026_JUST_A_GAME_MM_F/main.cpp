#include <vector>
#include <iostream>

class A
{
public:

    size_t f()
    {
        return (size_t)(&x);
    }

    [[nodiscard]] double get_x() const
    {
        return x;
    }

private:

    double x = 0;
};

int main()
{
    A a;
    size_t a1 = a.f();
    double *x1 = nullptr;
    x1 = (double*)(a1);
    *x1 = 123.456;
    std::cout << a.get_x() << std::endl;

    *x1 = 234.567;
    std::cout << a.get_x() << std::endl;

    *x1 = 345.678;
    std::cout << a.get_x() << std::endl;
}
