#include <vector>
#include <iostream>

class Singleton1
{
public:

    size_t DIM = 10000;

    static const Singleton1 &getInstance()
    {
        static Singleton1 theOnlyInstance{};

        return theOnlyInstance;
    }

    Singleton1(const Singleton1 &) = delete;

    Singleton1(Singleton1 &&) = delete;

    Singleton1 &operator=(const Singleton1 &) = delete;

    Singleton1 &operator=(Singleton1 &&) = delete;

    double x1 = 1;
    double x2 = 2;
    double *x3 = new double[DIM];

private:
    Singleton1() = default;
};

class Singleton2
{
public:

    size_t DIM = 10000;

    static const Singleton2 *getInstance()
    {
        static auto *theOnlyInstance = new Singleton2{};
        return theOnlyInstance;
    }

    Singleton2(const Singleton2 &) = delete;

    Singleton2(Singleton2 &&) = delete;

    Singleton2 &operator=(const Singleton2 &) = delete;

    Singleton2 &operator=(Singleton2 &&) = delete;

    double x1 = 1;
    double x2 = 2;
    double *x3 = new double[DIM];

private:
    Singleton2() = default;
};

int main()
{
    std::cout << "----------------------->> 1" << std::endl;

    const Singleton1 &i11 = Singleton1::getInstance();
    const Singleton1 &i12 = Singleton1::getInstance();

    std::cout << &i11 << std::endl;
    std::cout << &i12 << std::endl;

    for (size_t i = 0; i < i11.DIM; i++)
    {
        i11.x3[i] = i;
    }

    std::cout << i11.x3[0] << std::endl;
    std::cout << i11.x3[1] << std::endl;
    std::cout << i11.x3[2] << std::endl;

    std::cout << "----------------------->> 2" << std::endl;

    const Singleton2 *i21 = Singleton2::getInstance();
    const Singleton2 *i22 = Singleton2::getInstance();

    std::cout << i21 << std::endl;
    std::cout << i22 << std::endl;

    for (size_t i = 0; i < i21->DIM; i++)
    {
        i21->x3[i] = i;
    }

    std::cout << i21->x3[0] << std::endl;
    std::cout << i21->x3[1] << std::endl;
    std::cout << i21->x3[2] << std::endl;

    delete[] i21->x3;
    delete[] i11.x3;

    const_cast<Singleton1 &>(i11).x3 = new double[100]; // NO MEMORY LEAK
    const_cast<Singleton2 *>(i21)->x3 = new double[100];  // NO MEMORY LEAK

    i11.x3[0] = -2;
    i11.x3[1] = -1;
    i11.x3[2] = 0;

    std::cout << i11.x3[0] << std::endl;
    std::cout << i11.x3[1] << std::endl;
    std::cout << i11.x3[2] << std::endl;

    i21->x3[0] = -1;
    i21->x3[1] = 0;
    i21->x3[2] = 1;

    std::cout << i21->x3[0] << std::endl;
    std::cout << i21->x3[1] << std::endl;
    std::cout << i21->x3[2] << std::endl;

    //============================================

    auto *p3 = new double(100); // ONLY HERE THERE IS A MEMORY LEAK
    std::cout << *p3 << std::endl;
}
