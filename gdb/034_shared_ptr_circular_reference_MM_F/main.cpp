#include <iostream>
#include <memory>

class Resource
{
public:
    std::shared_ptr<Resource> m_ptr;

    Resource()
    {
        std::cout << "Resource acquired" << std::endl;
    }

    ~Resource()
    {
        std::cout << "Resource destroyed" << std::endl;
    }
};

int main()
{
    const auto ptr1 = std::make_shared<Resource>();

    ptr1->m_ptr = ptr1; // m_ptr is now sharing the Resource that contains it

    return 0;
}