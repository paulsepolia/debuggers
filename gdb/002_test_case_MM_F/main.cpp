#include <iostream>
#include <vector>

class A
{
public:

    int x = 0;

    virtual void f() const
    {
        std::cout << x << std::endl;
    }
};

class A1 : public A
{
public:

    int x = 1;

    void f() const override
    {
        std::cout << x << std::endl;
    }
};

class A2 : public A
{
public:

    int x = 2;

    void f() const override
    {
        std::cout << x << std::endl;
    }
};

class A3 : public A
{
public:

    int x = 3;

    void f() const override
    {
        std::cout << x << std::endl;
    }
};

class A4 : public A
{
public:

    int x = 4;

    void f() const override
    {
        std::cout << x << std::endl;
    }
};

int main()
{
    std::cout << "------------------->> 1" << std::endl;
    A *p1 = new A1;
    A *p2 = new A2;
    A *p3 = new A3;
    A *p4 = new A4;

    std::cout << "------------------->> 2" << std::endl;
    std::cout << p1->x << std::endl;
    std::cout << p2->x << std::endl;
    std::cout << p3->x << std::endl;
    std::cout << p4->x << std::endl;

    std::cout << "------------------->> 3" << std::endl;
    p1->f();
    p2->f();
    p3->f();
    p4->f();

    std::cout << "------------------->> 4" << std::endl;
    std::cout << dynamic_cast<A1 *>(p1) << std::endl;
    std::cout << dynamic_cast<A2 *>(p2) << std::endl;
    std::cout << dynamic_cast<A3 *>(p3) << std::endl;
    std::cout << dynamic_cast<A4 *>(p4) << std::endl;

    std::vector<A *> v1;

    v1.push_back(p1);
    v1.push_back(p2);
    v1.push_back(p3);
    v1.push_back(p4);

    std::cout << "------------------>> 6" << std::endl;

    for (const auto &el: v1)
    {
        std::cout << el << std::endl;
    }
}