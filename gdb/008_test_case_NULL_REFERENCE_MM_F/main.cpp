#include <iostream>
#include <vector>

class A
{
public:
    int x = 1;
private:
    int x1 = -1;
    int x2 = -2;
};

int main()
{
    std::cout << "----->> 1" << std::endl;
    double x1 = 101;
    std::cout << x1 << std::endl;
    std::cout << &x1 << std::endl;
    double *p1;
    p1 = &x1;
    std::cout << &p1 << std::endl;
    std::cout << p1 << std::endl;
    std::cout << *p1 << std::endl;

    std::cout << "----->> 2" << std::endl;
    A a2{};
    std::cout << &a2 << std::endl;
    int *p2 = (int*)&a2;
    std::cout << *p2 << std::endl;
    p2++;
    std::cout << *p2 << std::endl;
    p2++;
    std::cout << *p2 << std::endl;

    std::cout << "----->> 3" << std::endl;
    A a3{};
    std::cout << &a3 << std::endl;
    int *p3 = &(a2.x);
    std::cout << *p3 << std::endl;
    p3++;
    std::cout << *p3 << std::endl;
    p3++;
    std::cout << *p3 << std::endl;

    std::cout << "----->> 4" << std::endl;
    std::vector<double> v4;
    std::cout << &v4 << std::endl;
    v4.push_back(1);
    v4.push_back(2);
    v4.push_back(3);
    v4.push_back(4);
    std::cout << &v4[0] << std::endl;
    auto *p4 = new double(123);
    std::cout << p4 << std::endl;
    delete p4;
    p4 = nullptr;
    std::cout << *p4 << std::endl;
}
