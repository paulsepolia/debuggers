#include <iostream>
#include <array>

int main()
{
    std::cout << "-->> mark --> 1" << std::endl;
    double x1 = 100;
    std::cout << "-->> mark --> 2" << std::endl;
    double x2 = 200;
    std::cout << "-->> mark --> 3" << std::endl;
    std::array<double, 1'000'000'000> x3{};
    std::cout << "-->> mark --> 4" << std::endl;

    std::cout << "-->>     x1 = " << x1 << std::endl;
    std::cout << "-->>     x2 = " << x2 << std::endl;
    std::cout << "-->>    &x3 = " << &x3 << std::endl;
    std::cout << "-->> &x3[0] = " << &x3[0] << std::endl;
}
