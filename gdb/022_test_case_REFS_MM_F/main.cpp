#include <iostream>
#include <vector>
#include <chrono>
#include <memory>


int main()
{
    double a0 = 102;
    double &a1 = *(new double(101));
    std::cout << a1 << std::endl;
    delete &a1;
    double &a2 = a1;
    std::cout << a2 << std::endl;
    std::cout << &a2 << std::endl;

    a2 = a0;

    std::cout << a2 << std::endl;
    std::cout << &a2 << std::endl;
}