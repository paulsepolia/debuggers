#include <iostream>

class A
{
public:

    A()
    {
        std::cout << "Constructor" << std::endl;
    }

    ~A()
    {
        std::cout << "Destructor" << std::endl;
    }
};

class B
{
public:
    B()
    {
        std::cout << "Constructor" << std::endl;
    }

    ~B()
    {
        std::cout << "Destructor" << std::endl;
    }
};

int main()
{
    A obj;
    std::cout << "pgg --> 1" << std::endl;
    A();
    std::cout << "pgg --> 2" << std::endl;

    B obj2;
}