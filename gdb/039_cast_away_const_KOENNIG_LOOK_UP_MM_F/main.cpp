#include <iostream>

class A
{
public:

    void set(const double &v)
    {
        std::cout << "-->> void set(const double &v)" << std::endl;
        val = v;
    }

    void set(double &v)
    {
        std::cout << "-->> void set(double &v)" << std::endl;
        val = v;
    }

    [[nodiscard]] const double &get() const
    {
        return val;
    }

    [[nodiscard]] const double &constGet() const
    {
        const double *p = &val;
        (*const_cast<double *>(p)) = 111.111;
        return val;
    }

    [[nodiscard]] const double &constGet2() const
    {
        std::cout << "A is const" << std::endl;
        const_cast<double &>(val) = 111.111;
        return val;
    }

    [[nodiscard]] const double &constGet2()
    {
        std::cout << "A is NOT const" << std::endl;
        val = 111.111;
        return val;
    }

private:

    double val = 123.456;
};

namespace PGG
{
    struct X
    {
    };

    void f()
    {
        std::cout << "I am f" << std::endl;
    }

    void g(void(*fun)())
    {
        fun();
    }

    void g(X a)
    {
        std::cout << "I am X" << std::endl;
    }
}

int main()
{
    {
        std::cout << "-------------------->> 0" << std::endl;

        PGG::f();
        PGG::g(PGG::f);
        g(PGG::X()); // COMPILES // KOENING LOOK-UP or ARGUMENT DEPENDENT LOOK-UP
    }

    {
        std::cout << "-------------------->> 1" << std::endl;
        const double d = 123.456;
        double *p = nullptr;
        p = const_cast<double *>(&d);
        *p = 111.111;
        std::cout << *p << std::endl;
    }

    {
        std::cout << "-------------------->> 2" << std::endl;
        double d = 123.456;
        const double *p = &d;
        *const_cast<double *>(p) = 111.111;
        std::cout << *p << std::endl;
    }

    {
        std::cout << "-------------------->> 3" << std::endl;
        double d = 123.456;
        const double *p = &d;
        std::cout << *p << std::endl;
        p++;
        p++;
        p++;
        p++;
        std::cout << *p << std::endl;
    }

    {
        std::cout << "-------------------->> 4" << std::endl;
        const double d = 123.456;
        std::cout << d << std::endl;
        const double &a1 = const_cast<double &>(d) = 111.111;
        std::cout << d << std::endl;
        std::cout << a1 << std::endl;
    }

    {
        std::cout << "-------------------->> 5" << std::endl;
        const A a;
        const_cast<A &>(a).set(111.111);
        std::cout << a.get() << std::endl;
    }

    {
        std::cout << "-------------------->> 6" << std::endl;
        const A *a = new A();
        const_cast<A *>(a)->set(111.111);
        std::cout << a->get() << std::endl;
    }

    {
        std::cout << "-------------------->> 7" << std::endl;
        A a;
        std::cout << a.constGet() << std::endl;
    }

    {
        std::cout << "-------------------->> 8" << std::endl;
        A a;
        std::cout << a.constGet2() << std::endl;
    }

    {
        std::cout << "-------------------->> 9" << std::endl;
        const A a;
        std::cout << a.constGet2() << std::endl;
    }

    {
        std::cout << "-------------------->> 10" << std::endl;
        A a;
        a.set(10);
        std::cout << a.get() << std::endl;
        const double &v1 = 11;
        a.set(v1);
        std::cout << a.get() << std::endl;
        double v2 = 12;
        a.set(v2);
        std::cout << a.get() << std::endl;
        const double v3 = 13;
        a.set(v3);
        std::cout << a.get() << std::endl;
    }

    return 0;
}
