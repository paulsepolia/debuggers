#include <iostream>
#include <memory>

void *operator new(size_t size)
{
    const auto memoryAddress = malloc(size);
    std::cout << "Allocated " << size << " bytes at address " << memoryAddress << std::endl;
    return memoryAddress;
}

// with initialization
void *operator new(size_t size, double val)
{
    const auto memoryAddress = malloc(size);
    *static_cast<double *>(memoryAddress) = val;
    std::cout << "Allocated " << size << " bytes at address " << memoryAddress << std::endl;
    return memoryAddress;
}

// common delete for the above new operators
void operator delete(void *memoryAddress, size_t size)
{
    std::cout << "Deleted " << size << " bytes at " << memoryAddress << std::endl;
    free(memoryAddress);
}

// new for array case
void *operator new[](size_t size)
{
    const auto memoryAddress = malloc(size);
    std::cout << "Allocated[] " << size << " bytes at address " << memoryAddress << std::endl;
    return memoryAddress;
}

// delete for array case
void operator delete[](void *memoryAddress)
{
    std::cout << "Deleted[] at address " << memoryAddress << std::endl;
    free(memoryAddress);
}

class A
{
public:

    double x1 = 1;
    double x2 = 2;
    double x3 = 3;
};

int main()
{
    {
        std::cout << "------------------>> 1" << std::endl;
        auto *x = new double(100);
        delete x;
    }

    {
        std::cout << "------------------>> 2" << std::endl;
        auto *x = new double[10];
        delete[] x;
    }

    {
        std::cout << "------------------>> 3" << std::endl;
        auto *x = new(10) double;
        std::cout << *x << std::endl;
        delete x;
    }

    {
        std::cout << "------------------>> 4" << std::endl;
        auto *x = new double[10];
        delete[] x;
    }

    {
        std::cout << "------------------>> 5" << std::endl;
        A *p = new A;
        delete p;
    }

    {
        std::cout << "------------------>> 6" << std::endl;
        A *p = new A[10];
        delete[] p;
    }
}