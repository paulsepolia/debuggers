#include <iostream>
#include <random>

struct foo
{
    char a[12];
};

int main()
{
    const size_t DIM1 = 1000;

    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_int_distribution<std::mt19937::result_type> dist1_10(1, 6);

    foo a1{};

    const size_t DIM2 = dist1_10(rng) * DIM1;
    const size_t FACT = 100;

    size_t i = 0;
    size_t j = 0;
    double sum_loc = 0;

    for (i = 0; i < DIM2; i++)
    {
        a1.a[i] = i;

        auto *p1 = new double[DIM2 * FACT];

        for (j = 0; j < DIM2 * FACT; j++)
        {
            p1[j] = j;
            sum_loc += p1[j];
        }

        delete[] p1;
    }

    std::cout << "-->> 0 -->> " << sum_loc << std::endl;
    std::cout << "-->> 1 -->> " << int(a1.a[0]) << std::endl;
    std::cout << "-->> 2 -->> " << int(a1.a[1]) << std::endl;
}