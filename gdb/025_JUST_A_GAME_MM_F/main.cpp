#include <vector>
#include <iostream>

class A
{
public:

    virtual ~A() = default;

    virtual void f1()
    {
        x1 = 11;
        std::cout << x1 << std::endl;
    }

    virtual void f2()
    {
        x2 = 22;
        std::cout << x2 << std::endl;
    }

    double x1 = 1;
    double x2 = 2;
};

class B final : public A
{
public:

    ~B() final = default;

private:

    void f1() final
    {
        x1 = 33;
        std::cout << x1 << std::endl;
    }

    void f2() final
    {
        x2 = 44;
        std::cout << x2 << std::endl;
    }

private:

    double x1 = 3;
    double x2 = 4;
};

int main()
{
    std::cout << "--------------------------------->> 1" << std::endl;

    A * p1 = new B;

    p1->f1();
    p1->f2();
    std::cout << p1->x1 << std::endl;
    std::cout << p1->x2 << std::endl;

    std::cout << "--------------------------------->> 2" << std::endl;

    A *p2 = new A;

    std::cout << p2->x1 << std::endl;
    std::cout << p2->x2 << std::endl;
    p2->f1();
    p2->f2();
    std::cout << p2->x1 << std::endl;
    std::cout << p2->x2 << std::endl;

    std::cout << "--------------------------------->> 3" << std::endl;

    B *p3 = new B;

    std::cout << ((A*)p3)->x1 << std::endl;
    std::cout << ((A*)p3)->x2 << std::endl;
    ((A*)p3)->f1();
    ((A*)p3)->f2();
    std::cout << ((A*)p3)->x1 << std::endl;
    std::cout << ((A*)p3)->x2 << std::endl;
}
