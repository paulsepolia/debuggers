#include <iostream>

class Singleton1
{
public:

    Singleton1(const Singleton1 &) = delete;

    Singleton1 &operator=(const Singleton1 &) = delete;

    Singleton1(Singleton1 &&) = delete;

    Singleton1 &operator=(Singleton1 &&) = delete;

    static const Singleton1 &GetInstance()
    {
        static Singleton1 anInstance;
        return anInstance;
    }

    double x = 123.456;

private:

    Singleton1() = default;
};

class Singleton2
{
public:

    Singleton2(const Singleton2 &) = delete;

    Singleton2 &operator=(const Singleton2 &) = delete;

    Singleton2(Singleton2 &&) = delete;

    Singleton2 &operator=(Singleton2 &&) = delete;

    static const Singleton2 &GetInstance()
    {
        return anInstance;
    }

    double x = 123.456;

private:

    static Singleton2 anInstance;

    Singleton2() = default;
};

Singleton2 Singleton2::anInstance = Singleton2();

int main()
{
    {
        std::cout << "-------------------------------------->> 1" << std::endl;
        const Singleton1 &p1 = Singleton1::GetInstance();
        std::cout << p1.x << std::endl;

        (const_cast<Singleton1 &>(p1)).x = 234.567;
        std::cout << p1.x << std::endl;

        const Singleton1 &p2 = Singleton1::GetInstance();

        std::cout << &p1.x << std::endl;
        std::cout << &p2.x << std::endl;

    }

    {
        std::cout << "-------------------------------------->> 2" << std::endl;
        const Singleton2 &p1 = Singleton2::GetInstance();
        std::cout << p1.x << std::endl;

        (const_cast<Singleton2 &>(p1)).x = 234.567;
        std::cout << p1.x << std::endl;

        const Singleton2 &p2 = Singleton2::GetInstance();

        std::cout << &p1.x << std::endl;
        std::cout << &p2.x << std::endl;
    }
}