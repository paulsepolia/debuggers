#include <iostream>
#include <vector>

int main()
{
    const size_t DO_MAX = 1'000'000'000UL;
    double *p1 = nullptr;
    const double d = 100;
    p1 = (double *) (&d);

    for (size_t i = 0; i < DO_MAX; i++)
    {
        std::cout << "------------------>> i = " << i << std::endl;
        std::cout << *p1 << std::endl;
        p1++;
    }
}
