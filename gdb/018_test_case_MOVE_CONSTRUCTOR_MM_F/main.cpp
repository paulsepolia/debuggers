#include <iostream>

class A
{
public:

    A() : data{nullptr}, dim{0}
    {}

    A(const A &obj)
    {
        if (obj.data != nullptr)
        {
            data = new double[obj.dim];
            std::copy(obj.data, obj.data + obj.dim, data);
            dim = obj.dim;
        }
        else
        {
            data = nullptr;
            dim = obj.dim;
        }
    }

    A(A &&obj) noexcept
    {
        delete [] data;
        dim = 0;

        data = obj.data;
        dim = obj.dim;

        obj.data = nullptr;
        obj.dim = 0;
    }

    A &operator=(const A &obj)
    {
        if (this != &obj)
        {
            delete[] data;
            data = new double[obj.dim];
            std::copy(obj.data, obj.data + obj.dim, data);
            dim = obj.dim;
        }
        return *this;
    }

    A &operator=(A &&obj) noexcept
    {
        if (this != &obj)
        {
            delete[] data;
            data = obj.data;
            dim = obj.dim;

            obj.data = nullptr;
            obj.dim = 0;
        }

        return *this;
    }

    virtual ~A()
    {
        delete[] data;
        data = nullptr;
        dim = 0;
    }

    double *data = nullptr;
    size_t dim = 0;
};

int main()
{
    constexpr size_t DIM = 1000;

    A a1;
    a1.dim = DIM;
    a1.data = new double[a1.dim];

    for (size_t i = 0; i < a1.dim; i++)
    {
        a1.data[i] = (double) i;
    }

    std::cout << a1.data[0] << std::endl;
    std::cout << a1.data[1] << std::endl;
    std::cout << a1.data[2] << std::endl;
    std::cout << a1.data[3] << std::endl;

    A a2(a1);

    std::cout << a2.data[0] << std::endl;
    std::cout << a2.data[1] << std::endl;
    std::cout << a2.data[2] << std::endl;
    std::cout << a2.data[3] << std::endl;

    a2 = a1;

    std::cout << a2.data[0] << std::endl;
    std::cout << a2.data[1] << std::endl;
    std::cout << a2.data[2] << std::endl;
    std::cout << a2.data[3] << std::endl;

    a1.dim = DIM - 1;
    delete[] a1.data;
    a1.data = new double[a1.dim];

    a1.data[0] = -1;
    a1.data[1] = -2;
    a1.data[2] = -3;

    std::cout << a1.data[0] << std::endl;
    std::cout << a1.data[1] << std::endl;
    std::cout << a1.data[2] << std::endl;

    a2 = a1;

    std::cout << a2.data[0] << std::endl;
    std::cout << a2.data[1] << std::endl;
    std::cout << a2.data[2] << std::endl;
    std::cout << a2.dim << std::endl;

    A a3(std::move(a2));

    std::cout << "-->> a2 after move" << std::endl;

    std::cout << a2.data << std::endl;
    std::cout << a2.dim << std::endl;

    std::cout << "-->> after a2 has been moved to a3" << std::endl;

    std::cout << a3.data[0] << std::endl;
    std::cout << a3.data[1] << std::endl;
    std::cout << a3.data[2] << std::endl;
    std::cout << a3.dim << std::endl;

    a2 = std::move(a3);

    std::cout << "-->> a3 after move" << std::endl;

    std::cout << a3.data << std::endl;
    std::cout << a3.dim << std::endl;

    std::cout << "-->> after a2 has been moved to a3" << std::endl;

    std::cout << a2.data[0] << std::endl;
    std::cout << a2.data[1] << std::endl;
    std::cout << a2.data[2] << std::endl;
    std::cout << a2.dim << std::endl;
}