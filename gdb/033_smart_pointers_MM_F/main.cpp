#include <iostream>

template<typename T>
class MyPtr
{
public:

    explicit MyPtr(T *p)
    {
        data = p;
    }

    explicit MyPtr(const T *p)
    {
        data = p;
    }

    virtual ~MyPtr()
    {
        delete data;
    }

    T operator*() const
    {
        return *data;
    }

    const T *data = nullptr;
};

int main()
{
    auto *p1 = new int(1);
    MyPtr<int> myPtr1 = MyPtr<int>(p1);
    std::cout << *myPtr1 << std::endl;

    const auto *p2 = new int(2);
    MyPtr<int> myPtr2 = MyPtr<int>(p2);
    std::cout << *myPtr2 << std::endl;

    const auto *p3 = new double(3);
    MyPtr<double> myPtr3 = MyPtr<double>(p3);
    std::cout << *myPtr3 << std::endl;

    double x4 = 100;
    const double * p4 = &x4;
    MyPtr<double> myPtr4 = MyPtr<double>(p4);
    std::cout << *myPtr4 << std::endl;

    return 0;
}