#include <vector>
#include <iostream>
#include <algorithm>

bool f(double a, double b)
{
    return a > b;
}

bool (*fp)(double, double);

class fctor
{
public:

    bool operator()(double a, double b)
    {
        return a > b;
    }
};

int main()
{
    {
        std::cout << std::endl;
        std::cout << "-------------------------->> 1" << std::endl;
        std::vector<double> v = {1, 2, 3, 4, 5, 6};
        std::sort(v.begin(), v.end(), [](double a, double b)
        {
            return a > b;
        });
        for (const auto &el: v)
        {
            std::cout << el;
        }
    }

    {
        std::cout << std::endl;
        std::cout << "-------------------------->> 2" << std::endl;
        std::vector<double> v = {1, 2, 3, 4, 5, 6};
        std::sort(v.begin(), v.end(), f);
        for (const auto &el: v)
        {
            std::cout << el;
        }
    }

    {
        std::cout << std::endl;
        std::cout << "-------------------------->> 3" << std::endl;
        std::vector<double> v = {1, 2, 3, 4, 5, 6};
        std::sort(v.begin(), v.end(), [](double a, double b)
        {
            return a > b;
        });
        for (const auto &el: v)
        {
            std::cout << el;
        }
    }

    {
        std::cout << std::endl;
        std::cout << "-------------------------->> 4" << std::endl;
        std::vector<double> v = {1, 2, 3, 4, 5, 6};
        fp = f;
        std::sort(v.begin(), v.end(), fp);
        for (const auto &el: v)
        {
            std::cout << el;
        }
    }

    {
        std::cout << std::endl;
        std::cout << "-------------------------->> 6" << std::endl;
        std::vector<double> v = {1, 2, 3, 4, 5, 6};
        std::sort(v.begin(), v.end(), fctor());
        for (const auto &el: v)
        {
            std::cout << el;
        }
    }

    std::cout << std::endl;
}
