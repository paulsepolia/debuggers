#include <iostream>
#include <vector>
#include <chrono>
#include <iomanip>

class A
{
public:
    virtual void f() const
    {
        std::cout << "I am: A" << std::endl;
    }

    virtual ~A() = default;
};

class B1 final : public A
{
private:
    void f() const final
    {
        std::cout << "I am: B1" << std::endl;
    }

public:
    ~B1() final = default;
};

class B2 final : public A
{
private:
    void f() const final
    {
        std::cout << "I am: B2" << std::endl;
    }

public:
    ~B2() final = default;
};

class B3 final : public A
{
private:
    void f() const final
    {
        std::cout << "I am: B3" << std::endl;
    }

public:
    ~B3() final = default;
};

int main()
{
    {
        std::cout << "--------------->> 1" << std::endl;
        B1 b1;
        B2 b2;
        B3 b3;

        std::vector<A> v;

        v.push_back(b1);
        v.push_back(b2);
        v.push_back(b3);

        v[0].f();
        v[1].f();
        v[2].f();
    }

    {
        std::cout << "--------------->> 2" << std::endl;
        std::vector<A *> v;
        A *b1 = new B1;
        A *b2 = new B2;
        A *b3 = new B3;

        v.push_back(b1);
        v.push_back(b2);
        v.push_back(b3);

        v[0]->f();
        v[1]->f();
        v[2]->f();

        delete v[0];
        delete v[1];
        delete v[2];
    }

    {
        std::cout << "--------------->> 2" << std::endl;
        std::vector<A *> v;
        B1 b1;
        B2 b2;
        B3 b3;

        v.push_back(&b1);
        v.push_back(&b2);
        v.push_back(&b3);

        v[0]->f();
        v[1]->f();
        v[2]->f();
    }
}