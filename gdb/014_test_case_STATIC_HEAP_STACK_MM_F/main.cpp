#include <vector>
#include <iostream>

void f1()
{
    static double * x;
    x = new double(100); // WE DO HAVE MEMORY LEAK HERE
    std::cout << *x << std::endl;
}

int main()
{
    std::cout << "----------------------->> 1" << std::endl;

    static auto* x1 = new double(1); // NO MEMORY LEAK HERE
    std::cout << x1 << std::endl;
    std::cout << *x1 << std::endl;
    std::cout << &x1 << std::endl;
    delete x1;
    *x1 = 100; // IF I DO THIS I DESTROY ANY FURTHER HEAP ALLOCATIONS

    std::cout << "----------------------->> 2" << std::endl;

    double x2 = 2;
    std::cout << x2 << std::endl;
    std::cout << &x2 << std::endl;

    std::cout << "----------------------->> 3" << std::endl;

    const size_t DIM = 10000;
    auto* x3 = new double[DIM]; // WE DO HAVE MEMORY LEAK HERE
    x3[0] = 0;
    x3[DIM-1] = 3;
    std::cout << x3 << std::endl;
    std::cout << *x3 << std::endl;
    std::cout << &x3 << std::endl;
    std::cout << &x3[DIM-1] << std::endl;

    std::cout << "----------------------->> 4" << std::endl;

    f1();
    f1();
    f1();
    f1();
}
