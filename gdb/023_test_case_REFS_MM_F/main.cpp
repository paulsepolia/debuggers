#include <iostream>
#include <vector>

int main()
{
    std::vector<double> v1{1, 2, 3};
    const std::vector<double> &v2 = v1;

    std::cout << v1[0] << ", " << v2[0] << std::endl;
    std::cout << v1[1] << ", " << v2[1] << std::endl;
    std::cout << v1[2] << ", " << v2[2] << std::endl;

    const_cast<double &>(v2[0]) = 11;
    const_cast<double &>(v2[1]) = 12;
    const_cast<double &>(v2[2]) = 13;

    std::cout << v1[0] << ", " << v2[0] << std::endl;
    std::cout << v1[1] << ", " << v2[1] << std::endl;
    std::cout << v1[2] << ", " << v2[2] << std::endl;
}