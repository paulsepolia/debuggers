#include <iostream>

int main()
{
    int *x1 = nullptr;
    auto *x2 = (double *) (&x1);

    *x2 = 100;

    std::cout << x1 << std::endl;
    std::cout << x2 << std::endl;
    std::cout << *x2 << std::endl;
    std::cout << *x1 << std::endl;

    //NOT ALLOWED
    //auto *x3 = static_cast<double *>(&x1);


    return 0;
}
