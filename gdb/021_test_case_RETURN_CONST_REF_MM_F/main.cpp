#include <iostream>
#include <vector>
#include <chrono>
#include <memory>

const double &f1()
{
    auto *x1 = new double(100);
    return *x1;
}


int main()
{
    const double &r = f1();

    std::cout << r << std::endl;

    delete &r;

    std::cout << r << std::endl;
}