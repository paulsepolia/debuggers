#include <iostream>
#include <memory>

class Resource
{
public:
    std::shared_ptr<Resource> m_ptr;

    Resource()
    {
        std::cout << "Resource acquired" << std::endl;
    }

    ~Resource()
    {
        std::cout << "Resource destroyed" << std::endl;
    }
};

int main()
{
    const auto ptr1 = std::make_shared<Resource>();
    const auto ptr2 = std::make_shared<Resource>();
    ptr1->m_ptr = ptr2;
    ptr2->m_ptr = ptr1;
    
    return 0;
}