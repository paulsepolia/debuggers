#include <iostream>

template<typename T>
class Vector
{
public:

    Vector() : i_size(0), T_data(nullptr)
    {}

    Vector(size_t size, T value) : i_size(size), T_data(nullptr)
    {
        T_data = new T[i_size];
        for (size_t i = 0; i < i_size; i++)
        {
            T_data[i] = value;
        }
    }

    Vector(const Vector<T> &other) : i_size(other.i_size), T_data(nullptr)
    {
        T_data = new T[i_size];
        for (size_t i = 0; i < i_size; i++)
        {
            T_data[i] = other.T_data[i];
        }
    }

    Vector(Vector<T> &&other) noexcept
    {
        i_size = other.i_size;
        T_data = other.T_data;
        other.i_size = 0;
        other.T_data = nullptr;
    }

    Vector<T> &operator=(const Vector<T> &other)
    {
        if (this != &other)
        {
            delete[] T_data;
            i_size = other.i_size;
            T_data = new T[i_size];
            for (size_t i = 0; i < i_size; i++)
            {
                T_data[i] = other.T_data[i];
            }
        }
        return *this;
    }

    Vector<T> &operator=(Vector<T> &&other) noexcept
    {
        if (this != &other)
        {
            i_size = other.i_size;
            T_data = other.T_data;
            other.i_size = 0;
            other.T_data = nullptr;
        }
        return *this;
    }

    [[nodiscard]] size_t size() const
    {
        return i_size;
    }

    virtual ~Vector()
    {
        delete[] T_data;
        T_data = nullptr;
        i_size = 0;
    }

    T &operator[](size_t i)
    {
        return T_data[i];
    }

    T at(size_t i)
    {
        return T_data[i];
    }

private:

    size_t i_size;
    T *T_data;
};

int main()
{
    const size_t DIM = 10'000UL;

    {
        std::cout << "---------------------------->> 1" << std::endl;

        Vector<double> v1(DIM, 123.456);
        std::cout << v1[0] << std::endl;
        std::cout << v1[1] << std::endl;
        std::cout << v1[2] << std::endl;
        v1[0] = 0;
        v1[1] = 1;
        v1[2] = 2;
        std::cout << v1[0] << std::endl;
        std::cout << v1[1] << std::endl;
        std::cout << v1[2] << std::endl;
        std::cout << v1.at(0) << std::endl;
        std::cout << v1.at(1) << std::endl;
        std::cout << v1.at(2) << std::endl;
    }

    {
        std::cout << "---------------------------->> 2" << std::endl;

        Vector<double> v1(DIM, 123.456);
        Vector<double> v2(v1);

        std::cout << v1[0] << std::endl;
        std::cout << v2[0] << std::endl;

        std::cout << v1[1] << std::endl;
        std::cout << v2[1] << std::endl;

        v2[2] = 10;
        std::cout << v1[2] << std::endl;
        std::cout << v2[2] << std::endl;
    }

    {
        std::cout << "---------------------------->> 3" << std::endl;

        Vector<double> v1(DIM, 123.456);
        Vector<double> v2(std::move(v1));

        std::cout << v2[0] << std::endl;
        std::cout << v2[1] << std::endl;
        std::cout << v2[2] << std::endl;

        v1 = v2;
    }

    {
        std::cout << "---------------------------->> 3" << std::endl;

        Vector<double> v1(DIM, 123.456);
        std::cout << &v1 << std::endl;
        std::cout << &v1[0] << std::endl;

        Vector<double> v2(std::move(v1));
        std::cout << &v2 << std::endl;
        std::cout << &v2[0] << std::endl;

        v1 = std::move(v2);

        std::cout << &v1 << std::endl;
        std::cout << &v1[0] << std::endl;
    }


    return 0;
}
