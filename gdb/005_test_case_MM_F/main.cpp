#include <iostream>
#include <vector>

void f1(double x)
{
    std::cout << x << std::endl;
}

void f2(double x)
{
    std::cout << x << std::endl;
    f1(x);
}

void f3(double x)
{
    std::cout << x << std::endl;
    f2(x);
}

int main()
{
    std::cout << "----->> 1" << std::endl;
    void (*f)(double);
    f = std::move(f1);
    f(101);
    f(102);
    f1(103);
    f1(104);

    std::cout << "----->> 2" << std::endl;

    std::vector<void (*)(double)> vfp;

    vfp.push_back(f1);
    vfp.push_back(f2);
    vfp.push_back(f3);

    double x = 1;
    for (const auto &el: vfp)
    {
        el(x++);
    }

    std::cout << "----->> 3" << std::endl;
    void *p1;
    p1 = new double(103);

    std::cout << p1 << std::endl;
    std::cout << *(double *) (p1) << std::endl;
    delete (double *) p1;
    p1 = nullptr;

    std::cout << "----->> 4" << std::endl;
    void *p2;
    p2 = new int(104);
    std::cout << p2 << std::endl;
    std::cout << *(int *) (p2) << std::endl;
    delete (int *) p2;
    p2 = nullptr;

    std::cout << "----->> 5" << std::endl;
    double *p3;
    p3 = new double(105);
    std::cout << p3 << std::endl;
    std::cout << *(double *) (p3) << std::endl;

    size_t counter = 0;
    while(true)
    {
        p3++;
        std::cout << "counter = " << ++counter << " -->> " << *(double *) (p3) << std::endl;
    }

    delete (double *) p3;
    p3 = nullptr;
}
