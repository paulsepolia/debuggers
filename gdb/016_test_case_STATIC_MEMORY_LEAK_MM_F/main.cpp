#include <iostream>

int main()
{
    static auto* x1 = new double(1); // NO MEMORY LEAK HERE
    auto* x2 = new double(2); // MEMORY LEAK ONLY HERE

    std::cout << *x1 << std::endl;
    std::cout << *x2 << std::endl;
}
