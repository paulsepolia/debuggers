#include <iostream>
#include <vector>

int main()
{
    std::cout << "----->> 1" << std::endl;
    std::vector<double> v1;
    v1.push_back(1);
    v1.push_back(2);
    v1.push_back(3);
    v1.push_back(4);
    v1.push_back(5);

    double *p1;
    p1 = &v1[0];
    std::cout << *p1 << std::endl;
    p1++;
    std::cout << *p1 << std::endl;
    p1++;
    std::cout << *p1 << std::endl;
    p1++;
    std::cout << *p1 << std::endl;

    std::cout << "----->> 2" << std::endl;
    std::vector<double> v2;
    v2.push_back(1);
    v2.push_back(2);
    v2.push_back(3);
    v2.push_back(4);
    v2.push_back(5);

    std::cout << (long int) (&v2) << std::endl;
    std::cout << (long int) (&v2[0]) << std::endl;
    std::cout << (long int) (&v2) - (long int) (&v2[0]) << std::endl;
    std::cout << (long int) (&v2[1]) - (long int) (&v2[0]) << std::endl;
    std::cout << (long int) (&v2[2]) - (long int) (&v2[1]) << std::endl;
    std::cout << sizeof(double) << std::endl;

    std::cout << "----->> 3" << std::endl;
    double x1 = 101;
    double x2 = 102;
    double *p4 = nullptr;
    std::cout << "&x1 = " << &x1 << std::endl;
    std::cout << "&x2 = " << &x2 << std::endl;
    std::cout << "(&x1 - &x2)" << (&x1 - &x2) << std::endl;
    p4 = &x1;

    std::cout << "*p4 = " << *p4 << std::endl;
    std::cout << "p4 = " << p4 << std::endl;
    p4--;
    std::cout << "*p4 = " << *p4 << std::endl;
    std::cout << "p4 = " << p4 << std::endl;

    std::cout << "----->> 4" << std::endl;
    auto * p6 = new double(201);
    std::cout << p6 << std::endl;
    std::cout << *p6 << std::endl;
    p6++;
    std::cout << p6 << std::endl;
    std::cout << *p6 << std::endl;
    delete p6;
    p6 = nullptr;

}
