//============//
// shared_ptr //
//============//

#include <iostream>
#include <memory>
#include <vector>
#include <cmath>

using std::endl;
using std::cout;
using std::cin;
using std::shared_ptr;
using std::pow;

// the main function

int main()
{
    const int TRIALS = 100;

    for (int ik = 0; ik != TRIALS; ik++) {
        cout << "-------------------------------------------->> " << ik << endl;

        const unsigned long int DIM = static_cast<unsigned long int>(pow(10.0, 7.5));

        shared_ptr<double> * sp = new shared_ptr<double> [DIM];  // empty

        for (unsigned long int i = 0; i != DIM; i++) {
            sp[i].reset(new double (i));
        }

        cout << " --> *sp[0] = " << *sp[0] << endl;
        cout << " --> *sp[1] = " << *sp[1] << endl;
        cout << " --> *sp[2] = " << *sp[2] << endl;

        delete [] sp;
    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

// end
