
//=========================================//
// Unique pointer with non-default deleter //
//=========================================//

#include <iostream>
#include <memory>

using std::endl;
using std::cout;
using std::cin;
using std::unique_ptr;

// struct Foo

struct Foo {

    // construnctor

    Foo()
    {
        cout << "Foo..." << endl;
    }

    // destructor

    virtual ~Foo()
    {
        cout << "~Foo..." << endl;
    }
};

// struct D

struct D {
    void operator() (Foo * p)
    {
        cout << "Calling delete for Foo object..." << endl;
        delete p;
    }
};

// the main function

int main()
{
    const int I_MAX = 10;

    for (int i = 0; i != I_MAX; i++) {
        cout << "------------------------------------------------>> " << i << endl;

        cout << " --> Creating new Foo" << endl;
        unique_ptr<Foo, D> up(new Foo(), D());  // up owns the Foo pointer (deleter D)

        cout << " --> Replace owned Foo with a new Foo" << endl;
        up.reset(new Foo());  // calls deleter for the old one

        cout << " --> Release and delete the owned Foo" << endl;
        up.reset(nullptr);
    }

    return 0;
}

// END
