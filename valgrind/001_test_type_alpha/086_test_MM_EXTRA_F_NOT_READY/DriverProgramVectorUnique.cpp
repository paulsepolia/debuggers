
//=============================//
// UNIQUE POINTER BASED VECTOR //
//=============================//

#include <iostream>
#include <memory>
#include <cmath>
#include <iomanip>

using std::endl;
using std::cout;
using std::cin;
using std::unique_ptr;
using std::pow;
using std::fixed;
using std::setprecision;

#include "VectorUnique_LEAK.h"

//#include "VectorUnique.h"

#define OUT_A
//#undef OUT_A

//===================//
// the main function //
//===================//

int main()
{
    // local parameters

    const unsigned long int DIM = static_cast<unsigned long int>(pow(10.0, 5.0));
    const unsigned long int TRIALS = 2*static_cast<unsigned long int>(pow(10.0, 5.0));

    // adjust output

    cout << fixed;
    cout << setprecision(4);

    // main test loop

    for (unsigned long int i = 0; i != TRIALS; i++) {

//          cout << "----------------------------------------------->> " << i << endl;
#ifdef OUT_A
        cout << "----------------------------------------------->> " << i << endl;
        cout << "--> allocate vectors" << endl;
#endif

        VectorUnique<double> a1;
        VectorUnique<double> a2;

        a1.allocate(DIM);
        a2.allocate(DIM);

#ifdef OUT_A
        cout << "--> build vectors" << endl;
#endif

        for (unsigned long int j = 0; j != DIM; j++) {
            a1.set_element(j, static_cast<double>(j));
            a2.set_element(j, static_cast<double>(0.1+j));
        }

#ifdef OUT_A
        cout << "--> get back some elements" << endl;
        cout << "--> a1.get_element(0) = " << a1.get_element(0) << endl;
        cout << "--> a1.get_element(1) = " << a1.get_element(1) << endl;
        cout << "--> a1.get_element(2) = " << a1.get_element(2) << endl;
        cout << "--> a2.get_element(0) = " << a2.get_element(0) << endl;
        cout << "--> a2.get_element(1) = " << a2.get_element(1) << endl;
        cout << "--> a2.get_element(2) = " << a2.get_element(2) << endl;
        cout << "--> set equal the two vectors" << endl;
#endif

        a1 = 11.1;
        a2 = 22.2;

#ifdef OUT_A
        cout << "--> a1.get_element(0) = " << a1.get_element(0) << endl;
        cout << "--> a1.get_element(1) = " << a1.get_element(1) << endl;
        cout << "--> a1.get_element(2) = " << a1.get_element(2) << endl;
        cout << "--> a2.get_element(0) = " << a2.get_element(0) << endl;
        cout << "--> a2.get_element(1) = " << a2.get_element(1) << endl;
        cout << "--> a2.get_element(2) = " << a2.get_element(2) << endl;
#endif

        VectorUnique<double> a3(a1);

#ifdef OUT_A
        cout << "--> add the two vectors" << endl;
#endif

        a3 = a1 + a2;

#ifdef OUT_A
        cout << "--> a3.get_element(0) = " << a3.get_element(0) << endl;
        cout << "--> a3.get_element(1) = " << a3.get_element(1) << endl;
        cout << "--> a3.get_element(2) = " << a3.get_element(2) << endl;
        cout << "--> subtract the two vectors" << endl;
#endif

        a3 = a1 - a2;

#ifdef OUT_A
        cout << "--> a3.get_element(0) = " << a3.get_element(0) << endl;
        cout << "--> a3.get_element(1) = " << a3.get_element(1) << endl;
        cout << "--> a3.get_element(2) = " << a3.get_element(2) << endl;
        cout << "--> divide the two vectors" << endl;
#endif

        a3 = a1 / a2;

#ifdef OUT_A
        cout << "--> a3.get_element(0) = " << a3.get_element(0) << endl;
        cout << "--> a3.get_element(1) = " << a3.get_element(1) << endl;
        cout << "--> a3.get_element(2) = " << a3.get_element(2) << endl;
        cout << "--> multiply the two vectors" << endl;
#endif

        a3 = a1 * a2;

#ifdef OUT_A
        cout << "--> a3.get_element(0) = " << a3.get_element(0) << endl;
        cout << "--> a3.get_element(1) = " << a3.get_element(1) << endl;
        cout << "--> a3.get_element(2) = " << a3.get_element(2) << endl;
#endif

        a1 = 11.0000;
        a2 = 22.0000;
        a3 = 33.0000;

#ifdef OUT_A
        cout << "--> a1.get_element(10) = " << a1.get_element(10) << endl;
        cout << "--> a2.get_element(11) = " << a2.get_element(11) << endl;
        cout << "--> a3.get_element(12) = " << a3.get_element(12) << endl;
#endif

        ++a1;
        ++a2;
        ++a3;

#ifdef OUT_A
        cout << "--> a1.get_element(10) = " << a1.get_element(10) << endl;
        cout << "--> a2.get_element(11) = " << a2.get_element(11) << endl;
        cout << "--> a3.get_element(12) = " << a3.get_element(12) << endl;
#endif

        a1++;
        a2++;
        a3++;

#ifdef OUT_A
        cout << "--> a1.get_element(10) = " << a1.get_element(10) << endl;
        cout << "--> a2.get_element(11) = " << a2.get_element(11) << endl;
        cout << "--> a3.get_element(12) = " << a3.get_element(12) << endl;
#endif

        a1--;
        a2--;
        a3--;

#ifdef OUT_A
        cout << "--> a1.get_element(10) = " << a1.get_element(10) << endl;
        cout << "--> a2.get_element(11) = " << a2.get_element(11) << endl;
        cout << "--> a3.get_element(12) = " << a3.get_element(12) << endl;
#endif

        --a1;
        --a2;
        --a3;

#ifdef OUT_A
        cout << "--> a1.get_element(10) = " << a1.get_element(10) << endl;
        cout << "--> a2.get_element(11) = " << a2.get_element(11) << endl;
        cout << "--> a3.get_element(12) = " << a3.get_element(12) << endl;
#endif

        a1 += a2;
        a2 += a3;
        a3 += a1;

#ifdef OUT_A
        cout << "--> a1.get_element(10) = " << a1.get_element(10) << endl;
        cout << "--> a2.get_element(11) = " << a2.get_element(11) << endl;
        cout << "--> a3.get_element(12) = " << a3.get_element(12) << endl;
#endif

        a1 -= a2;
        a2 -= a3;
        a3 -= a1;

#ifdef OUT_A
        cout << "--> a1.get_element(10) = " << a1.get_element(10) << endl;
        cout << "--> a2.get_element(11) = " << a2.get_element(11) << endl;
        cout << "--> a3.get_element(12) = " << a3.get_element(12) << endl;
#endif

        a1 *= a2;
        a2 *= a3;
        a3 *= a1;

#ifdef OUT_A
        cout << "--> a1.get_element(10) = " << a1.get_element(10) << endl;
        cout << "--> a2.get_element(11) = " << a2.get_element(11) << endl;
        cout << "--> a3.get_element(12) = " << a3.get_element(12) << endl;
#endif

        a1 /= a2;
        a2 /= a3;
        a3 /= a1;

#ifdef OUT_A
        cout << "--> a1.get_element(10) = " << a1.get_element(10) << endl;
        cout << "--> a2.get_element(11) = " << a2.get_element(11) << endl;
        cout << "--> a3.get_element(12) = " << a3.get_element(12) << endl;
#endif

        a1 = 10.0;
        a2 = 20.0;
        a3 = 30.0;

        a1 = a1 + 10.0;
        a2 = a2 * 20.0;
        a3 = a3 / 30.0;

#ifdef OUT_A
        cout << "--> a1.get_element(10) = " << a1.get_element(10) << endl;
        cout << "--> a2.get_element(11) = " << a2.get_element(11) << endl;
        cout << "--> a3.get_element(12) = " << a3.get_element(12) << endl;
#endif

    }

    return 0;
}

//=====//
// end //
//=====//
