
//================//
// memset example //
//================//

#include <cstring>
#include <iostream>

using std::endl;
using std::cout;
using std::cin;

// function: funA

void funA(unsigned char * &pA)
{
    const int DIM = 100000000;

    pA = new unsigned char [DIM];

    memset(pA, 'a', sizeof(unsigned char)*DIM);
}

// the main function

int main ()
{
    const int TRIALS = 10;

    for (int i = 0; i != TRIALS; i++) {
        unsigned char * pA;

        funA(pA);

        delete [] pA;

        pA = 0;
    }

    unsigned char * pA;

    funA(pA);

    cout << " --> pA[0] = " << pA[0] << endl;
    cout << " --> pA[1] = " << pA[1] << endl;
    cout << " --> pA[2] = " << pA[2] << endl;

    return 0;
}

// end
