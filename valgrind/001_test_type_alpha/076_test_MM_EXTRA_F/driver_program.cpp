
//=============================//
// SHARED POINTER BASED VECTOR //
//=============================//

#include <iostream>
#include <memory>
#include <cmath>
#include <iomanip>

using std::endl;
using std::cout;
using std::cin;
using std::shared_ptr;
using std::make_shared;
using std::pow;
using std::fixed;
using std::setprecision;

#define OUT_A
//#undef OUT_A

// class A

template<class T>
class Vector {
public:

    // default constructor

    Vector() : m_dim(0), ssp(0) {}

    // constructor

    Vector(const unsigned long int & dim) : m_dim(dim)
    {
        this->allocate(m_dim);
    }

    // copy constructor

    Vector(const Vector & other) : m_dim(other.m_dim)
    {
        this->ssp = make_shared<shared_ptr<T>>(*other.ssp);
    }

    // allocate

    void allocate(const unsigned long int & DIM)
    {
        this->m_dim = DIM;
        this->ssp.reset(new shared_ptr<T> [DIM], [](shared_ptr<T> * p) {
            delete [] p;
        });
    }

    // set element

    void set_element(const unsigned long int & index, const T & elem)
    {
        this->ssp.get()[index].reset(new T (elem), [](T * p) {
            delete p;
        });
    }

    // get element

    T get_element(const unsigned long int & index)
    {
        T elem = *this->ssp.get()[index];
        return elem;
    }

    // assignment operator

    Vector & operator=(const Vector & other)
    {
        for (unsigned long int i = 0; i != other.m_dim; i++) {
            T elem = *other.ssp.get()[i];
            this->ssp.get()[i].reset(new T (elem), [](T * p) {
                delete p;
            });
        }

        return *this;
    }

    // + operator

    Vector operator+(const Vector & other)
    {
        Vector tmp(other.m_dim);

        for (unsigned long int i = 0; i != other.m_dim; i++) {
            T elem1 = *other.ssp.get()[i];
            T elem2 = *this->ssp.get()[i];
            T elem_tmp = elem2 + elem1;
            tmp.ssp.get()[i].reset(new T (elem_tmp), [](T * p) {
                delete p;
            });
        }

        return tmp;
    }

    // - operator

    Vector operator-(const Vector & other)
    {
        Vector tmp(other.m_dim);

        for (unsigned long int i = 0; i != other.m_dim; i++) {
            T elem1 = *other.ssp.get()[i];
            T elem2 = *this->ssp.get()[i];
            T elem_tmp = elem2 - elem1;
            tmp.ssp.get()[i].reset(new T (elem_tmp), [](T * p) {
                delete p;
            });
        }

        return tmp;
    }

    // * operator

    Vector operator*(const Vector & other)
    {
        Vector tmp(other.m_dim);

        for (unsigned long int i = 0; i != other.m_dim; i++) {
            T elem1 = *other.ssp.get()[i];
            T elem2 = *this->ssp.get()[i];
            T elem_tmp = elem2 * elem1;
            tmp.ssp.get()[i].reset(new T (elem_tmp), [](T * p) {
                delete p;
            });
        }

        return tmp;
    }

    // / operator

    Vector operator/(const Vector & other)
    {
        Vector tmp(other.m_dim);

        for (unsigned long int i = 0; i != other.m_dim; i++) {
            T elem1 = *other.ssp.get()[i];
            T elem2 = *this->ssp.get()[i];
            T elem_tmp = elem2 / elem1;
            tmp.ssp.get()[i].reset(new T (elem_tmp), [](T * p) {
                delete p;
            });
        }

        return tmp;
    }

    // destructor

    virtual ~Vector() {}

private:

    unsigned long int m_dim;
    shared_ptr<shared_ptr<T>> ssp;
};

// the main function

int main()
{
    // local parameters

    const unsigned long int DIM = static_cast<unsigned long int>(pow(10.0, 7.0));
    const unsigned long int TRIALS = static_cast<unsigned long int>(pow(10.0, 1.0));

    // adjust output

    cout << fixed;
    cout << setprecision(4);

    for (unsigned long int i = 0; i != TRIALS; i++) {

#ifdef OUT_A
        cout << "----------------------------------------------->> " << i << endl;
        cout << "--> allocate vectors" << endl;
#endif

        Vector<double> a1;
        Vector<double> a2;

        a1.allocate(DIM);
        a2.allocate(DIM);

#ifdef OUT_A
        cout << "--> build vectors" << endl;
#endif

        for (unsigned long int j = 0; j != DIM; j++) {
            a1.set_element(j, static_cast<double>(j));
            a2.set_element(j, static_cast<double>(0.1+j));
        }

#ifdef OUT_A
        cout << "--> get back some elements" << endl;
        cout << "--> a1.get_element(0) = " << a1.get_element(0) << endl;
        cout << "--> a1.get_element(1) = " << a1.get_element(1) << endl;
        cout << "--> a1.get_element(2) = " << a1.get_element(2) << endl;
        cout << "--> a2.get_element(0) = " << a2.get_element(0) << endl;
        cout << "--> a2.get_element(1) = " << a2.get_element(1) << endl;
        cout << "--> a2.get_element(2) = " << a2.get_element(2) << endl;
        cout << "--> set equal the two vectors" << endl;
#endif

        a1 = a2;

#ifdef OUT_A
        cout << "--> a1.get_element(0) = " << a1.get_element(0) << endl;
        cout << "--> a1.get_element(1) = " << a1.get_element(1) << endl;
        cout << "--> a1.get_element(2) = " << a1.get_element(2) << endl;
        cout << "--> a2.get_element(0) = " << a2.get_element(0) << endl;
        cout << "--> a2.get_element(1) = " << a2.get_element(1) << endl;
        cout << "--> a2.get_element(2) = " << a2.get_element(2) << endl;
#endif

        Vector<double> a3(DIM);

#ifdef OUT_A
        cout << "--> add the two vectors" << endl;
#endif

        a3 = a1 + a2;

#ifdef OUT_A
        cout << "--> a3.get_element(0) = " << a3.get_element(0) << endl;
        cout << "--> a3.get_element(1) = " << a3.get_element(1) << endl;
        cout << "--> a3.get_element(2) = " << a3.get_element(2) << endl;
        cout << "--> subtract the two vectors" << endl;
#endif

        a3 = a1 - a2;

#ifdef OUT_A
        cout << "--> a3.get_element(0) = " << a3.get_element(0) << endl;
        cout << "--> a3.get_element(1) = " << a3.get_element(1) << endl;
        cout << "--> a3.get_element(2) = " << a3.get_element(2) << endl;
        cout << "--> divide the two vectors" << endl;
#endif

        a3 = a1 / a2;

#ifdef OUT_A
        cout << "--> a3.get_element(0) = " << a3.get_element(0) << endl;
        cout << "--> a3.get_element(1) = " << a3.get_element(1) << endl;
        cout << "--> a3.get_element(2) = " << a3.get_element(2) << endl;
        cout << "--> multiply the two vectors" << endl;
#endif

        a3 = a1 * a2;

#ifdef OUT_A
        cout << "--> a3.get_element(0) = " << a3.get_element(0) << endl;
        cout << "--> a3.get_element(1) = " << a3.get_element(1) << endl;
        cout << "--> a3.get_element(2) = " << a3.get_element(2) << endl;
#endif
    }

    return 0;
}

// end
