
#include <iostream>
#include <cmath>

using std::endl;
using std::cout;
using std::cin;
using std::pow;

// --> functions declaration

// (a)

void memory_leak_one();

// (b)

void memory_leak_two();

// --> main function

int main()
{
    const int DIM_MAX = 10000;

    for (int i = 0; i != DIM_MAX; i++) {
        memory_leak_one();
        memory_leak_two();
    }

    return 0;
}

// --> functions definition

// (a)

void memory_leak_one()
{
    const int DIM = 1000;
    double * a1 = new double [DIM];

    for (int i = 0; i != DIM; i++) {
        a1[i] = i;
    }

    delete [] a1;
}

// (b)

void memory_leak_two()
{
    double * a1 = new double;

    *a1 = 10;

    delete a1;
}

// end
