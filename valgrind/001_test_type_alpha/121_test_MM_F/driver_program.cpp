//=============//
// memory leak //
//=============//

#include <iostream>

using std::cout;
using std::endl;

// --> f1 --> leaking function

int & f1()
{
    int * x = new int;
    *x = 11;

    return *x;
}

// --> f2

int & f2()
{
    int * x = &f1();
    return *x;
}

// --> f3

int & f3()
{
    int * x = &f2();
    return *x;
}

// --> f4

int & f4()
{
    int * x = &f3();
    return *x;
}

// the main function

int main()
{
    // --> f1

    int * x = &f1();

    cout << " -->  x = " <<  x << endl;
    cout << " --> *x = " << *x << endl;

    delete x;
    x = nullptr;

    // --> f2

    x = &f2();

    cout << " -->  x = " <<  x << endl;
    cout << " --> *x = " << *x << endl;

    delete x;
    x = nullptr;

    // --> f3

    x = &f3();

    cout << " -->  x = " <<  x << endl;
    cout << " --> *x = " << *x << endl;

    delete x;
    x = nullptr;

    // --> f4

    x = &f4();

    cout << " -->  x = " <<  x << endl;
    cout << " --> *x = " << *x << endl;

    delete x;
    x = nullptr;

    return 0;
}

// END
