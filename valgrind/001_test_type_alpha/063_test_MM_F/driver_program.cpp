
#include <iostream>
#include <vector>
#include <memory>
#include <cmath>

using std::endl;
using std::cout;
using std::cin;
using std::vector;
using std::make_shared;
using std::shared_ptr;
using std::pow;

// the main function

int main()
{
    const unsigned int DIM_A = static_cast<unsigned int>(pow(10.0, 7.0));
    const unsigned int DIM_B = 100;

    for (unsigned int ik = 0; ik != DIM_B; ik++) {

        cout << "------------------------------------------>> " << ik << endl;

        shared_ptr<vector<shared_ptr<double>>> spv = make_shared<vector<shared_ptr<double>>>();

        for (unsigned int i = 0; i != DIM_A; i++) {
            spv->push_back(make_shared<double>(i));
        }

        cout << "------>> *(*spv)[0] = " << *(*spv)[0] << endl;
        cout << "------>> *(*spv)[1] = " << *(*spv)[1] << endl;
        cout << "------>> *(*spv)[2] = " << *(*spv)[2] << endl;

        spv->clear();
        spv->shrink_to_fit();
    }

    return 0;
}

// end
