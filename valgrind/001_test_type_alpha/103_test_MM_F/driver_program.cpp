
//============//
// shared_ptr //
//============//

#include <iostream>
#include <memory>

using std::cout;
using std::endl;
using std::cin;
using std::shared_ptr;
using std::unique_ptr;

// deleler D1

template<class T>
struct D1 {
    void operator()(T * p)
    {
        delete [] p;
    }
};

//===================//
// the main function //
//===================//

int main ()
{
    shared_ptr<double> sp1;
    shared_ptr<double> sp2;
    double * p1;
    unique_ptr<double> up1;
    unique_ptr<double, D1<double>> up2;

    // get the RAM space needed

    cout << "--> sizeof(sp1) = " << sizeof(sp1) << endl;
    cout << "--> sizeof(p1)  = " << sizeof(p1)  << endl;
    cout << "--> sizeof(up1) = " << sizeof(up1) << endl;
    cout << "--> sizeof(up2) = " << sizeof(up2) << endl;

    // assign values to the pointers

    p1 = new double;
    *p1 = 10.0;

    sp1.reset(new double);
    *sp1 = 20.0;

    sp2.reset(new double(-1));

    sp1 = sp2;

    up1.reset(new double);
    *up1 = 30.0;

    up2.reset(new double);
    *up2 = 40.0;

    cout << "--> *sp1 = " << *sp1 << endl;
    cout << "--> *sp2 = " << *sp2 << endl;
    cout << "--> *p1  = " << *p1  << endl;
    cout << "--> *up1 = " << *up1 << endl;
    cout << "--> *up2 = " << *up2 << endl;

    delete p1;

    return 0;
}

//=====//
// END //
//=====//
