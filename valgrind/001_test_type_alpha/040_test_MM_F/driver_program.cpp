//============//
// shared_ptr //
//============//

#include <iostream>
#include <memory>
#include <vector>

using std::endl;
using std::cout;
using std::cin;
using std::shared_ptr;

int main()
{
    shared_ptr<int> sp;  // empty

    sp.reset(new int);  // takes ownership of pointer

    *sp = 10;

    cout << *sp << endl;

    sp.reset(new int);  // deletes managed object, acquires new pointer

    *sp = 20;

    cout << *sp << endl;

    sp.reset();          // deletes managed object

    return 0;
}

// end
