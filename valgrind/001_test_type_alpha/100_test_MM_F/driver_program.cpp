
//===========================//
// unique_ptr::reset example //
//===========================//

#include <iostream>
#include <memory>

using std::cout;
using std::endl;
using std::cin;
using std::unique_ptr;
using std::addressof;
using std::default_delete;
using std::move;

//===================//
// the main function //
//===================//

int main ()
{
    const int DIM = 500000000;
    const int TRIALS = 10;

    for (int ik = 0; ik != TRIALS; ik++) {
        cout << "-------------------------------------------------->> " << ik << endl;

        unique_ptr<int[]> upA(new int [DIM]);

        cout << "-------------------------->> A " << endl;

        for (int i = 0; i != DIM; i++) {
            upA[i] = i;
        }

        cout << "-------------------------->> B " << endl;

        unique_ptr<int[]> upB(new int [DIM]);

        upB = move(upA); // USE THE HEAP OF upA but deletes upA;

        for (int i = 0; i != DIM; i++) {
            upB[i] = i+10;
        }

        upB.reset(nullptr);
    }

    return 0;
}

//=====//
// END //
//=====//
