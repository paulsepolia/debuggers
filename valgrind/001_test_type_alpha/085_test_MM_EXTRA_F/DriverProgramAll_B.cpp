
//===============================//
// UNIQUE POINTER BASED VECTOR   //
// vs                            //
// ORDINARY POINTER BASED VECTOR //
//===============================//

#include <iostream>
#include <cmath>
#include <iomanip>
#include <ctime>

using std::endl;
using std::cout;
using std::cin;
using std::pow;
using std::fixed;
using std::setprecision;

#include "Vector.h"
#include "VectorUnique.h"

//===================//
// the main function //
//===================//

int main()
{
    // local parameters

    const unsigned long int DIM = static_cast<unsigned long int>(pow(10.0, 8.0));
    const unsigned long int TRIALS = 2*static_cast<unsigned long int>(pow(10.0, 0.0));

    // local variables

    time_t t1;
    time_t t2;

    // adjust output

    cout << fixed;
    cout << setprecision(6);

    for (unsigned long int i = 0; i != TRIALS; i++) {

        cout << "-------------------------------------------------------->> " << i << endl;

        cout << "--> 1  --> declare unique vectors" << endl;

        t1 = clock();

        VectorUnique<double> vun1;
        VectorUnique<double> vun2;

        t2 = clock();

        cout << "--> time taken is = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << "--> 2  --> declare vectors" << endl;

        t1 = clock();

        Vector<double> v1;
        Vector<double> v2;

        t2 = clock();

        cout << "--> time taken is = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << "--> 3  --> allocate unique vectors" << endl;

        t1 = clock();

        vun1.allocate(DIM);
        vun2.allocate(DIM);

        t2 = clock();

        cout << "--> time taken is = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << "--> 4  --> allocate vectors" << endl;

        t1 = clock();

        v1.allocate(DIM);
        v2.allocate(DIM);

        t2 = clock();

        cout << "--> time taken is = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << "--> 5  --> build unique vectors" << endl;

        t1 = clock();

        for (unsigned long int j = 0; j != DIM; j++) {
            vun1.set_element(j, static_cast<double>(j));
            vun2.set_element(j, static_cast<double>(0.1+j));
        }

        t2 = clock();

        cout << "--> time taken is = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << "--> 6  --> build vectors" << endl;

        t1 = clock();

        for (unsigned long int j = 0; j != DIM; j++) {
            v1.set_element(j, static_cast<double>(j));
            v2.set_element(j, static_cast<double>(0.1+j));
        }

        t2 = clock();

        cout << "--> time taken is = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << "--> 7  --> set equal two unique vectors" << endl;

        t1 = clock();

        vun1 = vun2;

        t2 = clock();

        cout << "--> time taken is = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << "--> 8  --> set equal two vectors" << endl;

        t1 = clock();

        v1 = v2;

        t2 = clock();

        cout << "--> time taken is = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << "--> 9  --> declare via copy-constructor a unique vector" << endl;

        t1 = clock();

        VectorUnique<double> vun3(DIM);

        t2 = clock();

        cout << "--> time taken is = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << "--> 10 --> declare via copy-constructor a vector" << endl;

        t1 = clock();

        Vector<double> v3(DIM);

        t2 = clock();

        cout << "--> time taken is = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << "--> 11 --> add two unique vectors" << endl;

        t1 = clock();

        vun3 = vun1 + vun2;

        t2 = clock();

        cout << "--> time taken is = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << "--> 12 --> add two vectors" << endl;

        t1 = clock();

        v3 = v1 + v2;

        t2 = clock();

        cout << "--> time taken is = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << "--> 13 --> subtract two unique vectors" << endl;

        t1 = clock();

        vun3 = vun1 - vun2;

        t2 = clock();

        cout << "--> time taken is = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << "--> 14 --> subtract two vectors" << endl;

        t1 = clock();

        v3 = v1 - v2;

        t2 = clock();

        cout << "--> time taken is = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << "--> 15 --> multiply two unique vectors" << endl;

        t1 = clock();

        vun3 = vun1 * vun2;

        t2 = clock();

        cout << "--> time taken is = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << "--> 16 --> multiply two vectors" << endl;

        t1 = clock();

        v3 = v1 * v2;

        t2 = clock();

        cout << "--> time taken is = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << "--> 17 --> divide two unique vectors" << endl;

        t1 = clock();

        vun3 = vun1 / vun2;

        t2 = clock();

        cout << "--> time taken is = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << "--> 18 --> divide two vectors" << endl;

        t1 = clock();

        v3 = v1 / v2;

        t2 = clock();

        cout << "--> time taken is = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    }

    return 0;
}

//=====//
// end //
//=====//
