
//================//
// shared pointer //
//================//

#include <iostream>
#include <memory>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::shared_ptr;
using std::vector;

//==================//
// the main program //
//==================//

typedef vector<double> vd;

int main ()
{
    const int IMAX = 10;

    for (int i = 0; i != IMAX; i++) {
        cout << "---------------------------------------------->> " << i << endl;

        vd v1;

        shared_ptr<vd> sp1(new vd( {1,2,3,4,5}));
        shared_ptr<vd> sp2(sp1);
        shared_ptr<vd> sp3(sp1);
        shared_ptr<vd> sp4(sp1);

        cout << " --> sp1.use_count() = " << sp1.use_count() << endl;
        cout << " --> sp2.use_count() = " << sp2.use_count() << endl;
        cout << " --> sp3.use_count() = " << sp3.use_count() << endl;
        cout << " --> sp4.use_count() = " << sp4.use_count() << endl;

        cout << " --> sp1.get() = " << sp1.get() << endl;
        cout << " --> sp2.get() = " << sp2.get() << endl;
        cout << " --> sp3.get() = " << sp3.get() << endl;
        cout << " --> sp4.get() = " << sp4.get() << endl;

        cout << " --> sizeof(sp1) = " << sizeof(sp1) << endl;
        cout << " --> sizeof(sp2) = " << sizeof(sp2) << endl;
        cout << " --> sizeof(sp3) = " << sizeof(sp3) << endl;
        cout << " --> sizeof(sp4) = " << sizeof(sp4) << endl;

        cout << " --> (*sp1.get())[0] = " << (*sp1.get())[0] << endl;
        cout << " --> (*sp1.get())[1] = " << (*sp1.get())[1] << endl;
        cout << " --> (*sp1.get())[2] = " << (*sp1.get())[2] << endl;
        cout << " --> (*sp1.get())[3] = " << (*sp1.get())[3] << endl;
        cout << " --> (*sp1.get())[4] = " << (*sp1.get())[4] << endl;

        cout << " --> (*sp2.get())[0] = " << (*sp2.get())[0] << endl;
        cout << " --> (*sp2.get())[1] = " << (*sp2.get())[1] << endl;
        cout << " --> (*sp2.get())[2] = " << (*sp2.get())[2] << endl;
        cout << " --> (*sp2.get())[3] = " << (*sp2.get())[3] << endl;
        cout << " --> (*sp2.get())[4] = " << (*sp2.get())[4] << endl;

        cout << " --> (*sp3.get())[0] = " << (*sp3.get())[0] << endl;
        cout << " --> (*sp3.get())[1] = " << (*sp3.get())[1] << endl;
        cout << " --> (*sp3.get())[2] = " << (*sp3.get())[2] << endl;
        cout << " --> (*sp3.get())[3] = " << (*sp3.get())[3] << endl;
        cout << " --> (*sp3.get())[4] = " << (*sp3.get())[4] << endl;

        cout << " --> (*sp4.get())[0] = " << (*sp4.get())[0] << endl;
        cout << " --> (*sp4.get())[1] = " << (*sp4.get())[1] << endl;
        cout << " --> (*sp4.get())[2] = " << (*sp4.get())[2] << endl;
        cout << " --> (*sp4.get())[3] = " << (*sp4.get())[3] << endl;
        cout << " --> (*sp4.get())[4] = " << (*sp4.get())[4] << endl;

    }

    return 0;
}

// END
