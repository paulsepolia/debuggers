
//=====================================//
// Unique pointer with default deleter //
//=====================================//

//================//
// NO MEMORY LEAK //
//================//

#include <iostream>
#include <memory>

using std::endl;
using std::cout;
using std::cin;
using std::unique_ptr;

// struct Foo

struct Foo {

    // construnctor

    Foo()
    {
        cout << "Foo..." << endl;
    }

    // destructor

    virtual ~Foo()
    {
        cout << "~Foo..." << endl;
    }
};

// the main function

int main()
{
    const int I_MAX = 1000000000;

    for (int i = 0; i != I_MAX; i++) {
        cout << "------------------------------------------------>> " << i << endl;

        cout << " --> Creating new Foo" << endl;
        unique_ptr<Foo> up(new Foo());  // up owns the Foo pointer

        cout << " --> Replace owned Foo with a new Foo" << endl;
        up.reset(new Foo()); // calls the default deleter

        cout << " --> Release and delete the owned Foo" << endl;
        up.reset(nullptr);
    }

    return 0;
}

// END
