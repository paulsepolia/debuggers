
#include <memory>
#include <iostream>

using std::endl;
using std::cout;
using std::cin;
using std::shared_ptr;
using std::make_shared;

// the main function

int main()
{
    cout << " --> step 1 --> declare shared pointer" << endl;

    shared_ptr<int> sp;

    cout << " --> step 2 --> make a shared pointer" << endl;

    sp = make_shared<int>(10);

    cout << " --> step 3 --> output" << endl;

    cout << " --> *sp = " << *sp << endl;

    cout << " --> step 4 --> make a shared pointer again" << endl;

    sp = make_shared<int>(11);

    cout << " --> step 5 --> output" << endl;

    cout << " --> *sp = " << *sp << endl;

    return 0;
}

// end
