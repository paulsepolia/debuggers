
//==========//
// weak_ptr //
//==========//

#include <iostream>
#include <memory>
#include <typeinfo>

using std::weak_ptr;
using std::cout;
using std::cin;
using std::endl;
using std::make_shared;

weak_ptr<int> gw;

void f()
{
    if (auto spt = gw.lock()) { // Has to be copied into a shared_ptr before usage
        cout << *spt << endl;
        cout << " --> typeid(spt).name() = " << typeid(spt).name() << endl;
    } else {
        cout << "gw is expired" << endl;
    }
}

// the main function

int main()
{
    {
        cout << "------------------------------->> 1" << endl;

        auto sp = make_shared<int>(42);
        cout << " --> typeid(sp).name() = " << typeid(sp).name() << endl;
        cout << " --> typeid(sp).hash_code() = " << typeid(sp).hash_code() << endl;

        gw = sp;
        cout << " --> typeid(gw).hash_code() = " << typeid(gw).hash_code() << endl;
        f();
    }

    {
        cout << "------------------------------->> 2" << endl;

        auto sp = make_shared<int>(43);
        cout << " --> typeid(sp).name() = " << typeid(sp).name() << endl;
        cout << " --> typeid(sp).hash_code() = " << typeid(sp).hash_code() << endl;

        gw = sp;
        cout << " --> typeid(gw).hash_code() = " << typeid(gw).hash_code() << endl;
        f();
    }

    cout << "---------------------------->> 3" << endl;

    cout << " --> typeid(f).name() = " << typeid(f).name() << endl;
    cout << " --> typeid(&f).name() = " << typeid(&f).name() << endl;
    cout << " --> typeid(f).hash_code() = " << typeid(f).hash_code() << endl;
    cout << " --> typeid(&f).hash_code() = " << typeid(&f).hash_code() << endl;

    auto sp = make_shared<int>(100);
    gw = sp;

    f();

    cout << "--> reset the shared pointer" << endl;

    sp.reset();

    f();
}

// END
