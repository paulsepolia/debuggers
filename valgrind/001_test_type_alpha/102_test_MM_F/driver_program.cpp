
//============//
// shared_ptr //
//============//

#include <iostream>
#include <memory>

using std::cout;
using std::endl;
using std::cin;
using std::shared_ptr;

//===================//
// the main function //
//===================//

int main ()
{
    shared_ptr<double> sp1;

    // set heap space for the shared pointer

    sp1.reset(new double);

    // assign a value to the shared pointer

    *sp1 = 10.0;

    // get the value

    cout << "-->           *sp1  = " << *sp1 << endl;
    cout << "-->            sp1  = " <<  sp1 << endl;
    cout << "-->           &sp1  = " << &sp1 << endl;
    cout << "--> addressof(sp1)  = " << addressof(sp1) << endl;

    // double pointer

    shared_ptr<double> sp2;

    // get the value

//	cout << "-->           *sp2  = " << *sp2 << endl; // NO VALUE
    cout << "-->            sp2  = " <<  sp2 << endl;
    cout << "-->           &sp2  = " << &sp2 << endl;
    cout << "--> addressof(sp2)  = " << addressof(sp2) << endl;

    sp2 = sp1;

    // get the value

    cout << "-->           *sp2  = " << *sp2 << endl;
    cout << "-->            sp2  = " <<  sp2 << endl;
    cout << "-->           &sp2  = " << &sp2 << endl;
    cout << "--> addressof(sp2)  = " << addressof(sp2) << endl;

    return 0;
}

//=====//
// END //
//=====//
