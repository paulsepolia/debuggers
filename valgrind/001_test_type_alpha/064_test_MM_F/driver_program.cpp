
#include <iostream>
#include <vector>
#include <memory>
#include <cmath>

using std::endl;
using std::cout;
using std::cin;
using std::vector;
using std::unique_ptr;
using std::pow;

// the main function

int main()
{
    const unsigned int DIM_A = static_cast<unsigned int>(pow(10.0, 7.0));
    const unsigned int DIM_B = 100;

    for (unsigned int ik = 0; ik != DIM_B; ik++) {

        cout << "------------------------------------------>> " << ik << endl;

        unique_ptr<vector<unique_ptr<double>>> spv;

        spv.reset(new vector<unique_ptr<double>>);

        for (unsigned int i = 0; i != DIM_A; i++) {
            spv->push_back(unique_ptr<double>(new double(i)));
        }

        cout << "------>> *(*spv)[0] = " << *(*spv)[0] << endl;
        cout << "------>> *(*spv)[1] = " << *(*spv)[1] << endl;
        cout << "------>> *(*spv)[2] = " << *(*spv)[2] << endl;

        spv->clear();         // those two commands are not mandatory
        spv->shrink_to_fit(); // those two commands are not mandatory
    }

    return 0;
}

// end
