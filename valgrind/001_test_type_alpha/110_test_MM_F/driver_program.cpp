
//================//
// shared pointer //
//================//

#include <iostream>
#include <memory>

using std::cin;
using std::cout;
using std::endl;
using std::shared_ptr;

//==================//
// the main program //
//==================//

int main ()
{
    const int IMAX = 50;

    for (int i = 0; i != IMAX; i++) {
        cout << "---------------------------------------------->> " << i << endl;

        int * p1 = new int(10);
        shared_ptr<int> sp1(p1);

        if (sp1.get() == p1) {
            cout << "sp1 and p1 point to the same location" << endl;
        };

        // three ways of accessing the same address

        cout << " --> *sp1.get() = " << *sp1.get() << endl;
        cout << " --> *sp1       = " << *sp1 << endl;
        cout << " --> *p1        = " << *p1 << endl;

        cout << " -->  sp1.get() = " << sp1.get() << endl;
        cout << " -->  p1        = " << p1 << endl;
    }

    return 0;
}

// END
