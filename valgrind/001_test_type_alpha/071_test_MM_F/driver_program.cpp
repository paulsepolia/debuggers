
//================//
// NO MEMORY LEAK //
//================//

#include <iostream>
#include <memory>
#include <cmath>

using std::endl;
using std::cout;
using std::cin;
using std::unique_ptr;
using std::shared_ptr;
using std::make_shared;
using std::pow;

// class A

template<class T>
class A {
public:

    // default constructor

    A() : sp(make_shared<T>(T(11.1))), up(new T(22.2)), p(new T(33.3)) {}

    // copy constructor

    A(const A & other) : sp(make_shared<T>(*other.sp)), up(new T(*other.up)), p(new T(*other.p)) {}

    // assignment operator

    A & operator=(const A & other)
    {
        sp = make_shared<T>(*other.sp);
        up.reset(new T(*other.up));
        *p = *other.p;

        return *this;
    }

    // + operator

    A operator+(const A & other)
    {
        A tmp;

        *tmp.sp = other.get_shared() + this->get_shared();
        *tmp.p = other.get_pointer() + this->get_pointer();
        tmp.up.reset(new T(other.get_unique() + this->get_unique()));

        return tmp;
    }

    // - operator

    A operator-(const A & other)
    {
        A tmp;

        *tmp.sp = other.get_shared() - this->get_shared();
        *tmp.p = other.get_pointer() - this->get_pointer();
        tmp.up.reset(new T(other.get_unique() - this->get_unique()));

        return tmp;
    }

    // * operator

    A operator*(const A & other)
    {
        A tmp;

        *tmp.sp = other.get_shared() * this->get_shared();
        *tmp.p = other.get_pointer() * this->get_pointer();
        tmp.up.reset(new T(other.get_unique() * this->get_unique()));

        return tmp;
    }

    // / operator

    A operator/(const A & other)
    {
        A tmp;

        *tmp.sp = other.get_shared() / this->get_shared();
        *tmp.p = other.get_pointer() / this->get_pointer();
        tmp.up.reset(new T(other.get_unique() / this->get_unique()));

        return tmp;
    }

    // set shared pointer

    void set_shared(const T & val)
    {
        sp = make_shared<T>(val);
    }

    // get shared pointer

    T get_shared() const
    {
        return *this->sp;
    }

    // set pointer

    void set_pointer(const T & val)
    {
        *p = val;
    }

    // get pointer

    T get_pointer() const
    {
        return *this->p;
    }

    // set unique pointer

    void set_unique(const T & val)
    {
        up.reset(new T(val));
    }

    // get unique pointer

    T get_unique() const
    {
        return *up.get();
    }

    // delete pointer

    void delete_pointer()
    {
        if (this->p!=0) {
            delete p;
            p = 0;
        }
    }

    // destructor

    virtual ~A()
    {
        if (p != 0) {
            delete p;
            p = 0;
        }
    }

private:

    shared_ptr<T> sp;
    unique_ptr<T> up;
    T * p;

};

// the main function

int main()
{
    const unsigned long int TRIALS = static_cast<unsigned long int>(pow(10.0, 2.0));
    const int TR2 = 10;

    A<double> a1;
    cout << " sizeof(a1) = " << sizeof(a1) << endl;

    cout << " --> a1.get_shared()  = " << a1.get_shared() << endl;
    cout << " --> a1.get_pointer() = " << a1.get_pointer() << endl;
    cout << " --> a1.get_unique()  = " << a1.get_unique() << endl;


    A<double> a2(a1);
    cout << " sizeof(a2) = " << sizeof(a2) << endl;

    cout << " --> a2.get_shared()  = " << a2.get_shared() << endl;
    cout << " --> a2.get_pointer() = " << a2.get_pointer() << endl;
    cout << " --> a2.get_unique()  = " << a2.get_unique() << endl;

    a2 = a1;

    cout << " --> a2.get_shared()  = " << a2.get_shared() << endl;
    cout << " --> a2.get_pointer() = " << a2.get_pointer() << endl;
    cout << " --> a2.get_unique()  = " << a2.get_unique() << endl;

    // check functions

    a1.set_shared(10);
    a2.set_shared(20);

    a1.set_pointer(44.4);
    a2.set_pointer(55.5);

    a1.set_unique(66.6);
    a2.set_unique(77.7);

    cout << " --> a1.get_shared()  = " << a1.get_shared() << endl;
    cout << " --> a2.get_shared()  = " << a2.get_shared() << endl;

    cout << " --> a1.get_pointer() = " << a1.get_pointer() << endl;
    cout << " --> a2.get_pointer() = " << a2.get_pointer() << endl;

    cout << " --> a1.get_unique()  = " << a1.get_unique() << endl;
    cout << " --> a2.get_unique()  = " << a2.get_unique() << endl;

    A<double> a3;

    a3 = a1 + a2;

    cout << " --> a3.get_shared()  = " << a3.get_shared() << endl;
    cout << " --> a3.get_pointer() = " << a3.get_pointer() << endl;
    cout << " --> a3.get_unique()  = " << a3.get_unique() << endl;

    a1.delete_pointer();
    a2.delete_pointer();
    a3.delete_pointer();

    // main memory loop

    for (int ik = 0; ik != TR2; ik++) {
        cout << "------------------------------------------------>> ik = " << ik << endl;

        for (unsigned long int i = 0; i != TRIALS; i++) {
            A<double> a1;
            A<double> a2(a1);
            A<double> a3;

            a2 = a1;
            a3 = a2;

            a3 = a2 + a1;
            a3 = a1 * a2;
            a3 = a1 / a2;
            a3 = a1 - a2;

            a1.delete_pointer();
            a2.delete_pointer();
            a3.delete_pointer();
        }
    }

    return 0;
}

// end
