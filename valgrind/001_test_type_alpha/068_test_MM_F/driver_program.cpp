
//=============//
// MEMORY LEAK //
//=============//

#include <iostream>
#include <vector>
#include <memory>
#include <cmath>

using std::endl;
using std::cout;
using std::cin;
using std::vector;
using std::unique_ptr;
using std::shared_ptr;
using std::make_shared;
using std::pow;

// class A

template<class T>
class A {
public:

    // default constructor

    A() : sp(make_shared<T>(T(11.1))), up(new T(22.2)), p(new T(33.3)) {}

    // destructor

    virtual ~A()
    {
        up.reset(); // not really needed
        delete p;
        p = 0;
    }

private:

    shared_ptr<T> sp;
    unique_ptr<T> up;
    T * p;

};

// class B

template<class T>
class B {
public:

    // default constructor

    B() : sp(make_shared<T>(T(11.1))), up(new T(22.2)), p(new T(33.3)) {}

    // destructor

    virtual ~B()
    {
        delete p;
        p = 0;
    }

private:

    shared_ptr<T> sp;
    unique_ptr<T> up;
    T * p;

};

// the main function

int main()
{
    const unsigned long int TRIALS = static_cast<unsigned long int>(pow(10.0, 7.2));
    const int TR2 = 10;

    A<double> a1;
    cout << " sizeof(a1) = " << sizeof(a1) << endl;

    B<double> b1;
    cout << " sizeof(b1) = " << sizeof(b1) << endl;

    for (int ik = 0; ik != TR2; ik++) {
        cout << "------------------------------------------------>> ik = " << ik << endl;

        for (unsigned long int i = 0; i != TRIALS; i++) {
            A<double> a1;
            A<double> a2;
            A<double> a3;
        }
    }

    return 0;
}

// end
