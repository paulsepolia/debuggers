
#include <iostream>

using std::endl;
using std::cout;
using std::cin;

// class A

class A {
public:

    // constructor

    explicit A() : pA(0), m_dim(0), flag(false) {}

    // copy construnctor

    A(A const & other) : m_dim(other.m_dim), flag(false)
    {
        if (other.is_allocated()) {
            this->allocate(m_dim);
            for (unsigned int i = 0; i != m_dim; i++) {
                this->set_element(other.get_element(i),i);
            }
        }
    }

    // assignment operator

    A & operator=(const A & other)
    {
        this->m_dim = other.m_dim;
        this->allocate(other.m_dim);
        for (unsigned int i = 0; i != this->m_dim; i++) {
            this->set_element(other.get_element(i),i);
        }

        return *this;
    }

    // is_allocated

    bool is_allocated() const
    {

        return this->flag;
    }

    // allocate

    void allocate(const unsigned int & dim)
    {
        if (!this->is_allocated()) {
            this->pA = new double [dim];
            this->m_dim = dim;
        }
        this->flag = true;
    }

    // deallocate

    void deallocate()
    {
        if (this->is_allocated()) {
            delete [] this->pA;
            this->pA = 0;
        }
    }

    // set_element

    void set_element(const double & elem, const unsigned int & index)
    {
        if(this->is_allocated()) {
            this->pA[index] = elem;
        }
    }

    // get_element

    double get_element(const unsigned int & index) const
    {
        if(this->is_allocated()) {
            return this->pA[index];
        } else {
            return -999;
        }
    }

    // set

    void set(const double & elem)
    {
        if (this->is_allocated()) {
            for (unsigned int i = 0; i != this->m_dim; i++) {
                this->set_element(elem, i);
            }
        }
    }

    // destructor

    virtual ~A()
    {
        if(this->is_allocated()) {
            delete [] this->pA;
            this->pA = 0;
        }
    }

private:

    double * pA;
    unsigned int m_dim;
    bool flag;

};

// class B

class B: public A {

public:

    virtual ~B() {}

};

// the main function

int main()
{
    // --> 1

    B ma;

    const unsigned int DIM = 1000000;

    ma.allocate(DIM);

    double elem;

    for (unsigned int i = 0; i != DIM; i++) {
        elem = static_cast<double>(i);
        ma.set_element(elem, i);
    }

    cout << " --> i = 0 --> " << ma.get_element(0) << endl;
    cout << " --> i = 1 --> " << ma.get_element(1) << endl;
    cout << " --> i = 2 --> " << ma.get_element(2) << endl;
    cout << " --> i = 3 --> " << ma.get_element(3) << endl;

    ma.deallocate();

    // --> 2

    const unsigned int DIM_B = 100000000;

    for (int i = 0; i != 4; i++) {
        {
            B mb1;
            B mb2;
            B mb3;

            mb1.allocate(DIM_B);
            mb2.allocate(DIM_B);
            mb3.allocate(DIM_B);

            mb1.set(10.0);
            mb2.set(20.0);
            mb3.set(30.0);

            cout << " --> mb1.get_element(1) = " << mb1.get_element(1) << endl;
            cout << " --> mb2.get_element(2) = " << mb2.get_element(2) << endl;
            cout << " --> mb3.get_element(3) = " << mb3.get_element(3) << endl;
        }
    }

    // --> 3

    for (int i = 0; i != 4; i++) {
        {
            B mb1;

            mb1.allocate(DIM_B);
            mb1.set(10.0+i);

            B mb2(mb1);
            B mb3(mb1);

            cout << " --> mb1.get_element(1) = " << mb1.get_element(1) << endl;
            cout << " --> mb2.get_element(2) = " << mb2.get_element(2) << endl;
            cout << " --> mb3.get_element(3) = " << mb3.get_element(3) << endl;
        }
    }

    // --> 4

    for (int i = 0; i != 4; i++) {
        {
            B mb1;

            mb1.allocate(DIM_B);
            mb1.set(21.0+i);

            B mb2;
            mb2 = mb1;
            B mb3;
            mb3 = mb2;

            cout << " --> mb1.get_element(1) = " << mb1.get_element(1) << endl;
            cout << " --> mb2.get_element(2) = " << mb2.get_element(2) << endl;
            cout << " --> mb3.get_element(3) = " << mb3.get_element(3) << endl;
        }
    }

    // sentineling

//     int sentinel;
//     cout << " --> enter an integer to exit: ";
//     cin >> sentinel;

    return 0;
}

// end
