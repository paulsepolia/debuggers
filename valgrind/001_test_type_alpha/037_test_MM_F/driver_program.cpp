//============//
// unique_ptr //
//============//

#include <iostream>
#include <memory>

using std::endl;
using std::cout;
using std::cin;
using std::unique_ptr;

const int DIM = 10000;

// class A

class A {
public:

    A() : x(0)
    {
        x = new int [DIM];
        for (int i = 0; i != DIM; i++) {
            x[i] = i;
        }
    }

private:
    int * x;

public:
    virtual ~A()
    {
        delete [] x;
    }
};

// the main function

int main ()
{
    const int DIM = 100000;
    const int TRIALS = 5;

    // unique_ptr

    for (int iK = 0; iK != TRIALS; iK++) {

        cout << "--------------------------------------->> " << iK << endl;

        unique_ptr<A[]> foo (new A [DIM]);

        for (int i = 0; i != DIM; ++i) {
            foo[i];
        }
    }

    // old style ptr

    for (int iK = 0; iK != TRIALS; iK++) {

        cout << "--------------------------------------->> " << iK << endl;

        A * foo  = new A [DIM];

        for (int i = 0; i != DIM; ++i) {
            foo[i];
        }

        delete [] foo;
    }

    return 0;
}

// end
