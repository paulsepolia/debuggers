
//================//
// unique pointer //
//================//

#include <iostream>
#include <memory>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::unique_ptr;
using std::vector;

//==================//
// the main program //
//==================//

typedef vector<double> vd;

int main ()
{
    const int IMAX = 10;

    for (int i = 0; i != IMAX; i++) {
        cout << "---------------------------------------------->> " << i << endl;

        unique_ptr<vd> up1(new vd( {1,2,3,4,5}));
        unique_ptr<vd> up2;
        unique_ptr<vd> up3;
        unique_ptr<vd> up4;

        cout << " --> sizeof(up1) = " << sizeof(up1) << endl;
        cout << " --> sizeof(up2) = " << sizeof(up2) << endl;
        cout << " --> sizeof(up3) = " << sizeof(up3) << endl;
        cout << " --> sizeof(up4) = " << sizeof(up4) << endl;

        cout << " --> (*up1)[0] = " << (*up1)[0] << endl;
        cout << " --> (*up1)[1] = " << (*up1)[1] << endl;
        cout << " --> (*up1)[2] = " << (*up1)[2] << endl;
        cout << " --> (*up1)[3] = " << (*up1)[3] << endl;
        cout << " --> (*up1)[4] = " << (*up1)[4] << endl;

        cout << " --> up1.get() = " << up1.get() << endl;
        cout << " --> up2.get() = " << up2.get() << endl;
        cout << " --> up3.get() = " << up3.get() << endl;
        cout << " --> up4.get() = " << up4.get() << endl;

        up2.reset(new vd( {2}));
        up3.reset(new vd( {3}));
        up4.reset(new vd( {4}));

        cout << " --> up1.get() = " << up1.get() << endl;
        cout << " --> up2.get() = " << up2.get() << endl;
        cout << " --> up3.get() = " << up3.get() << endl;
        cout << " --> up4.get() = " << up4.get() << endl;

        cout << " --> (*up1)[0] = " << (*up1)[0] << endl;
        cout << " --> (*up2)[0] = " << (*up2)[0] << endl;
        cout << " --> (*up3)[0] = " << (*up3)[0] << endl;
        cout << " --> (*up4)[0] = " << (*up4)[0] << endl;

        up2 = move(up1);
        cout << " --> (*up2)[0] = " << (*up2)[0] << endl;

        up3 = move(up2);
        cout << " --> (*up3)[0] = " << (*up3)[0] << endl;

        up4 = move(up3);
        cout << " --> (*up4)[0] = " << (*up4)[0] << endl;

        cout << " --> up1.get() = " << up1.get() << endl;
        cout << " --> up2.get() = " << up2.get() << endl;
        cout << " --> up3.get() = " << up3.get() << endl;
        cout << " --> up4.get() = " << up4.get() << endl;

        up4.reset();

        cout << " --> up4.get() = " << up4.get() << endl;

    }

    return 0;
}

// END
