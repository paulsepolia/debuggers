#!/bin/bash

  # 1. compile

  g++     -g                 \
		  -O0                \
          -Wall              \
          -std=gnu++17       \
          driver_program.cpp \
          -o x_gnu
