
//===========================//
// unique_ptr::reset example //
// custom deleters 		    //
//===========================//

#include <iostream>
#include <memory>

using std::cout;
using std::endl;
using std::cin;
using std::unique_ptr;

// deleter okay

struct DD_good {
    void operator()(int * p)
    {
        delete p;
    }
};

// deleter wrong

struct DD_wrong {
    void operator()(int * p)
    {
        delete p;
        int * p2 = new int [100];
    }
};


// deleter slow

struct DD_slow {
    void operator()(int * p)
    {
        delete p;
        const unsigned long int DIM = 50000000;
        for (unsigned long int i = 0; i != DIM; i++) {
            int * p2 = new int [1000000];
            delete [] p2;
        }
    }
};


//===================//
// the main function //
//===================//

int main ()
{
    const int TRIALS = 10;

    for (int ik = 0; ik != TRIALS; ik++) {
        cout << "-------------------------------------------------->> " << ik << endl;

        unique_ptr<int> upA(new int);
        cout << "-------------------------->> A" << endl;
        *upA = 10;
        upA.reset(nullptr);

        unique_ptr<int, DD_good> upB(new int, DD_good());
        cout << "-------------------------->> B" << endl;
        *upB = 20;
        upB.reset(nullptr);

        unique_ptr<int, DD_wrong> upC(new int, DD_wrong());
        cout << "-------------------------->> C" << endl;
        *upC = 30;
        upC.reset(nullptr); // MEMORY LEAK HERE
        // THE HAND_MADE DELETER DOES NOT WORK PROPERLY
        // BECAUSE ADDS DATA TO THE HEAP

        unique_ptr<int, DD_slow> upD(new int, DD_slow());
        cout << "-------------------------->> D" << endl;
        *upD = 40;
        upD.reset(nullptr); // slow deleter
        // does extra stuff here

    }

    return 0;
}

//=====//
// END //
//=====//
