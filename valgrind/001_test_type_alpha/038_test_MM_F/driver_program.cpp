//============//
// unique_ptr //
// shared_ptr //
//============//

#include <iostream>
#include <memory>
#include <vector>

using std::endl;
using std::cout;
using std::cin;
using std::shared_ptr;
using std::unique_ptr;
using std::vector;

const int DIM_A = 100;

// class A

class A {
public:

    A() : x(0)
    {
        x = shared_ptr<int>(new int(10));
        for (int i = 0; i != DIM_A; i++) {
            v1.push_back(x);
        }
    }

private:

    shared_ptr<int> x;
    vector<shared_ptr<int>> v1;

public:

    virtual ~A() {}
};

// the main function

int main ()
{
    const int DIM = 100000;
    const int TRIALS = 5;

    // unique_ptr

    for (int iK = 0; iK != TRIALS; iK++) {

        cout << "--------------------------------------->> " << iK << endl;

        unique_ptr<A[]> foo (new A [DIM]);

        for (int i = 0; i != DIM; ++i) {
            foo[i];
        }
    }

    // old style ptr

    for (int iK = 0; iK != TRIALS; iK++) {

        cout << "--------------------------------------->> " << iK << endl;

        A * foo  = new A [DIM];

        for (int i = 0; i != DIM; ++i) {
            foo[i];
        }

        delete [] foo;
    }

    return 0;
}

// end
