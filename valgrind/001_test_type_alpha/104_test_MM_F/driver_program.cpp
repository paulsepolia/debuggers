
//============//
// shared_ptr //
// unique_ptr //
// pointer    //
//============//

#include <iostream>
#include <memory>
#include <vector>
#include <cmath>

using std::cout;
using std::endl;
using std::cin;
using std::shared_ptr;
using std::unique_ptr;
using std::vector;
using std::pow;

// deleler D1

template<class T>
struct D1 {
    void operator()(T * p)
    {
        delete [] p;
    }
};

//===================//
// the main function //
//===================//

typedef unsigned long int uli;

int main ()
{
    // local parameters

    const unsigned long int DIM = static_cast<unsigned long int>(pow(10.0, 4.0));

    // local variables

    shared_ptr<double> sp1;
    shared_ptr<double> sp2;
    double * p1;

    vector<shared_ptr<double>> vsp1;
    vector<shared_ptr<double>> vsp2;
//     vector<unique_ptr<double>> vup1; // CAN BE USED TO BUILD A VECTOR see example 106
    vector<double*> vp1;

    // build vector of shared pointers

    for (uli i = 0; i != DIM; i++) {
        sp1.reset(new double(i));
        vsp1.push_back(sp1);
    }

    cout << "--> output --> 1" << endl;

    cout << "  vsp1[0] = " << vsp1[0] << endl;
    cout << "  vsp1[1] = " << vsp1[1] << endl;
    cout << "  vsp1[2] = " << vsp1[2] << endl;
    cout << "  vsp1[3] = " << vsp1[3] << endl;

    cout << " *vsp1[0] = " << *vsp1[0] << endl;
    cout << " *vsp1[1] = " << *vsp1[1] << endl;
    cout << " *vsp1[2] = " << *vsp1[2] << endl;
    cout << " *vsp1[3] = " << *vsp1[3] << endl;

    // vsp1 equals vsp2

    vsp2 = vsp1;

    cout << "--> output --> 2" << endl;

    cout << "  vsp2[0] = " << vsp2[0] << endl;
    cout << "  vsp2[1] = " << vsp2[1] << endl;
    cout << "  vsp2[2] = " << vsp2[2] << endl;
    cout << "  vsp2[3] = " << vsp2[3] << endl;

    cout << " *vsp2[0] = " << *vsp2[0] << endl;
    cout << " *vsp2[1] = " << *vsp2[1] << endl;
    cout << " *vsp2[2] = " << *vsp2[2] << endl;
    cout << " *vsp2[3] = " << *vsp2[3] << endl;

    // build vector of pointers

    for (uli i = 0; i != DIM; i++) {
        p1 = new double; // MEMORY LEAK HERE
        *p1 = i;
        vp1.push_back(p1);
    }

    cout << "--> output --> 3" << endl;

    cout << "  vp1[0] = " << vp1[0] << endl;
    cout << "  vp1[1] = " << vp1[1] << endl;
    cout << "  vp1[2] = " << vp1[2] << endl;
    cout << "  vp1[3] = " << vp1[3] << endl;

    cout << " *vp1[0] = " << *vp1[0] << endl;
    cout << " *vp1[1] = " << *vp1[1] << endl;
    cout << " *vp1[2] = " << *vp1[2] << endl;
    cout << " *vp1[3] = " << *vp1[3] << endl;

    delete p1;
    p1 = nullptr;

    return 0;
}

//=====//
// END //
//=====//
