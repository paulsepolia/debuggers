
#include <memory>
#include <iostream>

using std::endl;
using std::cout;
using std::cin;
using std::make_shared;
using std::shared_ptr;

// the main function

int main()
{
    cout << " --> step 1 --> declare shared pointer" << endl;

    shared_ptr<int> sp;

    cout << " --> step 2 --> get value of shared pointer" << endl;

    if (sp != 0) {
        cout << " --> *sp = " << *sp << endl;
    }

    cout << " --> step 3 --> take ownership of a pointer" << endl;

    sp.reset(new int);       // takes ownership of pointer

    *sp = 10;

    cout << " --> *sp = " << *sp << endl;

    cout << " --> step 4 --> take ownership of a pointer" << endl;

    sp.reset(new int);       // takes ownership of pointer

    if (sp != 0) {
        *sp = 11;
        cout << " --> *sp = " << *sp << endl;
    }

    return 0;
}

// end
