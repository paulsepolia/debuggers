
#include <iostream>
#include <cstring>

using std::endl;
using std::cout;
using std::cin;

// struct

struct A {
    char name[40];
    int age;
    int * id = new int;
};

// the main function

int main ()
{
    const int DIM = 10000;

    A * person = new A [DIM];

    char myname[] = "Pierre de Fermat";
    int myage = 123;

    // initialize person[0]

    memcpy(person[0].name, myname, strlen(myname)+1);

    memcpy(&person[0].age, &myage, sizeof(myage));

    *(person[0].id) = 10;

    // output

    cout << " --> person[0].name  = " << person[0].name << endl;
    cout << " --> person[0].age   = " << person[0].age << endl;
    cout << " --> person[0].id    = " << person[0].id << endl;
    cout << " --> *(person[0].id) = " << *(person[0].id) << endl;

    // copy all

    for (int i = 1; i != DIM; i++) {
        memcpy(&person[i], &person[0], sizeof(person[0]));
    }

    // output

    cout << " --> sizeof(long double)   = " << sizeof(long double) << endl;
    cout << " --> sizeof(long long int) = " << sizeof(long long int) << endl;
    cout << " --> sizeof(int)           = " << sizeof(int) << endl;
    cout << " --> sizeof(person[0])     = " << sizeof(person[0]) << endl;
    cout << " --> sizeof(person[1])     = " << sizeof(person[1]) << endl;
    cout << " --> sizeof(person[2])     = " << sizeof(person[2]) << endl;
    cout << " --> sizeof(A)             = " << sizeof(A) << endl;
    cout << " --> sizeof(person)        = " << sizeof(person) << endl;
    cout << " --> sizeof(&person)       = " << sizeof(&person) << endl;
    cout << " --> person[1].name        = " << person[1].name << endl;
    cout << " --> person[1].age         = " << person[1].age << endl;
    cout << " --> person[1].id          = " << person[1].id << endl;
    cout << " --> person[2].id          = " << person[2].id << endl;
    cout << " --> *(person[1].id)       = " << *(person[1].id) << endl;
    cout << " --> *(person[2].id)       = " << *(person[2].id) << endl;

    // free up heap

    person[0].id = 0;

    for (int i = 0; i != DIM; i++) {

        delete person[i].id;
    }

    delete [] person;

    return 0;
}

// end
