//============//
// unique_ptr //
//============//

#include <iostream>
#include <memory>

using std::endl;
using std::cout;
using std::cin;
using std::unique_ptr;

// the main function

int main ()
{
    const int DIM = 100000000;
    const int TRIALS = 20;

    // unique_ptr

    for (int iK = 0; iK != TRIALS; iK++) {

        cout << "--------------------------------------->> " << iK << endl;

        unique_ptr<double[]> foo (new double [DIM]);

        for (int i = 0; i != DIM; ++i) {
            foo[i] = i;
        }
    }

    // old style ptr

    for (int iK = 0; iK != TRIALS; iK++) {

        cout << "--------------------------------------->> " << iK << endl;

        double * foo  = new double [DIM];

        for (int i = 0; i != DIM; ++i) {
            foo[i] = i;
        }

        delete [] foo;
    }

    return 0;
}

// end
