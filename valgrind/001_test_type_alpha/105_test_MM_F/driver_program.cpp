
//============//
// unique_ptr //
//============//

#include <iostream>
#include <memory>
#include <vector>
#include <cmath>

using std::cout;
using std::endl;
using std::cin;
using std::unique_ptr;
using std::vector;
using std::pow;

//===================//
// the main function //
//===================//

typedef unsigned long int uli;

int main ()
{
    // local parameters

    const uli DIM = static_cast<uli>(pow(10.0, 4.0));
    const uli TRIALS = static_cast<uli>(pow(10.0, 1.0));

    // local variables

    vector<unique_ptr<double>> vup1;
    unique_ptr<double> up1;

    for(uli ik = 0; ik != TRIALS; ik++) {

        cout << "--------------------------------------------------->> " << ik << endl;

        // build vector of unique pointers

        for (uli i = 0; i != DIM; i++) {
            up1.reset(new double(i));
            vup1.push_back(move(up1));
        }

        cout << " *vup1[0] = " << *vup1[0] << endl;
        cout << " *vup1[1] = " << *vup1[1] << endl;
        cout << " *vup1[2] = " << *vup1[2] << endl;
        cout << " *vup1[3] = " << *vup1[3] << endl;

        vup1.clear();
        vup1.shrink_to_fit();
    }

    vup1.clear();
    vup1.shrink_to_fit();

    return 0;
}

//=====//
// END //
//=====//
