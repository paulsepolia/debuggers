//=============//
// memory leak //
//=============//

#include <iostream>

using std::cout;
using std::endl;

// leaking function

int & f()
{
    int * x = new int;
    *x = 11;

    return *x;
}

// the main function

int main()
{
    int * x = &f();

    cout << " -->  x = " <<  x << endl;
    cout << " --> *x = " << *x << endl;

//	delete x;

    return 0;
}

// END
