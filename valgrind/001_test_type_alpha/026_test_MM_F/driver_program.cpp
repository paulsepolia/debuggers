
#include <iostream>

using std::endl;
using std::cout;
using std::cin;

// function A

const int DIM = 1000;

int * funA(const int & dim)
{
    return (new int [dim]);
}

// function B

int * funB()
{
    return (new int);
}

// the main function

int main()
{
    // funA

    int * p;

    p = funA(DIM);

    for (int i = 0; i != DIM; i++) {
        p[i] = i;
    }

    cout << " p[0] = " << p[0] << endl;
    cout << " p[1] = " << p[1] << endl;
    cout << " p[2] = " << p[2] << endl;

    delete [] p;

    // funB

    int * pA;

    pA = funB();

    *pA = 10;

    cout << " *pA = " << *pA << endl;

    delete pA;

    return 0;
}

// end
