
//================//
// shared pointer //
//================//

#include <iostream>
#include <memory>

using std::cin;
using std::cout;
using std::endl;
using std::shared_ptr;

//==================//
// the main program //
//==================//

int main ()
{
    int * p = new int (10);
    shared_ptr<int> a (p);

    if (a.get() == p) {
        cout << "a and p point to the same location" << endl;
    };

    // three ways of accessing the same address

    cout << *a.get() << endl;
    cout << *a << endl;
    cout << *p << endl;

    return 0;
}

// END
