
//===========================//
// unique_ptr::reset example //
//===========================//

#include <iostream>
#include <memory>

using std::cout;
using std::endl;
using std::cin;
using std::unique_ptr;

//===================//
// the main function //
//===================//

int main ()
{
    unique_ptr<int> up;     // empty

    cout << " --> 1" << endl;

    up.reset(new int);       // takes ownership of pointer

    *up = 5;

    cout << " -->  up.get()  = " << up.get() << endl;
    cout << " --> *up        = " << *up << endl;
    cout << " --> sizeof(up) = " << sizeof(up) << endl;

    cout << " --> 2" << endl;

    up.reset(new int);       // deletes managed object, acquires new pointer

    *up = 10;

    cout << " -->  up.get()  = " << up.get() << endl;
    cout << " --> *up        = " << *up << endl;
    cout << " --> sizeof(up) = " << sizeof(up) << endl;

    cout << " --> 3" << endl;

    up.reset();               // deletes managed object

    cout << " -->  up.get()  = " << up.get() << endl;
//     cout << " --> *up        = " << *up << endl; // no value and breaks
    cout << " --> sizeof(up) = " << sizeof(up) << endl;
    cout << " --> sizeof(int*) = " << sizeof(int*) << endl;
    cout << " --> sizeof(int) = " << sizeof(int) << endl;
    cout << " --> sizeof(double) = " << sizeof(double) << endl;
    cout << " --> sizeof(long double) = " << sizeof(long double) << endl;
    cout << " --> sizeof(long double *) = " << sizeof(long double *) << endl;

    return 0;
}

//=====//
// END //
//=====//
