//================================//
// shared_ptr constructor example //
//================================//

#include <iostream>
#include <memory>

using std::endl;
using std::cout;
using std::cin;

using std::default_delete;
using std::shared_ptr;
using std::unique_ptr;
using std::move;
using std::allocator;

// the main function

int main ()
{
    shared_ptr<int> p1;

    shared_ptr<int> p2 (nullptr);

    shared_ptr<int> p3 (new int);

    shared_ptr<int> p4 (new int, default_delete<int>());

    shared_ptr<int> p8 (unique_ptr<int>(new int));

    cout << "use_count: " << endl;

    cout << "p1: " << p1.use_count() << endl;
    cout << "p2: " << p2.use_count() << endl;
    cout << "p3: " << p3.use_count() << endl;
    cout << "p4: " << p4.use_count() << endl;
    cout << "p8: " << p8.use_count() << endl;

    return 0;
}

// end
