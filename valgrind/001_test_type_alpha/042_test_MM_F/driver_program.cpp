
// EXCELLENT

//============//
// shared_ptr //
//============//

#include <iostream>
#include <memory>
#include <vector>
#include <cmath>

using std::endl;
using std::cout;
using std::cin;
using std::shared_ptr;
using std::pow;

// the main function

int main()
{
    const int TRIALS = 100;
    const int DIM = 1000000;

    for (int ik = 0; ik != TRIALS; ik++) {
        cout << "-------------------------------------------->> " << ik << endl;

        shared_ptr<shared_ptr<double>> da;

        da.reset(new shared_ptr<double>[DIM], [](shared_ptr<double> * p) {
            delete [] p;
        });

        da.get()[0].reset(new double[DIM], [](double * p) {
            delete [] p;
        });
    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

// end
