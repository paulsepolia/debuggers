//==================//
// use of allocator //
//==================//

#include <iostream>
#include <memory>
#include <cmath>
#include <iomanip>

using std::cout;
using std::endl;
using std::allocator;
using std::pow;
using std::boolalpha;

// type definitions

typedef unsigned long int uli;
typedef const uli culi;

// the main program

int main()
{
    cout << boolalpha;

    // local parameters

    culi DIMEN(static_cast<culi>(pow(10.0, 4.0)));

    // main game

    // allocate an array of pointers to double

    allocator<double*> pdal;
    allocator<double> dal;

    // allocate --> step --> 1/2

    double** arr1 = pdal.allocate(DIMEN);
    double** arr2 = pdal.allocate(DIMEN);

    // allocate --> step --> 2/2

    for(uli i = 0; i != DIMEN; i++) {
        arr1[i] = dal.allocate(DIMEN);
        arr2[i] = dal.allocate(DIMEN);
    }

    // build arrays --> arr1 and arr2

    for(uli i = 0; i != DIMEN; i++) {
        for(uli j = 0; j != DIMEN; j++) {
            arr1[i][j] = static_cast<double>(i+j);
            arr2[i][j] = static_cast<double>(i+j+1);
        }
    }

    // some output

    cout << " --> arr1[0][0] = " << arr1[0][0] << endl;
    cout << " --> arr1[1][1] = " << arr1[1][1] << endl;
    cout << " --> arr1[2][2] = " << arr1[2][2] << endl;

    cout << " --> arr2[0][0] = " << arr2[0][0] << endl;
    cout << " --> arr2[1][1] = " << arr2[1][1] << endl;
    cout << " --> arr2[2][2] = " << arr2[2][2] << endl;

    cout << " (arr1 == arr2) = " << (arr1 == arr2) << endl;

    cout << " --> pdal.address(arr1[0]) = " << pdal.address(arr1[0]) << endl;
    cout << " --> pdal.address(arr1[1]) = " << pdal.address(arr1[1]) << endl;
    cout << " --> pdal.address(arr1[2]) = " << pdal.address(arr1[2]) << endl;

    cout << " --> pdal.address(arr2[0]) = " << pdal.address(arr2[0]) << endl;
    cout << " --> pdal.address(arr2[1]) = " << pdal.address(arr2[1]) << endl;
    cout << " --> pdal.address(arr2[2]) = " << pdal.address(arr2[2]) << endl;

    // deallocate --> 1

    for(uli i = 0; i != DIMEN; i++) {
        dal.deallocate(arr1[i], DIMEN);
        dal.deallocate(arr2[i], DIMEN);
    }

    // deallocate --> 2

    pdal.deallocate(arr1, DIMEN);
    pdal.deallocate(arr2, DIMEN);

    return 0;
}

// end
