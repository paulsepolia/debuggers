//======================//
// USING MOVE SEMANTICS //
//======================//


#include <iostream>
#include <utility>

using std::cout;
using std::endl;
using std::move;

// function --> fun1

double fun1()
{
    double p(10);
    cout << " -->  p  = " <<  p << endl;
    cout << " --> &p  = " << &p << endl;

    return p;
}

// function --> fun2

double fun2()
{
    double p(20);
    cout << " -->  p  = " <<  p << endl;
    cout << " --> &p  = " << &p << endl;

    return move(p);
}

// the main function

int main()
{
    double p1;

    // fun1

    p1 = fun1();

    cout << " -->  p1 = fun1();" << endl;
    cout << " -->  p1 = " <<  p1 << endl;
    cout << " --> &p1 = " << &p1 << endl;

    // fun2

    p1 = fun2();

    cout << " -->  p1 = fun2();" << endl;
    cout << " -->  p1 = " <<  p1 << endl;
    cout << " --> &p1 = " << &p1 << endl;

    return 0;
}

// end
