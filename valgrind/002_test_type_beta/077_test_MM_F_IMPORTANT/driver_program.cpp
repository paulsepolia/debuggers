//======================//
// USING MOVE SEMANTICS //
//======================//

#include <iostream>
#include <utility>
#include <cmath>
#include <vector>

using std::cout;
using std::endl;
using std::move;
using std::pow;
using std::vector;

// type definition

typedef unsigned long int uli;

// parameters

const uli TRIALS = static_cast<uli>(pow(10.0, 1.0));
const uli DIMEN = static_cast<uli>(pow(10.0, 8.0));

// function --> fun1

vector<double> fun1()
{
    vector<double> v_local;

    cout << " --> fun1 --> build the vector" << endl;

    for(uli i = 0; i != DIMEN; i++) {
        v_local.push_back(static_cast<double>(i));
    }

    cout << " --> fun1 --> return the vector" << endl;

    return v_local;
}

// function --> fun2

vector<double> fun2()
{
    vector<double> v_local;

    cout << " --> fun2 --> build the vector" << endl;

    for(uli i = 0; i != DIMEN; i++) {
        v_local.push_back(static_cast<double>(i));
    }

    cout << " --> fun2 --> return the vector" << endl;

    return move(v_local);
}

// the main function

int main()
{
    vector<double> v1;

    for(uli i = 0; i != TRIALS; i++) {
        cout << "----------------------------------------------->> " << i << endl;
        // fun1

        v1 = fun1();

        cout << " --> v1 = fun1();" << endl;
        cout << " --> v1[0]       = " << v1[0] << endl;
        cout << " --> v1[DIMEN-1] = " << v1[DIMEN-1] << endl;

        // fun2

        v1 = fun2();

        cout << " --> v1 = fun2();" << endl;
        cout << " --> v1[0]       = " << v1[0] << endl;
        cout << " --> v1[DIMEN-1] = " << v1[DIMEN-1] << endl;
    }

    return 0;
}

// end
