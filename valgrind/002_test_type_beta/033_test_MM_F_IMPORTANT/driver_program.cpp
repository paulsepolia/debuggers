//================//
// shared pointer //
//================//

#include <iostream>
#include <memory>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::shared_ptr;
using std::vector;

//==================//
// the main program //
//==================//

int main ()
{
    const int SIZE = 5;
    const int TRIALS = 100;
    shared_ptr<double> sp1;
    vector<double*> pv1;
    vector<shared_ptr<double>> pv2;

    for (int ik = 0; ik != TRIALS; ik++) {

        // build the vector of pointers

        for (int i = 0; i != SIZE; i++) {
            pv1.push_back(new double (i));
        }

        // output the vector of C pointers

        for (int i = 0; i != SIZE; i++) {
            cout << " --> *pv1[" << i << "]= " << *pv1[i] << endl;
        }

        // delete the pointers

        for (int i = 0; i != SIZE; i++) {
            delete pv1[i];
        }

        // build the vector of shared pointers

        for (int i = 0; i != SIZE; i++) {
            sp1.reset(new double(i));
            pv2.push_back(sp1);
        }

        // output vector of shared pointers

        for (int i = 0; i != SIZE; i++) {
            cout << " --> *pv2[" << i << "]= " << *pv2[i] << endl;
        }

        // clear and reset the vector

        pv1.clear();
        pv1.shrink_to_fit();
        pv2.clear();
        pv2.shrink_to_fit();

    }

    return 0;
}

// END
