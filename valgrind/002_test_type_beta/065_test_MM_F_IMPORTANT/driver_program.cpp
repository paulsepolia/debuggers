//=========================================//
// 'new' and 'delete' overloaded operators //
//=========================================//

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

//=====================================//
// class-specific allocation functions //
//=====================================//

// class X declaration

class X {
public:

    // new
    static void* operator new(size_t);

    // new[]
    static void* operator new[](size_t);

    // delete
    static void operator delete(void*);

    // delete[]
    static void operator delete[](void*);
};

// class X efinition

// X::new
void * X::operator new(size_t sz)
{
    cout << " --> class X 'new' for size = " << sz << endl;
    return ::operator new(sz);
}

// X::new[]
void* X::operator new[](size_t sz)
{
    cout << " --> class X 'new[sz]' for size = " << sz << endl;
    return ::operator new(sz);
}

// X::delete
void X::operator delete(void* ptr)
{
    cout << " --> class X 'delete' called" << endl;
    ::operator delete(ptr);
}

// X::delete[]

void X::operator delete[](void* ptr)
{
    cout << " --> class X 'delete []' called" << endl;
    ::operator delete(ptr);
}

//====================================================//
// overload of 'new' and 'delete' for non class types //
//====================================================//

void* operator new(size_t sz) throw(std::bad_alloc)
{
    cout << " --> global op 'new' called, size = " << sz << endl;
    return malloc(sz);
}

void operator delete(void* ptr) noexcept
{
    cout << " --> global op 'delete' called" << endl;
    free(ptr);
}

void* operator new[](size_t sz) throw(std::bad_alloc)
{
    cout << " --> global op 'new [sz]' called, size = " << sz << endl;
    return malloc(sz);
}

void operator delete[](void* ptr) noexcept
{
    cout << " --> global op 'delete []' called" << endl;
    free(ptr);
}

// the main function

int main()
{
    const int DO_MAX = 1000;

    for(int i = 0; i != DO_MAX; i++) {
        cout << " ----------------------------------------------------->> " << i << endl;
        cout << " --> X* p1 = new X;" << endl;
        X* p1 = new X;

        cout << " --> delete p1;" << endl;
        delete p1;

        cout << " --> X* p2 = new X[10];" << endl;
        X* p2 = new X[10];

        cout << " --> delete [] p2;" << endl;
        delete[] p2;

        cout << " --> double * p3 = new double;" << endl;
        double * p3 = new double;

        cout << " --> delete p3;" << endl;
        delete p3;

        cout << " --> double * p4 = new double [20];" << endl;
        double * p4 = new double [20];

        cout << " --> delete [] p4" << endl;
        delete [] p4;
    }
    return 0;
}

// END
