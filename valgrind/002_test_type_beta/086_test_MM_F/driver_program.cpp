//=======================//
// use of move symantics //
//=======================//

#include <iostream>
#include <new>

using std::cout;
using std::endl;
using std::move;

// the main program

int main()
{
    // copy

    int* p1(new int(10));

    cout << " --> *p1 = " << *p1 << endl;
    cout << " -->  p1 = " <<  p1 << endl;
    cout << " --> &p1 = " << &p1 << endl;

    int* p2(0);

    cout << " -->  p2 = " <<  p2 << endl;
    cout << " --> &p2 = " << &p2 << endl;

    cout << " -->  p2 = p1;" << endl;

    p2 = p1;

    cout << " --> *p2 = " << *p2 << endl;
    cout << " -->  p2 = " <<  p2 << endl;
    cout << " --> &p2 = " << &p2 << endl;

    // move

    int* p3(new int(10));

    cout << " --> *p3 = " << *p3 << endl;
    cout << " -->  p3 = " <<  p3 << endl;
    cout << " --> &p3 = " << &p3 << endl;

    int* p4(0);

    cout << " -->  p4 = " <<  p4 << endl;
    cout << " --> &p4 = " << &p4 << endl;

    cout << " -->  p4 = move(p3);" << endl;

    p4 = move(p3);

    cout << " --> *p3 = " << *p3 << endl;
    cout << " -->  p3 = " <<  p3 << endl;
    cout << " --> &p3 = " << &p3 << endl;

    cout << " --> *p4 = " << *p4 << endl;
    cout << " -->  p4 = " <<  p4 << endl;
    cout << " --> &p4 = " << &p4 << endl;

    delete p2;
    delete p4;

    cout << " p--> exit" << endl;
    return 0;
}

// end
