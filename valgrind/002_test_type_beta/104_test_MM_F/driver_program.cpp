//=====================//
// make_shared example //
//=====================//

#include <iostream>
#include <memory>

using std::shared_ptr;
using std::make_shared;
using std::pair;
using std::cout;
using std::endl;

int main()
{
    shared_ptr<int> foo1(make_shared<int>(10));
    shared_ptr<int> foo2(new int(10)); // same as the above statement

    auto bar(make_shared<int>(20));
    auto baz(make_shared<pair<int,int>>(30,40));

    cout << "*foo1: " << *foo1 << endl;
    cout << "*foo2: " << *foo2 << endl;
    cout << " *bar: " << *bar << endl;
    cout << " *baz: " << baz->first << ' ' << baz->second << endl;

    foo1.reset();
    foo2.reset();
    bar.reset();
    baz.reset();

    foo1.reset(new int(22));
    foo2.reset(new int(33));
    baz.reset(new pair<int,int>(1,2));
    bar.reset(new int(111));

    cout << "*foo1: " << *foo1 << endl;
    cout << "*foo2: " << *foo2 << endl;
    cout << " *bar: " << *bar << endl;
    cout << " *baz: " << baz->first << ' ' << baz->second << endl;

    return 0;
}

// end
