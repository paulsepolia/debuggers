//==================//
// use of allocator //
//==================//

#include <iostream>
#include <memory>
#include <cmath>
#include <iomanip>

using std::cout;
using std::endl;
using std::allocator;
using std::pow;
using std::boolalpha;

// type definitions

typedef unsigned long int uli;
typedef const uli culi;

// the main program

int main()
{
    cout << boolalpha;

    // local parameters

    culi DIMEN(static_cast<culi>(pow(10.0, 4.0)));

    // main game

    allocator<double> dal;
    double * arr1 = dal.allocate(DIMEN);
    double * arr2 = dal.allocate(DIMEN);

    // build array --> arr1

    for(uli i = 0; i != DIMEN; i++) {
        arr1[i] = static_cast<double>(i);
        arr2[i] = static_cast<double>(i+1);
    }

    cout << " --> arr1[0] = " << arr1[0] << endl;
    cout << " --> arr1[1] = " << arr1[1] << endl;
    cout << " --> arr1[2] = " << arr1[2] << endl;

    cout << " --> arr2[0] = " << arr2[0] << endl;
    cout << " --> arr2[1] = " << arr2[1] << endl;
    cout << " --> arr2[2] = " << arr2[2] << endl;

    cout << " (arr1 == arr2) = " << (arr1 == arr2) << endl;

    cout << " --> dal.address(arr1[0]) = " << dal.address(arr1[0]) << endl;
    cout << " --> dal.address(arr1[1]) = " << dal.address(arr1[1]) << endl;
    cout << " --> dal.address(arr1[2]) = " << dal.address(arr1[2]) << endl;

    cout << " --> &arr1[0] = " << &arr1[0] << endl;
    cout << " --> &arr1[1] = " << &arr1[1] << endl;
    cout << " --> &arr1[2] = " << &arr1[2] << endl;

    cout << " --> dal.address(arr2[0]) = " << dal.address(arr2[0]) << endl;
    cout << " --> dal.address(arr2[1]) = " << dal.address(arr2[1]) << endl;
    cout << " --> dal.address(arr2[2]) = " << dal.address(arr2[2]) << endl;

    cout << " --> &arr2[0] = " << &arr2[0] << endl;
    cout << " --> &arr2[1] = " << &arr2[1] << endl;
    cout << " --> &arr2[2] = " << &arr2[2] << endl;

    // deallocate

    dal.deallocate(arr1, DIMEN);
    dal.deallocate(arr2, DIMEN);

    return 0;
}

// end
