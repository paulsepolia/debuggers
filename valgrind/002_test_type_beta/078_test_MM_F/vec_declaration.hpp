//=======================//
// class Vec declaration //
//=======================//

#ifndef VEC_DECLARATION_H
#define VEC_DECLARATION_H

#include "parameters.hpp"

template<typename T>
class Vec {
public:

    // construnctor

    Vec();

    // destructor

    virtual ~Vec();

    // copy constructor

    Vec(const Vec &);

    // copy assignment operator

    Vec & operator=(const Vec &);

    // member functions

    void allocate(const uli &);
    void deallocate();
    T get(const uli &) const;
    void set(const uli &, const T &);
    T operator[](const uli &);

private:

    uli _size;
    T * _p;
};

#endif // VEC_DECLARATION_H
