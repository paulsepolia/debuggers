//================//
// shared pointer //
//================//

// Convert many C-pointers to shared pointer
// using only one shared ptr

#include <iostream>
#include <memory>

using std::cin;
using std::cout;
using std::endl;
using std::shared_ptr;

// function

double * fun1(const double & val, const int & dim)
{
    // C pointer to shared pointer

    double * p1 = new double [dim];
    shared_ptr<double> sp1; // if I do that the results are WRONG
    sp1.reset(p1);          // but there is no leak

    // build the array

    for (int i = 0; i != dim; i++) {
        p1[i] = static_cast<double>(i+val);
    }

    return p1;
}

//==================//
// the main program //
//==================//

int main ()
{
    const int DIM = 1000;
    const int IMAX = 10;
    const double VAL = 0.0;

    for (int i = 0; i != IMAX; i++) {
        cout << "---------------------------------------------->> " << i << endl;

        cout << " --> fun1[0] = " << fun1(VAL, DIM)[0] << endl;
        cout << " --> fun1[1] = " << fun1(VAL, DIM)[1] << endl;
        cout << " --> fun1[2] = " << fun1(VAL, DIM)[2] << endl;
        cout << " --> fun1[3] = " << fun1(VAL, DIM)[3] << endl;
    }

    return 0;
}

// END
