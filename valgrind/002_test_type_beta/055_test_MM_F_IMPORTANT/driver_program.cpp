//=============================================//
// delete heap via shared pointers 		  //
// and static shared pointer garbage collector //
// the classes are friends to garbage collector//
// NO LEAK!                        		  //
//=============================================//
//======================//
// BAD DESIGN and WRONG //
//======================//

#include <iostream>
#include <memory>
#include <vector>
#include <cmath>

using std::cin;
using std::cout;
using std::endl;
using std::shared_ptr;
using std::vector;
using std::pow;

//==================//
// type definitions //
//==================//

class A1;
class A2;
typedef unsigned long int uli;

//======================================//
// class 'GarbageCollector' declaration //
//======================================//

template<typename T>
class GarbageCollector {
public:
    virtual void freeRAM() const;
protected:
    static vector<T> _garbage;
public:
    friend class A1;
    friend class A2;
};

//=====================================//
// class 'GarbageCollector' definition //
//=====================================//

// static member initialization

template<typename T>
vector<T> GarbageCollector<T>::_garbage;

// member function --> FreeRAM

template<typename T>
void GarbageCollector<T>::freeRAM() const
{
    _garbage.clear();
    _garbage.shrink_to_fit();
}

//======================//
// class A1 declaration //
//======================//

class A1 {
public:

    // constructor

    A1();

    // destructor

    virtual ~A1();

    // member function

    virtual void fun() const;
};

//=====================//
// class A1 definition //
//=====================//

// constructor

A1::A1() {}

// destructor

A1::~A1() {}

// member function

void A1::fun() const
{
    const uli IMAX = static_cast<uli>(pow(10.0, 4.0));
    for (uli i = 0; i != IMAX; i++) {
        double * pd = new double(i);
        // collect garbages here
        shared_ptr<double> spd(pd);
        GarbageCollector<shared_ptr<double>>::_garbage.push_back(spd);
    }
}

//======================//
// class A2 declaration //
//======================//

class A2 {
public:

    // constructor

    A2();

    // destructor

    virtual ~A2();

    // member function

    virtual void fun() const;
};

//=====================//
// class A2 definition //
//=====================//

// constructor

A2::A2() {}

// destructor

A2::~A2() {}

// member function

void A2::fun() const
{
    const uli IMAX = static_cast<uli>(pow(10.0, 4.0));
    for (uli i = 0; i != IMAX; i++) {
        long double * pd = new long double(i);
        // collect garbages here
        shared_ptr<long double> spd(pd);
        GarbageCollector<shared_ptr<long double>>::_garbage.push_back(spd);
    }
}

//==================//
// the main program //
//==================//

int main ()
{
    // local parameters and variables

    const uli IMAX = 5UL;
    const uli DO_MAX = 10;
    GarbageCollector<double> trashD;
    GarbageCollector<long double> trashLD;

    for (uli k = 0; k != DO_MAX; k++) {

        cout << " -------------------------------------------------------->> k = " << k << endl;

        // # 1

        cout << " --> declare an object of type A1" << endl;

        A1 a1;

        cout << " --> call the 'fun' member function many times" << endl;

        for (uli i = 0; i != IMAX; i++) {
            a1.fun();
        }

        cout << " --> empty trash bin ..." << endl;

        trashD.freeRAM();

        // # 2

        cout << " --> declare an object of type A2" << endl;

        A2 a2;

        cout << " --> call the 'fun' member function many times" << endl;

        for (uli i = 0; i != IMAX; i++) {
            a2.fun();
        }

        cout << " --> empty trash bin ..." << endl;

        trashLD.freeRAM();

        // # 3

        cout << " --> declare an object of type A2" << endl;

        A2 a3;

        cout << " --> call the 'fun' member function many times" << endl;

        for (uli i = 0; i != IMAX; i++) {
            a3.fun();
        }

        cout << " --> empty trash bin ..." << endl;

        trashLD.freeRAM();

    }

    return 0;
}

// END
