//=======================//
// use of move symantics //
//=======================//

#include <iostream>
#include <list>
#include <new>
#include <cmath>
#include <iomanip>
#include <chrono>

using std::cout;
using std::endl;
using std::list;
using std::move;
using std::pow;
using std::boolalpha;
using namespace std::chrono;

// type definitions

typedef unsigned long int uli;
typedef const uli culi;

// the main program

int main()
{
    // local parameters

    culi TRIALS(static_cast<culi>(pow(10.0, 5.0)));
    culi DIMEN(static_cast<culi>(pow(10.0, 4.0)));
    cout << boolalpha;
    auto t1 = system_clock::now();
    auto t2 = system_clock::now();
    duration<double> time_span;

    // main game

    list<double> lis1;

    lis1.clear();
    lis1.resize(DIMEN);

    cout << " lis1.size() = " << lis1.size() << endl;

    list<double> lis2;
    lis2.clear();

    cout << " lis2.size() = " << lis2.size() << endl;

    cout << " lis2 = lis1;" << endl;

    t1 = system_clock::now();

    uli i = 0;
    while(i != TRIALS) {
        lis2 = lis1;
        i++;
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    cout << " i = " << i << endl;
    cout << " lis1.size() = " << lis1.size() << endl;
    cout << " lis2.size() = " << lis2.size() << endl;
    cout << " (lis1 == lis2) = " << (lis1 == lis2) << endl;

    t1 = system_clock::now();

    i = 0;
    while(i != TRIALS) {
        lis2 = move(lis1);
        lis1 = move(lis2);
        i++;
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    cout << " i = " << i << endl;
    cout << " lis1.size() = " << lis1.size() << endl;
    cout << " lis2.size() = " << lis2.size() << endl;
    cout << " (lis1 == lis2) = " << (lis1 == lis2) << endl;

    return 0;
}

// end
