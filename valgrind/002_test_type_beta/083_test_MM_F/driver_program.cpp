//==============================================//
// driver program 						   //
// move contructor and move assignment operator //
// and unique_ptr 						   //
//==============================================//

#include "parameters.hpp"
#include "vec.hpp"
#include <iostream>
#include <cmath>
#include <iomanip>

using std::cout;
using std::endl;
using std::cos;
using std::sin;
using std::fixed;
using std::setprecision;
using std::showpos;
using std::boolalpha;

// the main program

int main()
{
    cout << setprecision(10);
    cout << fixed;
    cout << showpos;
    cout << boolalpha;

    bool tmp_bool;

    for(uli i = 0; i != TRIALS; i++) {
        cout << "----------------------------------------------->> " << i << endl;

        Vec<double> v1;
        Vec<double> v2;

        cout << " ---------->> check allocation" << endl;
        tmp_bool = v1.check_allocation();
        cout << " ---------->> v1 --> " << tmp_bool << endl;
        tmp_bool = v2.check_allocation();
        cout << " ---------->> v2 --> " << tmp_bool << endl;

        cout << " ---------->> allocate" << endl;

        v1.allocate(DIMEN);
        v2.allocate(DIMEN);

        cout << " ---------->> check allocation" << endl;
        tmp_bool = v1.check_allocation();
        cout << " ---------->> v1 --> " << tmp_bool << endl;
        tmp_bool = v2.check_allocation();
        cout << " ---------->> v2 --> " << tmp_bool << endl;

        cout << " ---------->> build" << endl;

        for(uli j = 0; j != DIMEN; j++) {
            v1.set(j, sin(static_cast<double>(j)));
            v2.set(j, cos(static_cast<double>(j+1)));
        }

        cout << " ---------->> v1 = v2;" << endl;

        cout << " --> " << v1.get(10) << endl;
        cout << " --> " << v2.get(10) << endl;

        v1 = move(v2);

        cout << " --> " << v1.get(10) << endl;

        // some tests here

        //cout << " --> " << v2.get(10) << endl; // throws and error and quits
        //v2.set(10, 10.0); // throws an error and quits

        uli v1_size = v1.get_size();

        cout << " ---------->> v1_size = " << v1_size << endl;

        //v1.set(1000, 10.0);
        //v1.set(-1, 10.0);
        //v1.get(-10);
        //v2.get(0);
        //v2.get(-100);
        //v2.get(10);

        cout << " ---------->> deallocate" << endl;

        v1.deallocate();
        v2.deallocate();

        cout << " ---------->> check allocation" << endl;
        tmp_bool = v1.check_allocation();
        cout << " ---------->> v1 --> " << tmp_bool << endl;
        tmp_bool = v2.check_allocation();
        cout << " ---------->> v2 --> " << tmp_bool << endl;
    }

    cout << " p--> exit" << endl;
    return 0;
}

// end
