//================//
// shared pointer //
//================//

#include <iostream>
#include <memory>
#include <cmath>
#include <iomanip>

using std::cout;
using std::endl;
using std::allocator;
using std::pow;
using std::boolalpha;
using std::shared_ptr;

// type definitions

typedef unsigned long int uli;
typedef const uli culi;

// the main program

int main()
{
    cout << boolalpha;

    // local parameters

    culi DIMEN(static_cast<culi>(pow(10.0, 2.0)));
    culi TRIALS(static_cast<culi>(pow(10.0, 2.0)));

    // main game

    // allocate an array of pointers to double

    for(uli k = 0; k != TRIALS; k++) {

        cout << "----------------------------------------------------->> " << k << endl;

        shared_ptr<double>* arr1(new shared_ptr<double> [DIMEN]);
        shared_ptr<double>* arr2(new shared_ptr<double> [DIMEN]);

        for(uli i = 0; i != DIMEN; i++) {
            arr1[i].reset(new double [DIMEN]);
            arr2[i].reset(new double [DIMEN]);
        }

        // build arrays --> arr1 and arr2

        for(uli i = 0; i != DIMEN; i++) {
            for(uli j = 0; j != DIMEN; j++) {
                arr1[i].get()[j] = static_cast<double>(i+j);
                arr2[i].get()[j] = static_cast<double>(i+j+1);
            }
        }

        // some output

        cout << " --> arr1[0].get()[0] = " << arr1[0].get()[0] << endl;
        cout << " --> arr1[1].get()[1] = " << arr1[1].get()[1] << endl;
        cout << " --> arr1[2].get()[2] = " << arr1[2].get()[2] << endl;

        cout << " --> arr2[0].get()[0] = " << arr2[0].get()[0] << endl;
        cout << " --> arr2[1].get()[1] = " << arr2[1].get()[1] << endl;
        cout << " --> arr2[2].get()[2] = " << arr2[2].get()[2] << endl;

        cout << " (arr1 == arr2) = " << (arr1 == arr2) << endl;

        // deallocate  --> step --> 1/2

        for(uli i = 0; i != DIMEN; i++) {
            arr1[i].reset();
            arr2[i].reset();
        }

        // deallocate --> step --> 2/2

        delete [] arr1;
        delete [] arr2;
    }

    return 0;
}

// end
