
#include <iostream>

using std::endl;
using std::cout;
using std::cin;

// base class --> Base

class Base {
public:

    // constructor

    Base()
    {
        cout << " --> constructor --> Base" << endl;
        x1 = new int (11); // this is from Base
        x2 = new int (22); // this is from Base
        x3 = new int (33); // this is from Base
    }

    // destructor

    virtual ~Base()
    {
        cout << " --> destructor --> ~Base" << endl;

        if (x1) {
            cout << " --> ~Base --> delete --> x1" << endl;
            delete x1;
            x1 = 0;
        }

        if (x2) {
            cout << " --> ~Base --> delete --> x2" << endl;
            delete x2;
            x2 = 0;
        }

        if (x3) {
            cout << " --> ~Base --> delete --> x3" << endl;
            delete x3;
            x3 = 0;
        }
    }

protected:

    int * x1;
    int * x2;
    int * x3;
};

// derived class --> D1

class D1 : public Base {
public:

    // constructor

    D1() : Base()
    {
        cout << " --> constructor --> D1" << endl;
        x1 = new int (1); // this is from D1
        x2 = new int (2); // this is from D1
        x3 = new int (3); // this is from D1
    }

    // destructor

    virtual ~D1()
    {
        cout << " --> destructor --> ~D1" << endl;

        if (x1) {
            cout << " --> ~D1 --> delete --> x1" << endl;
            delete x1;
            x1 = 0;
        }

        if (x2) {
            cout << " --> ~D1 --> delete --> x2" << endl;
            delete x2;
            x2 = 0;
        }

        if (x3) {
            cout << " --> ~D1 --> delete --> x3" << endl;
            delete x3;
            x3 = 0;
        }
    }

protected:

    int * x1;
    int * x2;
    int * x3;
};

// the main program

int main()
{
    const int DIM = 100;

    // # 1

    cout << "------------------------------------------------------------>> # 1" << endl;
    cout << " --> Base * pb1 = new Base [DIM];" << endl;
    Base * pb1 = new Base [DIM];
    cout << " --> delete [] pb1;" << endl;
    delete [] pb1;

    // # 2

    cout << "------------------------------------------------------------>> # 2" << endl;

// you can not have an array of the following type
// it produces segmentation faults
// I DO NOT KNOW THE REASON YET

//               cout << " --> Base * pb2 = new D1 [DIM];" << endl;
//               Base * pb2 = new D1 [DIM] ;
//               cout << " --> delete [] pb2;" << endl;
//               delete [] pb2;

    // # 3

    cout << "------------------------------------------------------------>> # 3" << endl;
    cout << " --> D1 * pd1 = new D1 [DIM];" << endl;
    D1 * pd1 = new D1 [DIM];
    cout << " --> delete [] pd1;" << endl;
    delete [] pd1;

    cout << " ----------------------------> EXIT HERE(Nothing will happen)" << endl;

    return 0;
}

// end
