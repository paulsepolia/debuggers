//=================================//
// delete heap via shared pointers //
// NO LEAK!                        //
//=================================//

#include <iostream>
#include <memory>
#include <vector>
#include <cmath>

using std::cin;
using std::cout;
using std::endl;
using std::shared_ptr;
using std::vector;
using std::pow;

//==================//
// the main program //
//==================//

int main ()
{
    // local parameters and variables

    const unsigned long int IMAX = static_cast<unsigned long int>(pow(10.0, 7.0));
    const unsigned long int JMAX = static_cast<unsigned long int>(pow(10.0, 1.0));

    // main loop

    for(unsigned long int j = 0; j != JMAX; j++) {
        cout << " ----------------------------------------------------->> " << j << endl;
        cout << " --> declare the garbage container" << endl;
        vector<shared_ptr<double>> garbage;
        cout << " --> build the garbage container" << endl;
        for (unsigned long int i = 0; i != IMAX; i++) {
            double * p1 = new double(i);
            shared_ptr<double> sp1;
            sp1.reset(p1);
            garbage.push_back(sp1);
        }
        cout << " --> *garbage[j].get() = " << *garbage[j].get() << endl;
        cout << " --> clear the garbage container" << endl;
        garbage.clear();
        garbage.shrink_to_fit();
    }

    return 0;
}

// END
