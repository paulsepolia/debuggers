//=================================//
// delete heap via shared pointers //
// NO LEAK!                        //
//=================================//

#include <iostream>
#include <memory>
#include <vector>
#include <cmath>

using std::cin;
using std::cout;
using std::endl;
using std::shared_ptr;
using std::vector;
using std::pow;

static vector<shared_ptr<double>> garbage;

//==================//
// the main program //
//==================//

int main ()
{
    // local parameters and variables

    const unsigned long int IMAX = static_cast<unsigned long int>(pow(10.0, 6.0));
    const int TEST_VAL = 10;

    // main loop

    cout << " --> build the static shared pointer vector" << endl;

    for (unsigned long int i = 0; i != IMAX; i++) {
        double * p1 = new double(i);
        shared_ptr<double> sp1;
        sp1.reset(p1);
        garbage.push_back(sp1);
    }

    // test if the values are still there
    // YES THERE ARE!

    for (int i = 0; i != TEST_VAL; i++) {
        cout << " ----------------------------------------------->> i = " << i << endl << endl;
        cout << " -->  garbage[i]       = " <<  garbage[i] << endl;
        cout << " -->  garbage[i].get() = " <<  garbage[i].get() << endl;
        cout << " --> *garbage[i].get() = " << *garbage[i].get() << endl;

        cout << endl;
    }

    // test if the values are still there
    // YES THERE ARE!

    for (int i = IMAX-1; i != IMAX-1-TEST_VAL; i--) {
        cout << " ----------------------------------------------->> i = " << i << endl << endl;
        cout << " -->  garbage[i]       = " <<  garbage[i] << endl;
        cout << " -->  garbage[i].get() = " <<  garbage[i].get() << endl;
        cout << " --> *garbage[i].get() = " << *garbage[i].get() << endl;

        cout << endl;
    }

    return 0;
}

// END
