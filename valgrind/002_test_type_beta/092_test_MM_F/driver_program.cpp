//=======================//
// use of move symantics //
//=======================//

#include <iostream>
#include <set>
#include <new>
#include <cmath>
#include <iomanip>
#include <chrono>

using std::cout;
using std::endl;
using std::set;
using std::move;
using std::pow;
using std::boolalpha;
using namespace std::chrono;

// type definitions

typedef unsigned long int uli;
typedef const uli culi;

// the main program

int main()
{
    // local parameters

    culi TRIALS(static_cast<culi>(pow(10.0, 5.0)));
    culi DIMEN(static_cast<culi>(pow(10.0, 4.0)));
    cout << boolalpha;
    auto t1 = system_clock::now();
    auto t2 = system_clock::now();
    duration<double> time_span;

    // main game

    set<double> set1;
    set1.clear();

    // build map --> set1

    for(uli i = 0; i != DIMEN; i++) {
        set1.insert(static_cast<double>(i));
    }

    cout << " set1.size() = " << set1.size() << endl;

    set<double> set2;
    set2.clear();

    cout << " set2.size() = " << set2.size() << endl;

    cout << " set2 = set1;" << endl;

    t1 = system_clock::now();

    uli i = 0;
    while(i != TRIALS) {
        set2 = set1;
        i++;
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    cout << " i = " << i << endl;
    cout << " set1.size() = " << set1.size() << endl;
    cout << " set2.size() = " << set2.size() << endl;
    cout << " (set1 == set2) = " << (set1 == set2) << endl;

    t1 = system_clock::now();

    i = 0;
    while(i != TRIALS) {
        set2 = move(set1);
        set1 = move(set2);
        i++;
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    cout << " i = " << i << endl;
    cout << " set1.size() = " << set1.size() << endl;
    cout << " set2.size() = " << set2.size() << endl;
    cout << " (set1 == set2) = " << (set1 == set2) << endl;

    return 0;
}

// end
