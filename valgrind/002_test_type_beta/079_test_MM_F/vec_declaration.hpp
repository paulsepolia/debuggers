//=======================//
// class Vec declaration //
//=======================//

#ifndef VEC_DECLARATION_H
#define VEC_DECLARATION_H

#include "parameters.hpp"
#include <memory>

using std::shared_ptr;

template<typename T>
class Vec {
public:

    // construnctor

    Vec();

    // destructor

    virtual ~Vec();

    // copy constructor

    Vec(const Vec &);

    // move constructor

    Vec(Vec &&);

    // copy assignment operator

    Vec & operator=(const Vec &);

    // move assignment operator

    Vec & operator=(Vec &&);

    // member functions

    void allocate(const uli &);
    void deallocate();
    T get(const uli &) const;
    void set(const uli &, const T &);
    T operator[](const uli &);

private:

    uli _size;
    shared_ptr<T> _p;
};

#endif // VEC_DECLARATION_H
