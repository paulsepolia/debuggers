//==============================================//
// driver program 						   //
// move contructor and move assignment operator //
// and shared_ptr 						   //
//==============================================//

#include "parameters.hpp"
#include "vec.hpp"
#include "functions.hpp"
#include <iostream>

using std::cout;
using std::endl;

// the main program

int main()
{
    Vec<double> v1;

    for(uli i = 0; i != TRIALS; i++) {
        cout << "----------------------------------------------->> " << i << endl;

        // fun1

        cout << " p--> v1 = fun1<double>();" << endl;
        v1 = fun1<double>();
        cout << " p--> v1[0]       = " << v1[0] << endl;
        cout << " p--> v1[DIMEN-1] = " << v1[DIMEN-1] << endl;
        cout << " p--> v1.deallocate();" << endl;
        v1.deallocate();
    }

    cout << " p--> exit" << endl;
    return 0;
}

// end
