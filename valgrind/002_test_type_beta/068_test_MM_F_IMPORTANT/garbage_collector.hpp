//============================//
// class --> GarbageCollector //
//============================//

#ifndef GARBAGE_COLLECTOR_H
#define GARBAGE_COLLECTOR_H

#include "type_definitions.hpp"

#include <memory>
#include <vector>
#include <cstdlib>

using std::shared_ptr;
using std::vector;

//===================//
// class declaration //
//===================//

template<typename T>
class GarbageCollector {
public:
    virtual void freeRAM() const;
protected:
    static vector<shared_ptr<T>> _garbage;
private:
    template<typename K>
    friend void* operator new(uli, GarbageCollector<K>&) throw (std::bad_alloc);
};

//==================//
// class definition //
//==================//

// static member initialization

template<typename T>
vector<shared_ptr<T>> GarbageCollector<T>::_garbage;

// member function --> freeRAM

template<typename T>
void GarbageCollector<T>::freeRAM() const
{
    _garbage.clear();
    _garbage.shrink_to_fit();
}

// friend function --> operator new

template<typename T>
void* operator new(uli sz, GarbageCollector<T> & trash) throw (std::bad_alloc)
{
    //cout << "global op new called, size = " << sz << endl;
    void * local1 = malloc(sz);
    T * local2 = (T*)local1;
    shared_ptr<T> spd(local2);
    trash._garbage.push_back(spd);
    return local1;
}

#endif // GARBAGE_COLLECTOR_H
