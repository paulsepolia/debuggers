//==============//
// class --> A1 //
//==============//

#ifndef A1_H
#define A1_H

#include "type_definitions.hpp"
#include "garbage_collector.hpp"

#include <cmath>

using std::pow;

//===================//
// class declaration //
//===================//

template<typename T>
class A1 : public GarbageCollector<T> {
public:

    // constructor

    A1();

    // destructor

    virtual ~A1();

    // member function

    template<typename K>
    K fun();
};

//==================//
// class definition //
//==================//

// constructor

template<typename T>
A1<T>::A1() {}

// destructor

template<typename T>
A1<T>::~A1() {}

// member function

template<typename T>
template<typename K>
K A1<T>::fun()
{
    const uli IMAX = static_cast<uli>(pow(10.0, 4.0));
    K val;
    for (uli i = 0; i != IMAX; i++) {
        K * pd = new (*this) K(i); // use the overloaded operator
        val = *pd; // for testing purposes
    }

    return val;
}

#endif // A1_H
