//==============//
// class --> A2 //
//==============//

#ifndef A2_H
#define A2_H

#include "type_definitions.hpp"
#include "garbage_collector.hpp"

#include <cmath>

using std::pow;

//===================//
// class declaration //
//===================//

template<typename T>
class A2 : public GarbageCollector<T> {
public:

    // constructor

    A2();

    // destructor

    virtual ~A2();

    // member function

    template<typename K>
    K fun();
};

//==================//
// class definition //
//==================//

// constructor

template<typename T>
A2<T>::A2() {}

// destructor

template<typename T>
A2<T>::~A2() {}

// member function

template<typename T>
template<typename K>
K A2<T>::fun()
{
    const uli IMAX = static_cast<uli>(pow(10.0, 4.0));
    K val;
    for (uli i = 0; i != IMAX; i++) {
        K * pd = new (*this) K(i); // use the overloaded operator
        val = *pd; // for testing purposes
    }

    return val;
}

#endif // A2_H
