//=====================================//
// overloaded new and delete operators //
//=====================================//

#include <iostream>
#include <cmath>
#include <vector>

using std::cout;
using std::endl;
using std::pow;
using std::vector;
using std::free;

// type definitions

typedef unsigned long int uli;
typedef const uli culi;

// overlod the new operator

void* operator new(size_t sz) throw(std::bad_alloc)
{
    static uli index(0);
    index++;
    cout << " --> inside new overloaded operator, index = " << index << endl;
    return malloc(sz);
}

// overload the delete operator

void operator delete(void* ptr) noexcept
{

    static uli index1(0);
    index1++;
    cout << " --> inside delete overloaded operator, index1 = " << index1 << endl;
    if(ptr != 0) {
        static uli index2(0);
        index2++;
        cout << " --> overloaded delete, index2 = " << index2 << endl;
        free(ptr);
        ptr = 0;
    }
}

// class A

class A {};

// the main function

int main()
{
    // local parameters

    culi	DIMEN(2*static_cast<uli>(pow(10.0, 0.0)));

    //===================//
    // pointer to double //
    //===================//

    // allocate double

    cout << "------------------------------------------------>>  1" << endl;

    double * pd1 = new double(10.0);

    // delete pointer to double

    cout << "------------------------------------------------>>  2" << endl;

    delete pd1;

    // set to null pointer to double

    cout << "------------------------------------------------>>  3" << endl;

    pd1 = 0;

    // try to delete again already deleted pointer
    // THE INTEL COMPLILER DOES NOT CALL THE OVERLOADED delete OPERATOR
    // THE GCC DOES CALL THE OVERLOADED delete OPERATOR

    cout << "------------------------------------------------>>  4" << endl;

    delete pd1;

    //====================//
    // pointer to class A //
    //====================//

    // allocate pointer to A type object

    cout << "------------------------------------------------>>  5" << endl;

    A * pA1 = new A();

    //  delete pointer to A

    cout << "------------------------------------------------>>  6" << endl;

    delete pA1;

    // set to null pointer to A

    cout << "------------------------------------------------>>  7" << endl;

    pA1 = 0;

    // try to delete again already deleted pointer
    // THE INTEL COMPLILER DOES NOT CALL THE OVERLOADED delete OPERATOR
    // THE GCC DOES CALL THE OVERLOADED delete OPERATOR

    cout << "------------------------------------------------>>  8" << endl;

    delete pA1;

    //=============================//
    // pointer to array of doubles //
    //=============================//

    // allocate array of doubles

    cout << "------------------------------------------------>>  9" << endl;

    double * pd2 = new double [DIMEN];

    // delete pointer to array of doubles

    cout << "------------------------------------------------>> 10" << endl;

    delete [] pd2;

    // set to null pointer

    cout << "------------------------------------------------>> 11" << endl;

    pd2 = 0;

    // try to delete again already deleted pointer
    // THE INTEL COMPLILER DOES NOT CALL THE OVERLOADED delete OPERATOR
    // THE GCC DOES CALL THE OVERLOADED delete OPERATOR

    cout << "------------------------------------------------>> 12" << endl;

    delete [] pd2;

    //===============================//
    // pointer to array of A objects //
    //===============================//

    // allocate array of A type objects

    cout << "------------------------------------------------>> 13" << endl;

    A * pA2 = new A [DIMEN];

    // delete pointer to array of A type objects

    cout << "------------------------------------------------>> 14" << endl;

    delete [] pA2;

    // set to null pointer

    cout << "------------------------------------------------>> 15" << endl;

    pA2 = 0;

    // try to delete again already deleted pointer
    // THE INTEL COMPLILER DOES NOT CALL THE OVERLOADED delete OPERATOR
    // THE GCC DOES CALL THE OVERLOADED delete OPERATOR

    cout << "------------------------------------------------>> 16" << endl;

    delete [] pA2;

    return 0;
}

// end
