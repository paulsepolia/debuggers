//========//
// void * //
//========//

#include <iostream>
#include <iomanip>
#include <string>
using std::cin;
using std::cout;
using std::endl;
using std::setprecision;
using std::fixed;
using std::string;

//==================//
// the main program //
//==================//

int main ()
{
    cout << fixed;
    cout << setprecision(10);

    void * p1 = new string("abc");
    void * p2 = new string("ABC");

    long int * pd1 = new long int [100];
    long int * pd2 = new long int [100];

    cout << "--->> cast --> c-style cast" << endl;

    pd1 = (long int*)(p1);
    pd2 = (long int*)(p2);

    cout << " *pd1 = " << *pd1 << endl;
    cout << " *pd2 = " << *pd2 << endl;

    cout << "--->> cast --> static_cast" << endl;

    pd1 = static_cast<long int*>(p1);
    pd2 = static_cast<long int*>(p2);

    cout << " *pd1 = " << *pd1 << endl;
    cout << " *pd2 = " << *pd2 << endl;

    cout << "--->> cast --> dynamic_cast --> can not be done" << endl;

    //pd1 = dynamic_cast<double*>(p1); // ERROR: target is not pointer or reference to a class
    //pd2 = dynamic_cast<double*>(p2); // ERROR: target is not pointer or reference to a class

    cout << "--->> cast --> reinterpret_cast" << endl;

    pd1 = reinterpret_cast<long int*>(p1);
    pd2 = reinterpret_cast<long int*>(p2);

    cout << " *pd1 = " << *pd1 << endl;
    cout << " *pd2 = " << *pd2 << endl;

    return 0;
}

// END
