//==============================================//
// driver program 						   //
// move contructor and move assignment operator //
// and unique_ptr 						   //
//==============================================//

#include "parameters.hpp"
#include "vec.hpp"
#include "functions.hpp"
#include <iostream>
#include <cmath>
#include <iomanip>

using std::cout;
using std::endl;
using std::cos;
using std::sin;
using std::fixed;
using std::setprecision;
using std::showpos;

// the main program

int main()
{
    cout << setprecision(10);
    cout << fixed;
    cout << showpos;

    for(uli i = 0; i != TRIALS; i++) {
        cout << "----------------------------------------------->> " << i << endl;

        Vec<double> v1;
        Vec<double> v2;

        cout << " ---------->> allocate" << endl;

        v1.allocate(DIMEN);
        v2.allocate(DIMEN);

        cout << " ---------->> build" << endl;

        for(uli j = 0; j != DIMEN; j++) {
            v1.set(j, sin(static_cast<double>(j)));
            v2.set(j, cos(static_cast<double>(j+1)));
        }

        cout << " ---------->> v1 = v2;" << endl;

        cout << " --> " << v1.get(10) << endl;
        cout << " --> " << v2.get(10) << endl;

        v1 = v2;

        cout << " --> " << v1.get(10) << endl;
        cout << " --> " << v2.get(10) << endl;

        cout << " ---------->> deallocate" << endl;

        v1.deallocate();
        v2.deallocate();
    }

    cout << " p--> exit" << endl;
    return 0;
}

// end
