
#include <iostream>

using std::endl;
using std::cout;
using std::cin;

// the main program

int main()
{
    int * x1 = new int;

    *x1 = 10;

    cout << " --> *x1 = " << *x1 << endl;
    cout << " --> &x1 = " << &x1 << endl;
    cout << " -->  x1 = " <<  x1 << endl;

    int * x2 = new int;

    *x2 = 20;

    x1 = x2;

    cout << " --> *x1 = " << *x1 << endl;
    cout << " --> &x1 = " << &x1 << endl;
    cout << " -->  x1 = " <<  x1 << endl;

    cout << " --> *x2 = " << *x2 << endl;
    cout << " --> &x2 = " << &x2 << endl;
    cout << " -->  x2 = " <<  x2 << endl;

    delete x2;

    return 0;
}

// end
