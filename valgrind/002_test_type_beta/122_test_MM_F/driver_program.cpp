//============//
// shared_ptr //
//============//

#include <iostream>
#include <memory>
#include <cmath>
#include <vector>

using std::cout;
using std::endl;
using std::shared_ptr;
using std::pow;
using std::vector;
using std::puts;
using std::free;

// type definitions

typedef unsigned long int uli;
typedef const uli culi;

// overload the delete operator

void operator delete(void* ptr) noexcept
{

    static uli index;
    index++;
    cout << " --> index = " << index << endl;

    cout << " --> overloaded delete operator called" << endl;

    if(ptr != 0) {
        cout << " --> INDEX --> 2" << endl;
        free(ptr);
        ptr = 0;
    }
}

// deleter

template<typename T>
struct D {

    // a verbose array deleter

    void operator()(T * p)
    {

        cout << " --> deleter called" << endl;

        if(p != 0) {
            cout << " --> INDEX --> 1" << endl;
            delete [] p;
            p = 0;
        }
    }
};

// the main function

int main()
{
    culi	DIMEN(static_cast<uli>(pow(10.0, 1.0)));
    culi	TRIALS(static_cast<uli>(pow(10.0, 1.0)));

    // the pointer

    double * p = new double(10.0);

    // the vector

    vector<shared_ptr<double>> vsp;

    // the shared pointer

    shared_ptr<double> sp(p, D<double>());

    // build the vector

    for(uli k = 0; k != TRIALS; k++) {
        for(uli i = 0; i != DIMEN; i++) {

            vsp.push_back(sp);
        }
    }

    cout << " -->  sp             = " << sp << endl;
    cout << " -->  sp.get()       = " << sp.get() << endl;
    cout << " -->  sp.use_count() = " << sp.use_count() << endl;
    cout << " --> *sp.get()       = " << *sp.get() << endl;
    cout << " --> *sp             = " << *sp << endl;

    cout << " -->  vsp[1]             = " << vsp[1] << endl;
    cout << " -->  vsp[1].get()       = " << vsp[1].get() << endl;
    cout << " -->  vsp[1].use_count() = " << vsp[1].use_count() << endl;
    cout << " --> *vsp[1].get()       = " << *vsp[1].get() << endl;
    cout << " --> *vsp[1]             = " << *vsp[1] << endl;

    cout << " -->  vsp[2]             = " << vsp[2] << endl;
    cout << " -->  vsp[2].get()       = " << vsp[2].get() << endl;
    cout << " -->  vsp[2].use_count() = " << vsp[2].use_count() << endl;
    cout << " --> *vsp[2].get()       = " << *vsp[2].get() << endl;
    cout << " --> *vsp[2]             = " << *vsp[2] << endl;

    //if (p != 0) {
    //     cout << " --> delete here" << endl;
    //     delete p;
    //}

    return 0;
}

// end
