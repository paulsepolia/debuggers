//================//
// shared pointer //
//================//

#include <iostream>
#include <memory>

using std::cin;
using std::cout;
using std::endl;
using std::shared_ptr;

//==================//
// the main program //
//==================//

int main ()
{
    const int IMAX = 10;

    for (int i = 0; i != IMAX; i++) {
        cout << "---------------------------------------------->> " << i << endl;

        double * p1;

        p1 = new double(111);

        shared_ptr<double> sp1(p1);

        cout << " --> sp1.use_count() = " << sp1.use_count() << endl;

        cout << " --> sp1.get() = " << sp1.get() << endl;

        cout << " --> *sp1.get() = " << *sp1.get() << endl;
    }

    return 0;
}

// END
