//=====================================//
// overloaded new and delete operators //
// garbage collector 			    //
//=====================================//

#include <iostream>
#include <cmath>
#include <list>
#include <memory>

using std::cout;
using std::endl;
using std::pow;
using std::list;
using std::free;
using std::shared_ptr;

// parameres

const bool FLAG_DEBUG1(false);
const bool FLAG_DEBUG2(true);

// type definitions

typedef unsigned long int uli;
typedef const uli culi;

// static variables

static uli indexAllocate(0);
static uli indexDelete(0);

// overload the new operator

void* operator new(size_t sz) throw(std::bad_alloc)
{
    indexAllocate++;
    if(FLAG_DEBUG1) {
        cout << " --> inside new overloaded operator, indexAllocate = "
             << indexAllocate << endl;
    }
    return malloc(sz);
}

// overload the delete operator

void operator delete(void* ptr) noexcept
{
    indexDelete++;
    if(FLAG_DEBUG1) {
        cout << " --> overloaded delete, indexDelete = "
             << indexDelete << endl;
    }
    free(ptr);
    ptr = 0;

    if(FLAG_DEBUG2) {
        if(indexDelete == indexAllocate) {
            cout << " --> NO MEMORY LEAKS UP TO NOW" << endl;
        }
    }
}

// deleter

template<typename T>
class my_deleter {
public:
    void operator()(T * ptr)
    {
        delete [] ptr;
        ptr = 0;
    }
};

// class A

class A {
public:

    static list<shared_ptr<double>> _garbageListA;
};

list<shared_ptr<double>> A::_garbageListA {};

// class B

class B : public A {
};

// the main function

int main()
{
    culi	DIMEN(1*static_cast<uli>(pow(10.0, 2.0)));
    culi TRIALS(1*static_cast<uli>(pow(10.0, 1.0)));

    A objA;
    B objB;

    // create pointers, associate them with shared pointers
    // and put the shared pointers in garbage list

    for(uli k = 0; k != TRIALS; k++) {

        cout << "------------------------------------------------------------>> " << k << endl;
        cout << "---------------------------------------->>  1" << endl;

        for(uli i = 0 ; i != DIMEN; i++) {
            double * pd = new double(i);
            shared_ptr<double> spd(pd, my_deleter<double>());
            objA._garbageListA.push_back(spd);
        }

        cout << "---------------------------------------->>  2" << endl;

        for(uli i = 0 ; i != DIMEN; i++) {
            objB._garbageListA.front().reset();
            objB._garbageListA.pop_front();

        }

        cout << "---------------------------------------->>  3" << endl;
    }

    return 0;
}

// end
