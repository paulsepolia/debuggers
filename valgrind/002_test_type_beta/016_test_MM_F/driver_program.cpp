
#include <iostream>

using std::endl;
using std::cout;
using std::cin;

// base class --> Base

class Base {
public:

    // constructor

    Base()
    {
        cout << " --> constructor --> Base" << endl;
    }

    // destructor

    virtual ~Base()
    {
        cout << " --> destructor --> ~Base" << endl;
    }
};

// derived class --> D1

class D1 : public Base {
public:

    // constructor

    D1() : Base()
    {
        cout << " --> constructor --> D1" << endl;
    }

    // destructor

    virtual ~D1()
    {
        cout << " --> destructor --> ~D1" << endl;
    }
};

// the main program

int main()
{
    const int DIM = 100;

    // # 1

    cout << "------------------------------------------------------------>> # 1" << endl;

    cout << " --> Base * pb2 = new D1 [DIM];" << endl;
    Base * pb2 = new D1 [DIM] ;
    cout << " --> delete [] pb2;" << endl;
    delete [] pb2;

    cout << " ----------------------------> EXIT HERE(Nothing will happen)" << endl;

    return 0;
}

// end
