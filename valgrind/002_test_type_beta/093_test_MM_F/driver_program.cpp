//==================//
// use of allocator //
//==================//

#include <iostream>
#include <memory>
#include <cmath>
#include <iomanip>
#include <chrono>
#include <new>

using std::cout;
using std::endl;
using std::allocator;
using std::pow;
using std::boolalpha;
using namespace std::chrono;
using std::move;

// type definitions

typedef unsigned long int uli;
typedef const uli culi;

// the main program

int main()
{
    // local parameters

    culi TRIALS(static_cast<culi>(pow(10.0, 5.0)));
    culi DIMEN(static_cast<culi>(pow(10.0, 4.0)));
    cout << boolalpha;
    auto t1 = system_clock::now();
    auto t2 = system_clock::now();
    duration<double> time_span;

    // main game

    allocator<double> dalloc;
    double * arr1 = dalloc.allocate(DIMEN);

    // build array --> arr1

    for(uli i = 0; i != DIMEN; i++) {
        arr1[i] = static_cast<double>(i);
    }

    double * arr2;

    cout << " arr2 = arr1;" << endl;

    t1 = system_clock::now();

    uli i = 0;
    while(i != TRIALS) {
        arr2 = arr1;
        i++;
    }

    cout << " --> arr1[0] = " << arr1[0] << endl;
    cout << " --> arr1[1] = " << arr1[1] << endl;
    cout << " --> arr1[2] = " << arr1[2] << endl;
    cout << " --> arr2[0] = " << arr2[0] << endl;
    cout << " --> arr2[1] = " << arr2[1] << endl;
    cout << " --> arr2[2] = " << arr2[2] << endl;

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    cout << " i = " << i << endl;
    cout << " (arr1 == arr2) = " << (arr1 == arr2) << endl;

    t1 = system_clock::now();

    i = 0;
    while(i != TRIALS) {
        arr2 = move(arr1);
        arr1 = move(arr2);
        i++;
    }

    cout << " --> arr1[0] = " << arr1[0] << endl;
    cout << " --> arr1[1] = " << arr1[1] << endl;
    cout << " --> arr1[2] = " << arr1[2] << endl;
    cout << " --> arr2[0] = " << arr2[0] << endl;
    cout << " --> arr2[1] = " << arr2[1] << endl;
    cout << " --> arr2[2] = " << arr2[2] << endl;

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    cout << " i = " << i << endl;
    cout << " (arr1 == arr2) = " << (arr1 == arr2) << endl;

    cout << " --> dalloc.address(arr1[0]) = " << dalloc.address(arr1[0]) << endl;
    cout << " --> dalloc.address(arr1[1]) = " << dalloc.address(arr1[1]) << endl;
    cout << " --> dalloc.address(arr1[2]) = " << dalloc.address(arr1[2]) << endl;
    cout << " --> &arr1[0] = " << &arr1[0] << endl;
    cout << " --> &arr1[1] = " << &arr1[1] << endl;
    cout << " --> &arr1[2] = " << &arr1[2] << endl;

    cout << " --> dalloc.address(arr2[0]) = " << dalloc.address(arr2[0]) << endl;
    cout << " --> dalloc.address(arr2[1]) = " << dalloc.address(arr2[1]) << endl;
    cout << " --> dalloc.address(arr2[2]) = " << dalloc.address(arr2[2]) << endl;
    cout << " --> &arr2[0] = " << &arr2[0] << endl;
    cout << " --> &arr2[1] = " << &arr2[1] << endl;
    cout << " --> &arr2[2] = " << &arr2[2] << endl;


    dalloc.deallocate(arr1, DIMEN);
    //dalloc.deallocate(arr2, DIMEN); // IT IS A SEGFAULT

    return 0;
}

// end
