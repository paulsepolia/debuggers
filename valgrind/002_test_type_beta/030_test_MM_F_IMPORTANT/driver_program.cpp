//================//
// shared pointer //
//================//

// Convert a C-pointer to shared pointer

#include <iostream>
#include <memory>

using std::cin;
using std::cout;
using std::endl;
using std::shared_ptr;

//==================//
// the main program //
//==================//

int main ()
{
    const int IMAX = 10;
    double * p1;

    for (int i = 0; i != IMAX; i++) {
        cout << "---------------------------------------------->> " << i << endl;

        shared_ptr<double> sp1;

        p1 = new double(21.123);

        sp1.reset(p1);

        cout << " --> sp1.use_count() = " << sp1.use_count() << endl;

        cout << " --> sp1.get() = " << sp1.get() << endl;

        cout << " --> *sp1.get() = " << *sp1.get() << endl;
    }

    return 0;
}

// END
