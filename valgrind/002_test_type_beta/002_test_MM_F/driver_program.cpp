
#include <iostream>

using std::endl;
using std::cout;
using std::cin;

// base class --> B1

class B1 {
public:

    // constructor

    B1()
    {
        cout << " --> constructor --> B1" << endl;
        x1 = 10;
        x2 = 20;
        x3 = 30;
        x4 = new int (20);
    }

    // destructor

    virtual ~B1()
    {
        cout << " --> destructor --> ~B1" << endl;

        if (x4) {
            cout << " --> ~B1 --> delete --> x4" << endl;
            delete x4;
            x4 = 0;
        }
    }

private:

    int x1;
    int x2;
    int x3;

protected:

    int * x4;
};

// derived class --> D1

class D1 : public B1 {
public:

    // constructor

    D1() : B1()
    {
        cout << " --> constructor --> D1" << endl;
        x4 = new int (10); // this is form D1
        x5 = 40;
        x6 = 50;
        x7 = new int (60);
    }

    // destructor

    virtual ~D1()
    {
        cout << " --> destructor --> ~D1" << endl;
        if (x7) {
            cout << " --> ~D1 --> delete --> x7" << endl;
            delete x7;
            x7 = 0;
        }
        if (x4) {
            cout << " --> ~D1 --> delete --> x4" << endl;
            delete x4;
            x4 = 0;
        }
    }

private:

    int x5;
    int x6;
    int * x7;
    int * x4;
};

// the main program

int main()
{
    // # 1

    cout << " --> B1 * pb1 = new B1;" << endl;

    B1 * pb1 = new B1;

    cout << " --> delete pb1;" << endl;

    delete pb1;

    // # 2

    cout << " --> B1 * pd1 = new D1;" << endl;

    B1 * pd1 = new D1;

    cout << " --> delete pd1;" << endl;

    delete pd1;

    // # 3

    cout << " --> D1 * pd2 = new D1;" << endl;

    D1 * pd2 = new D1;

    cout << " --> delete pd2;" << endl;

    delete pd2;

    return 0;
}

// end
